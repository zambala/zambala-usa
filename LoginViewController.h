//
//  LoginViewController.h
//  ZambalaUSA
//
//  Created by zenwise mac 2 on 9/5/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *clientIDTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
- (IBAction)onContinueButtonTap:(id)sender;

@property NSMutableDictionary * loginResponseDictionary;

@end
