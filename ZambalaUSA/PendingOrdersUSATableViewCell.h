//
//  PendingOrdersUSATableViewCell.h
//  
//
//  Created by Zenwise Technologies on 16/10/17.
//
//

#import <UIKit/UIKit.h>

@interface PendingOrdersUSATableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateAndTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *tickerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *totalQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *filledQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *pendingQuantityLabel;

@end
