//
//  OrderViewController.m
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "OrderViewController.h"
#import "ACFloatingTextField.h"
#import "AppDelegate.h"
#import "MainOrdersUSA.h"

@interface OrderViewController ()
{
    ACFloatingTextField * textField1;
    ACFloatingTextField * textField2;
    ACFloatingTextField * textField3;
    ACFloatingTextField * textField4;
    ACFloatingTextField * textField5;
    NSString * orderTypeCheck;
    AppDelegate * delegate;
    NSString * str;
}

@end

@implementation OrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Order";
    
    orderTypeCheck =@"1";

    
    self.submitOrderButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.submitOrderButton.layer.shadowOpacity = 0;
    self.submitOrderButton.layer.shadowRadius = 0;
    self.submitOrderButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    
    self.orderTypeView.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.orderTypeView.layer.shadowOpacity = 0;
    self.orderTypeView.layer.shadowRadius = 2.1;
    self.orderTypeView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    
    self.orderValueView.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.orderValueView.layer.shadowOpacity = 0;
    self.orderValueView.layer.shadowRadius = 2.1;
    self.orderValueView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);

    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    textField1=[ACFloatingTextField new];
    textField1.frame=CGRectMake(10,58,80,30);
    [textField1 setTranslatesAutoresizingMaskIntoConstraints:YES];
    textField1.placeholder=@"";
    [textField1 setFont:[UIFont fontWithName:@"Ubuntu-Medium" size:13.5]];
    //    [textField1 setValue:[UIColor darkGrayColor] forKey:@"_placeholderLabel.textColor"];
    textField1.clearButtonMode=UITextFieldViewModeNever;
    [self.orderValueView addSubview:textField1];
    CALayer * border1=[CALayer layer];
    CGFloat borderWidth1=1;
    border1.borderColor=[UIColor whiteColor].CGColor;
    border1.frame=CGRectMake(0, textField1.frame.size.height-borderWidth1, textField1.frame.size.width, textField1.frame.size.height);
    border1.borderWidth=borderWidth1;
    [textField1.layer addSublayer:border1];
    textField1.layer.masksToBounds=YES;
    
    textField1.selectedLineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];
    textField1.placeHolderColor = [UIColor whiteColor];
    textField1.textColor=[UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField1.selectedPlaceHolderColor = [UIColor whiteColor];
    textField1.lineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];

    NSLog(@"Orders:%@",self.recomondation);
    [self adviceViewContent];
    
    [self.buySellSegment addTarget:self action:@selector(onBuySellTap) forControlEvents:UIControlEventValueChanged];
    
    
    if([self.recomondation isEqualToString:@"Buy"])
    {
        self.buySellCheck=@"1";
        [self.buySellSegment setSelectedSegmentIndex:0];
    }else if([self.recomondation isEqualToString:@"Sell"])
    {
        self.buySellCheck=@"2";
        [self.buySellSegment setSelectedSegmentIndex:1];
    }else if ([self.recomondation isEqualToString:@"StrongBuy"])
    {
        self.buySellCheck=@"1";
        [self.buySellSegment setSelectedSegmentIndex:0];
    }
    
    [self.submitOrderButton addTarget:self action:@selector(onSubmitButtontap) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)adviceViewContent
{
    
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.tickerNameLabel.text=self.tickerName;
        self.companyNameLabel.text=self.companyName;
        
    });
    

  
   
    self.adviceView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    
    self.adviceView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    
    self.adviceView.layer.shadowOpacity = 2.0f;
    
    self.adviceView.layer.shadowRadius = 3.2f;
    
    self.adviceView.layer.cornerRadius=2.1f;
    
    self.adviceView.layer.masksToBounds = YES;
   // self.adviceView.layer.borderColor =  [[UIColor colorWithRed:247/255 green:247/255 blue:247/255 alpha:0.18] CGColor];
    
    //self.adviceView.layer.borderWidth = 1.0f;
  // self.adviceView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:brands2.bounds cornerRadius:self.adviceView.layer.cornerRadius].CGPath;
    
    self.orderValueView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    
    self.orderValueView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    
    self.orderValueView.layer.shadowOpacity = 2.0f;
    
    self.orderValueView.layer.shadowRadius = 3.2f;
    
    self.orderValueView.layer.cornerRadius=2.1f;
    
    self.orderValueView.layer.masksToBounds = YES;
    
    
    self.orderTypeView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];
    
    self.orderTypeView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    
    self.orderTypeView.layer.shadowOpacity = 2.0f;
    
    self.orderTypeView.layer.shadowRadius = 3.2f;
    
    self.orderTypeView.layer.cornerRadius=2.1f;
    
    self.orderTypeView.layer.masksToBounds = YES;
}
-(void)onBuySellTap
{
    if(self.buySellSegment.selectedSegmentIndex==0)
    {
       self.buySellCheck=@"1";
    }else if (self.buySellSegment.selectedSegmentIndex==1)
    {
       self.buySellCheck=@"2";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)marketButtonAction:(id)sender {
    
    [textField2 removeFromSuperview];
    [textField3 removeFromSuperview];
    [textField4 removeFromSuperview];
    
    orderTypeCheck=@"1";


    [self.marketButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.15f]];
    [self.limitButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    [self.stopLossButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    
    self.sumbitOrderButtonHeight.constant=41.6;

}
- (IBAction)limitButtonAction:(id)sender {
    
    [textField2 removeFromSuperview];
    [textField3 removeFromSuperview];
    [textField4 removeFromSuperview];
    orderTypeCheck = @"2";
    
    [self.marketButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    [self.limitButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.15f]];
    [self.stopLossButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];

    self.sumbitOrderButtonHeight.constant=120;
    
    textField2=[ACFloatingTextField new];
    textField2.frame=CGRectMake(13,385,80,45);
    [textField2 setTranslatesAutoresizingMaskIntoConstraints:YES];
    textField2.placeholder=@"Limit price";
     [textField2 setFont:[UIFont fontWithName:@"Ubuntu-Medium" size:13.5]];
    //    [textField1 setValue:[UIColor darkGrayColor] forKey:@"_placeholderLabel.textColor"];
    textField2.clearButtonMode=UITextFieldViewModeNever;
    [self.orderMainView addSubview:textField2];
    CALayer * border2=[CALayer layer];
    CGFloat borderWidth2=1;
    border2.borderColor=[UIColor whiteColor].CGColor;
    border2.frame=CGRectMake(0, textField2.frame.size.height-borderWidth2
                             , textField2.frame.size.width, textField2.frame.size.height);
    border2.borderWidth=borderWidth2;
    [textField2.layer addSublayer:border2];
    textField2.layer.masksToBounds=YES;
    
    textField2.selectedLineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];
    textField2.placeHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField2.textColor=[UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField2.selectedPlaceHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField2.lineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];
    
    
   }
- (IBAction)stopLossButtonAction:(id)sender {
    
 
    [textField2 removeFromSuperview];
     [textField3 removeFromSuperview];
     [textField4 removeFromSuperview];
    orderTypeCheck =@"3";

    
    [self.marketButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    [self.limitButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    [self.stopLossButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.15f]];
    
    self.sumbitOrderButtonHeight.constant=120;
    textField3=[ACFloatingTextField new];
    textField3.frame=CGRectMake(13,385,100,45);
    [textField3 setTranslatesAutoresizingMaskIntoConstraints:YES];
    textField3.placeholder=@"Trigger price";
     [textField3 setFont:[UIFont fontWithName:@"Ubuntu-Medium" size:13.5]];
    //    [textField1 setValue:[UIColor darkGrayColor] forKey:@"_placeholderLabel.textColor"];
    textField3.clearButtonMode=UITextFieldViewModeNever;
    [self.orderMainView addSubview:textField3];
    CALayer * border3=[CALayer layer];
    CGFloat borderWidth3=1;
    border3.borderColor=[UIColor whiteColor].CGColor;
    border3.frame=CGRectMake(0, textField3.frame.size.height-borderWidth3, textField3.frame.size.width, textField3.frame.size.height);
    border3.borderWidth=borderWidth3;
    [textField3.layer addSublayer:border3];
    textField3.layer.masksToBounds=YES;
    
    textField3.selectedLineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];
    textField3.placeHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField3.textColor=[UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField3.selectedPlaceHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField3.lineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];
    
    
    textField4=[ACFloatingTextField new];
    textField4.frame=CGRectMake(self.view.frame.size.width-191,385,100,45);
    [textField4 setTranslatesAutoresizingMaskIntoConstraints:YES];
    textField4.placeholder=@"Stop limit price";
     [textField4 setFont:[UIFont fontWithName:@"Ubuntu-Medium" size:13.5]];
    //    [textField1 setValue:[UIColor darkGrayColor] forKey:@"_placeholderLabel.textColor"];
    textField4.clearButtonMode=UITextFieldViewModeNever;
    [self.orderMainView addSubview:textField4];
    CALayer * border4=[CALayer layer];
    CGFloat borderWidth4=1;
    border4.borderColor=[UIColor whiteColor].CGColor;
    border4.frame=CGRectMake(0, textField4.frame.size.height-borderWidth4, textField4.frame.size.width, textField4.frame.size.height);
    border4.borderWidth=borderWidth4;
    [textField4.layer addSublayer:border4];
    textField4.layer.masksToBounds=YES;
    
    textField4.selectedLineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];
    textField4.placeHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField4.textColor=[UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField4.selectedPlaceHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField4.lineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];

}
-(void)orderServer
{
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    int len=6;
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    for (int i=0; i<len; i++) {
        
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
        
    }
    
    NSLog(@"Random:%@",randomString);
    
    //trailtype
    //uid
    //legs
    
    NSString * legs = [NSString stringWithFormat:@"\"Leg1\":{\"tx\":\"%@\",\"qty\":\"%@\",\"symbol\":\"%@\"}",self.buySellCheck,textField1.text,delegate.stockTickerString];
    
    //        ,\"tx\":\"1\"
    
    if([orderTypeCheck isEqualToString:@"1"])
    {
    
    
    
    str = [NSString stringWithFormat:@"api={\"name\":\"JSON_PlaceOrder\",\"sourceID\":\"T7qqV91jR2\",\"sessionID\":\"%@\",\"firmCode\":\"5ntJOPXqQb\",\"requestType\":\"real\",\"account\":\"T00051009\",\"duration\":\"1\",\"ordertype\":\"%@\",\"limprice\":\"0\",\"stopprice\":\"0\",\"legs\": {%@},\"requesteddestination\":\"\",\"aon\":\"0\",\"trailPrice\":\"\",\"trailType\":0,\"ActivateTime0\":\"\",\"ExpireTime0\":\"\",\"HiddenOrder0\":\"\",\"Conditions0\":\"\",\"ActivateTime1\":\"\",\"ExpireTime1\":\"\",\"HiddenOrder1\":\"\",\"Conditions1\":\"\",\"QueueOrderID\":\"\",\"forMargin\":\"C\",\"contingency\":\"XXXXXXXXXXXXXXXXXX 1 1 Single Order\",\"manualCommission\":\"\",\"forCollateral\":\"\",\"journalEntry\":\"\",\"channel\":\"\",\"limittype\":\"\",\"watchprice\":\"\",\"isMFrequest\":\"N\",\"qtyType\":\"\",\"uid\":\"%@\",\"timeInForce\":\"1\",\"IPaddress\":\"1.1.1.1\"}",delegate.sessionID,orderTypeCheck,legs,randomString];
    }else if ([orderTypeCheck isEqualToString:@"2"])
    {
        
        str = [NSString stringWithFormat:@"api={\"name\":\"JSON_PlaceOrder\",\"sourceID\":\"T7qqV91jR2\",\"sessionID\":\"%@\",\"firmCode\":\"5ntJOPXqQb\",\"requestType\":\"real\",\"account\":\"T00051009\",\"duration\":\"1\",\"ordertype\":\"%@\",\"limprice\":\"%@\",\"stopprice\":\"0\",\"legs\": {%@},\"requesteddestination\":\"\",\"aon\":\"0\",\"trailPrice\":\"\",\"trailType\":0,\"ActivateTime0\":\"\",\"ExpireTime0\":\"\",\"HiddenOrder0\":\"\",\"Conditions0\":\"\",\"ActivateTime1\":\"\",\"ExpireTime1\":\"\",\"HiddenOrder1\":\"\",\"Conditions1\":\"\",\"QueueOrderID\":\"\",\"forMargin\":\"C\",\"contingency\":\"XXXXXXXXXXXXXXXXXX 1 1 Single Order\",\"manualCommission\":\"\",\"forCollateral\":\"\",\"journalEntry\":\"\",\"channel\":\"\",\"limittype\":\"\",\"watchprice\":\"\",\"isMFrequest\":\"N\",\"qtyType\":\"\",\"uid\":\"%@\",\"timeInForce\":\"1\",\"IPaddress\":\"1.1.1.1\"}",delegate.sessionID,orderTypeCheck,textField2.text,legs,randomString];
        
        
    }else if ([orderTypeCheck isEqualToString:@"3"])
    {
        str = [NSString stringWithFormat:@"api={\"name\":\"JSON_PlaceOrder\",\"sourceID\":\"T7qqV91jR2\",\"sessionID\":\"%@\",\"firmCode\":\"5ntJOPXqQb\",\"requestType\":\"real\",\"account\":\"T00051009\",\"duration\":\"1\",\"ordertype\":\"%@\",\"limprice\":\"%@\",\"stopprice\":\"%@\",\"legs\": {%@},\"requesteddestination\":\"\",\"aon\":\"0\",\"trailPrice\":\"\",\"trailType\":0,\"ActivateTime0\":\"\",\"ExpireTime0\":\"\",\"HiddenOrder0\":\"\",\"Conditions0\":\"\",\"ActivateTime1\":\"\",\"ExpireTime1\":\"\",\"HiddenOrder1\":\"\",\"Conditions1\":\"\",\"QueueOrderID\":\"\",\"forMargin\":\"C\",\"contingency\":\"XXXXXXXXXXXXXXXXXX 1 1 Single Order\",\"manualCommission\":\"\",\"forCollateral\":\"\",\"journalEntry\":\"\",\"channel\":\"\",\"limittype\":\"\",\"watchprice\":\"\",\"isMFrequest\":\"N\",\"qtyType\":\"\",\"uid\":\"%@\",\"timeInForce\":\"1\",\"IPaddress\":\"1.1.1.1\"}",delegate.sessionID,orderTypeCheck,textField3.text,textField4.text,legs,randomString];
    }
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    
    
    NSLog(@"order string:%@",str);
    
    
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://api-test.choicetrade.com/"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        NSString * successCheck = [json objectForKey:@"message"];
                                                        
                                                        if([successCheck containsString:@"Success"])
                                                        {
                                                        
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                
                                                                UIAlertController * alert = [UIAlertController
                                                                                             alertControllerWithTitle:@"Order Status"
                                                                                             message:@"Order Placed Successfully!"
                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                //Add Buttons
                                                                
                                                                UIAlertAction* okButton = [UIAlertAction
                                                                                           actionWithTitle:@"Ok"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               //Handle your yes please button action here
                                                                            textField1.text=@"";
                                                                                               textField2.text=@"";
                                                                                               textField3.text=@"";
                                                                                               textField4.text=@"";
                                                                                               MainOrdersUSA * mainOrders = [self.storyboard instantiateViewControllerWithIdentifier:@"MainOrdersUSA"];
                                                                                               mainOrders.orderStatusString=@"success";
                                                                                               [self.navigationController pushViewController:mainOrders animated:YES];
                                                                  //  [self presentViewController:mainOrders animated:YES completion:nil];
                                                                                               
                                                                                           }];
                                                                //Add your buttons to alert controller
                                                                
                                                                [alert addAction:okButton];
                                                                
                                                                
                                                                [self presentViewController:alert animated:YES completion:nil];
                                                               
                                                            });
                                                        }else
                                                        {
                                                            
                                                           
                                                            NSString * orderFailString = [NSString stringWithFormat:@"Order Failed! Reason:%@",[json objectForKey:@"message"]];
                                                            UIAlertController * alert = [UIAlertController
                                                                                         alertControllerWithTitle:@"Order Status"
                                                                                         message:orderFailString
                                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            //Add Buttons
                                                            
                                                            UIAlertAction* okButton = [UIAlertAction
                                                                                       actionWithTitle:@"Ok"
                                                                                       style:UIAlertActionStyleDefault
                                                                                       handler:^(UIAlertAction * action) {
                                                                                           //Handle your yes please button action here
                                                                                           textField1.text=@"";
                                                                                           textField2.text=@"";
                                                                                           textField3.text=@"";
                                                                                           textField4.text=@"";
                                                                                           MainOrdersUSA * mainOrders = [self.storyboard instantiateViewControllerWithIdentifier:@"MainOrdersUSA"];
                                                                                           mainOrders.orderStatusString=@"failed";
                                                                    
                                                                                           [self presentViewController:mainOrders animated:YES completion:nil];
                                                                                           
                                                                                           
                                                                                       }];
                                                            //Add your buttons to alert controller
                                                            
                                                            [alert addAction:okButton];
                                                            
                                                            
                                                            [self presentViewController:alert animated:YES completion:nil];
                                                        }
                                                    
                                                    }
                                                }];
    [dataTask resume];
    
    
}
-(void)onSubmitButtontap
{
    [self orderServer];
}

@end
