//
//  ProfileViewController.m
//  ZambalaUSA
//
//  Created by guna on 18/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "ProfileViewController.h"
#import "RecentAdviceTableViewCell.h"
#import "AppDelegate.h"
#import "OrderViewController.h"
#import "NewOrderViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface ProfileViewController ()
{
    AppDelegate * delegate;
    NSMutableString * tickerStoreString;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    self.responseDict=[[NSMutableDictionary alloc]init];
    [self.navigationItem.backBarButtonItem setTitle:@"Title here"];
    
    
    
    [[self.successRateButton layer] setBorderWidth:2.0f];
    [[self.successRateButton layer] setBorderColor:[UIColor colorWithRed:30/255.0 green:113/255.0 blue:142/255.0 alpha:0.7f].CGColor];
    self.successRateButton.layer.cornerRadius=4;
    self.successRateButton.clipsToBounds=YES;
    
    [[self.averageReturnButton layer] setBorderWidth:2.0f];
    [[self.averageReturnButton layer] setBorderColor:[UIColor colorWithRed:30/255.0 green:113/255.0 blue:142/255.0 alpha:0.7f].CGColor];
    self.averageReturnButton.layer.cornerRadius=4;
    self.averageReturnButton.clipsToBounds=YES;
    
    self.performanceView.layer.shadowOffset = CGSizeMake(0, 2);
    self.performanceView.layer.shadowOpacity =0.1;
    self.performanceView.layer.shadowRadius = 3.1;
    self.performanceView.clipsToBounds = NO;
    
    
    self.stockRatingView.layer.shadowOffset = CGSizeMake(0, 2);
    self.stockRatingView.layer.shadowOpacity = 0.1;
    self.stockRatingView.layer.shadowRadius = 3.1;
    self.stockRatingView.clipsToBounds = NO;
    
    self.bestStockRatingView.layer.shadowOffset = CGSizeMake(0, 2);
    self.bestStockRatingView.layer.shadowOpacity = 0.1;
    self.bestStockRatingView.layer.shadowRadius = 3.1;
    self.bestStockRatingView.clipsToBounds = NO;
    self.tickerStoreArray = [[NSMutableArray alloc]init];
    
    [self serverHit];
    

    // Do any additional setup after loading the view.
}


-(void)serverHit
{
    NSLog(@"Test:%@",delegate.expertIDArray);
    NSDictionary *headers = @{ @"x-apikey": @"TR_Zenwise",
                               @"x-apitoken": @"8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",
                               };
    NSString * url = [NSString stringWithFormat:@"https://api.tipranks.com/api/Analysts/Overview/%@?detail=full",delegate.expertIDArray];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"Response dict:%@",self.responseDict);
                                                        if(self.tickerStoreArray.count>0)
                                                        {
                                                            [self.tickerStoreArray removeAllObjects];
                                                            tickerStoreString =[[NSString stringWithFormat:@""] mutableCopy];
                                                        }
                                                        for (int i=0; i<self.self.responseDict.count; i++) {
                                                            
                                                            [self.tickerStoreArray addObject:[[[self.responseDict objectForKey:@"stocks"] objectAtIndex:i] objectForKey:@"ticker"]];
                                                        }
                                                        
                                                        tickerStoreString = [[self.tickerStoreArray componentsJoinedByString:@","] mutableCopy];
                                                        

                                                       
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        
                                                        
                                                        [self allocation];
                                                        [self LTPServer];
                                                        
                                                    });

                                                }];
    [dataTask resume];
}

-(void)LTPServer
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"64a51fa5-83b5-e61d-be01-275a44103c72" };
    
    NSString * url1 = [NSString stringWithFormat:@"http://trexternalapi-preprod2.azurewebsites.net/api/stocks/prices/%@?X-APIKey=TR_Zenwise&X-APIToken=8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",tickerStoreString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.ltpResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"LTP:%@",self.ltpResponseArray);
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{

                                                            self.recentAdviceTableView.delegate=self;
                                                            self.recentAdviceTableView.dataSource=self;
                                                            [self.recentAdviceTableView reloadData];
                                                           // self.activityIndicator.hidden=YES;
                                                           // [self.activityIndicator stopAnimating];
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
}


-(void)allocation
{
    self.navigationItem.title =[self.responseDict objectForKey:@"analystName"];
    self.profileName.text = [self.responseDict objectForKey:@"analystName"];
    self.firmName.text = [self.responseDict objectForKey:@"firmName"];
    self.sectorLabel.text=[self.responseDict objectForKey:@"mainSector"];
    float success =[[self.responseDict objectForKey:@"successRate"] floatValue]*100 ;
    NSString * successString = [NSString stringWithFormat:@"%.2f%%",success];
    NSLog(@"Success Rate String:%@",successString);
    self.successRateLabel.text=successString;
    
    float average = [[self.responseDict objectForKey:@"excessReturn"] floatValue]*100;
    
    NSString * averageLabel = [NSString stringWithFormat:@"%.2f%%",average];
    
    
    
    if([averageLabel containsString:@"-"])
    {
        
        NSString * finalAverageLabel = [averageLabel substringFromIndex:1];
        float finalFLoat = [finalAverageLabel floatValue];
        NSString *final = [NSString stringWithFormat:@"%.2f%%",finalFLoat];
        NSLog(@"FinalLabel:%@",final);
        self.averageReturnLabel.textColor= [UIColor colorWithRed:255/255 green:55/255 blue:55/255 alpha:1];
        self.averageReturnLabel.text = final;
    }else
    {
        self.averageReturnLabel.textColor= [UIColor colorWithRed:27/255.0f green:160/255.0f blue:33/255.0f alpha:1.0f];
        //self.averageReturnLabel.textColor= [UIColor greenColor];
        self.averageReturnLabel.text = averageLabel;
    }
    
    
    if([[self.responseDict objectForKey:@"expertPictureUrl"] isEqual:[NSNull null]])
    {
        NSString *ImageURL = @"https://www.tipranks.com/images/analyst-fallback_tsqr.png";
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
        self.profileImageView.image = [UIImage imageWithData:imageData];
    }else
    {
        NSString *ImageURL = [NSString stringWithFormat:@"https://az712682.vo.msecnd.net/expert-pictures/%@",[self.responseDict objectForKey:@"expertPictureUrl"]];
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
        self.profileImageView.image = [UIImage imageWithData:imageData];
    }
    
    NSString * numOfStars = [self.responseDict objectForKey:@"numOfStars"];
    float floatStar= [numOfStars floatValue];
    NSLog(@"Float Star:%f",floatStar);
    self.starRatingView.minimumValue=0;
    self.starRatingView.maximumValue=5;
    self.starRatingView.value=floatStar;
    self.starRatingView.allowsHalfStars=YES;
    self.starRatingView.allowsHalfStars=YES;
    self.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
    self.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
    self.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
    [self.starRatingView setUserInteractionEnabled:NO];
    self.successRateDepthLabel.text = [NSString stringWithFormat:@"%@ out of %@ ratings were successful",[[self.responseDict objectForKey:@"goodRecommendations"] stringValue],[[self.responseDict objectForKey:@"totalRecommendations"] stringValue]];

    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self.responseDict objectForKey:@"stocks"] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    RecentAdviceTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"recent" forIndexPath:indexPath];
    cell.adviceTypeButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    cell.adviceTypeButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    cell.adviceTypeButton.layer.shadowOpacity = 1.0f;
    cell.adviceTypeButton.layer.shadowRadius = 2.0f;
    cell.adviceTypeButton.layer.cornerRadius=1.0f;
    cell.adviceTypeButton.layer.masksToBounds = NO;
    cell.layer.shadowRadius  = 1.5f;
    cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    cell.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    cell.layer.shadowOpacity = 0.9f;
    cell.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(cell.bounds, shadowInsets)];
    cell.layer.shadowPath    = shadowPath.CGPath;
    
//    cell.layer.shadowColor = [[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.18f]CGColor];
//    cell.layer.shadowOffset = CGSizeMake(0, 2);
//    cell.layer.shadowOpacity = 0.1;
//    cell.layer.shadowRadius = 4.2;
//    cell.clipsToBounds = NO;
    
    float  priceTargetFloat;
    
//    for (int i=0; i<[[self.responseDict objectForKey:@"stocks"] count]; i++) {
        NSString * testString = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ticker"];
        
        for(int j=0;j<self.ltpResponseArray.count;j++)
        {
        NSString * testString1 = [[self.ltpResponseArray objectAtIndex:j] objectForKey:@"ticker"];
        if([testString isEqualToString:testString1])
        {
            NSLog(@"Teststring:%@",testString);
            NSLog(@"Teststring1:%@",testString1);
            NSString * ltp = [NSString stringWithFormat:@"%@",[[self.ltpResponseArray objectAtIndex:j] objectForKey:@"price"]];
            float ltpInt = [ltp floatValue];
            cell.ltpLabel.text = [NSString stringWithFormat:@"%.2f",ltpInt];
            
            NSString * priceTarget = [NSString stringWithFormat:@"%@",[[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];
            if([priceTarget isEqual:[NSNull null]]||[priceTarget isEqualToString:@"<null>"])
            {
                priceTargetFloat = 0;
            }else
            {
                priceTargetFloat = [[NSString stringWithFormat:@"%@",[[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]] floatValue];
            }
            float upsideCalculation = ((priceTargetFloat-ltpInt)/(ltpInt))*100;
            NSString * upsideString = [NSString stringWithFormat:@"%.2f",upsideCalculation];
            
            NSLog(@"Upside calculation:%f",upsideCalculation);
            NSString * percent1 = @"%";
            
            if([upsideString containsString:@"-"])
            {
                if(priceTargetFloat==0)
                {
                    cell.upsideLabel.text = @"0";
                }
                else
                {
                    cell.upsideLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
                    cell.upsideLabel.text =[NSString stringWithFormat:@"%.2f%@(%@)",upsideCalculation,percent1,@"upside"];
                }
            }else
            {
                if(priceTargetFloat==0)
                {
                    cell.upsideLabel.text= @"0";
                }else
                {
                    cell.upsideLabel.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
                    cell.upsideLabel.text =[NSString stringWithFormat:@"%.2f%@(%@)",upsideCalculation,percent1,@"upside"];
                }
            }
            

           // NSString * actedBy = [NSString stringWithFormat:@"%@",[[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@""]];
    
    cell.tickerLabel.text = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    cell.companyNameLabel.text= [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"companyName"];
    cell.dateLabel.text= [NSString stringWithFormat:@"Advice on %@",[[[self.responseDict objectForKey:@"stocks"]objectAtIndex:indexPath.row]objectForKey:@"recommendationDate"]];
            
    NSString * adviceType = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"rating"];
        if([adviceType isEqual:[NSNull null]]||[adviceType isEqualToString:@"<null>"])
        {
            [cell.adviceTypeButton setBackgroundColor:[UIColor colorWithRed:(85/255.0) green:(90/255.0) blue:(92/255.0) alpha:1.0f]];
            cell.adviceType.text = @"HOLD";
            [cell.adviceTypeButton setTitle:@"HOLD" forState:UIControlStateNormal];
        }else if([adviceType isEqualToString:@"Buy"])
        {
            [cell.adviceTypeButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
            cell.adviceType.text=adviceType;
            [cell.adviceTypeButton setTitle:adviceType forState:UIControlStateNormal];
        }else if ([adviceType isEqualToString:@"Sell"])
        {
            [cell.adviceTypeButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
            cell.adviceType.text=adviceType;
            [cell.adviceTypeButton setTitle:adviceType forState:UIControlStateNormal];
        }
            
//    NSString * buttonTitle = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"rating"];
//    
    
    
    NSString * dollar = @"$";
    
    NSString * priceTarget1 = [NSString stringWithFormat:@"%@",[[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];
    
    NSString * finalPriceTarget = [dollar stringByAppendingString:priceTarget1];
    
    if([priceTarget1 isEqual:[NSNull null]]||[priceTarget1 isEqualToString:@"<null>"])
    {
        NSString * dollar = @"$";
        NSString * zero = @"0";
        NSString * final = [dollar stringByAppendingString:zero];
        cell.priceTargetLabel.text=final;
    }else
    {
        cell.priceTargetLabel.text=finalPriceTarget;
    }
    
   
            
            break;
    
        }
        else
        {
            cell.tickerLabel.text = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ticker"];
            cell.companyNameLabel.text= [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"companyName"];

            NSString * adviceType = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"rating"];
            if([adviceType isEqual:[NSNull null]]||[adviceType isEqualToString:@"<null>"])
            {
                [cell.adviceTypeButton setBackgroundColor:[UIColor colorWithRed:(85/255.0) green:(90/255.0) blue:(92/255.0) alpha:1.0f]];
                cell.adviceType.text = @"HOLD";
                [cell.adviceTypeButton setTitle:@"HOLD" forState:UIControlStateNormal];
            }else if([adviceType isEqualToString:@"Buy"])
            {
                [cell.adviceTypeButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
                cell.adviceType.text=adviceType;
                [cell.adviceTypeButton setTitle:adviceType forState:UIControlStateNormal];
            }else if ([adviceType isEqualToString:@"Sell"])
            {
                [cell.adviceTypeButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
                cell.adviceType.text=adviceType;
                [cell.adviceTypeButton setTitle:adviceType forState:UIControlStateNormal];
            }else if ([adviceType isEqualToString:@"Hold"])
            {
                [cell.adviceTypeButton setBackgroundColor:[UIColor colorWithRed:(85/255.0) green:(90/255.0) blue:(92/255.0) alpha:1.0f]];
                cell.adviceType.text = @"HOLD";
                [cell.adviceTypeButton setTitle:@"HOLD" forState:UIControlStateNormal];
            }
            cell.ltpLabel.text=@"0";
            cell.upsideLabel.text=@"";
           
            NSString * dollar = @"$";

            NSString * priceTarget1 = [NSString stringWithFormat:@"%@",[[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];

            NSString * finalPriceTarget = [dollar stringByAppendingString:priceTarget1];

            if([priceTarget1 isEqual:[NSNull null]]||[priceTarget1 isEqualToString:@"<null>"])
            {
                NSString * dollar = @"$";
                NSString * zero = @"0";
                NSString * final = [dollar stringByAppendingString:zero];
                cell.priceTargetLabel.text=final;
            }else
            {
                cell.priceTargetLabel.text=finalPriceTarget;
            }
        }

        
    }
    [cell.adviceTypeButton addTarget:self action:@selector(onBuySellTap:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
}
-(void)onBuySellTap:(UIButton*)sender
{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.recentAdviceTableView];
    NSIndexPath *indexPath = [self.recentAdviceTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    NewOrderViewController * trendingOrders = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    delegate.stockTickerString = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    delegate.orderRecomdationType = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"rating"];
    trendingOrders.recomondation =   [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"rating"];
    
    trendingOrders.tickerName = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    trendingOrders.companyName =[[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"companyName"];
    
    [self.navigationController pushViewController:trendingOrders animated:YES];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
