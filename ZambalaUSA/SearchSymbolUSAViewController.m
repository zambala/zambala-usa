//
//  SearchSymbolUSAViewController.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 09/11/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "SearchSymbolUSAViewController.h"
#import <UITextField_AutoSuggestion/UITextField+AutoSuggestion.h>
#import "MarketWatchViewController.h"
#import "AppDelegate.h"


@interface SearchSymbolUSAViewController ()<UITextFieldAutoSuggestionDataSource, UITextFieldDelegate>
@end
#define Symbol_ID @"symbol_id"

#define WEEKS @[@"Monday", @"Tuesday", @"Wednesday", @"Thirsday", @"Friday", @"Saturday", @"Sunday"]

@implementation SearchSymbolUSAViewController
{
    NSString * instrumentType;
    NSString * searchType;
    NSString * searchSymbol;
    NSString *searchStr;
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.title = @"Search";
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.segmentView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.segmentView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.segmentView.layer.shadowOpacity = 1.0f;
    self.segmentView.layer.shadowRadius = 1.0f;
    self.segmentView.layer.cornerRadius=1.0f;
    self.segmentView.layer.masksToBounds = NO;
    
    self.typeOfSearchView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.typeOfSearchView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.typeOfSearchView.layer.shadowOpacity = 1.0f;
    self.typeOfSearchView.layer.shadowRadius = 1.0f;
    self.typeOfSearchView.layer.cornerRadius=1.0f;
    self.typeOfSearchView.layer.masksToBounds = NO;
    
    self.textFieldView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.textFieldView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.textFieldView.layer.shadowOpacity = 1.0f;
    self.textFieldView.layer.shadowRadius = 1.0f;
    self.textFieldView.layer.cornerRadius=1.0f;
    self.textFieldView.layer.masksToBounds = NO;
    
    self.addButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.addButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.addButton.layer.shadowOpacity = 1.0f;
    self.addButton.layer.shadowRadius = 1.0f;
    self.addButton.layer.cornerRadius=1.0f;
    self.addButton.layer.masksToBounds = NO;
    

    
    
    [self.equityButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.equityButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self.etfButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.etfButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self.symbolButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.symbolButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    [self.nameButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.nameButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    
    
    self.equityButton.selected=YES;
    self.symbolButton.selected=YES;
    instrumentType=@"equity";
    searchType=@"symbol";
    
    [self.equityButton addTarget:self action:@selector(onEquityButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.etfButton addTarget:self action:@selector(onETFButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.symbolButton addTarget:self action:@selector(onSymbolButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.nameButton addTarget:self action:@selector(onNameButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.addButton addTarget:self action:@selector(onAddButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    
    self.symbols = [[NSMutableArray alloc]init];
    self.company = [[NSMutableArray alloc]init];
    self.selectedCompanyArray = [[NSMutableArray alloc]init];
    
    
    self.symbolTF.delegate=self;
   // self.searchTableView.hidden=YES;
    
    self.symbolTF.delegate = self;
    self.symbolTF.autoSuggestionDataSource = self;
    self.symbolTF.fieldIdentifier = Symbol_ID;
    [self.symbolTF observeTextFieldChanges];
    
    // Do any additional setup after loading the view.
}
- (void)loadWeekDays {
    // cancel previous requests
    @try {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(loadWeekDaysInBackground) object:self];
        [self.symbolTF setLoading:false];
        
        // clear previous results
        [self.symbols removeAllObjects];
        [self.symbolTF reloadContents];
        
        // start loading
        [self.symbolTF setLoading:true];
        [self performSelector:@selector(loadWeekDaysInBackground) withObject:self afterDelay:2.0f];
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
}


- (void)loadWeekDaysInBackground {
    // finish loading
    @try {
        [self.symbolTF setLoading:false];
        
        
        [self.symbols addObjectsFromArray:self.company];
        
        [self.symbolTF reloadContents];
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
}

- (UITableViewCell *)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    
    static NSString *cellIdentifier = @"WeekAutoSuggestionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
    
    cell.textLabel.text = weeks[indexPath.row];
    
    return cell;
}

- (NSInteger)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section forText:(NSString *)text {
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    
    @try {
        NSInteger count = [self.symbols filteredArrayUsingPredicate:filterPredictate].count;
        return count;
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    }
    
    return 0;
    
}

- (void)autoSuggestionField:(UITextField *)field textChanged:(NSString *)text {
    [self loadWeekDays];
}

- (CGFloat)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    return 50;
}
- (void)autoSuggestionField:(UITextField *)field tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath forText:(NSString *)text {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    NSLog(@"Selected suggestion at index row - %ld", (long)indexPath.row);
    
   
    
    NSPredicate *filterPredictate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@", text];
    NSArray *weeks = [self.symbols filteredArrayUsingPredicate:filterPredictate];
    
    self.symbolTF.text = weeks[indexPath.row];
    if(self.selectedCompanyArray.count>0)
    {
        [self.selectedCompanyArray removeAllObjects];
    }
    
    [self.selectedCompanyArray addObject:[self.searchResponseArray objectAtIndex:indexPath.row]];
    
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
     searchSymbol = [self.symbolTF.text stringByReplacingCharactersInRange:range withString:string];
    if(searchSymbol.length>=1)
    {
        [self serchServer];
    }
    return YES;
}




-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}

- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
   // [self.view setFrame:CGRectMake(0,-100,self.view.frame.size.width,self.view.frame.size.height)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.view endEditing:YES];
    return YES;
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    //[self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

-(void)serchServer
{
    NSString * url;
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"e35a25b6-fe3e-6af0-ddf4-121cd4b25d62" };
    if([searchType isEqualToString:@"symbol"])
    {
    url = [NSString stringWithFormat:@"http://zambaladev.ap-southeast-1.elasticbeanstalk.com/api/usstockinfo/symbol?symbol=%@&instrumenttype=%@&limit=5000&orderby=symbol&sortby=ASC",searchSymbol,instrumentType];
    }else if([searchType isEqualToString:@"name"])
    {
    url = [NSString stringWithFormat:@"http://zambaladev.ap-southeast-1.elasticbeanstalk.com/api/usstockinfo/symbol?longname=%@&instrumenttype=%@&limit=5000&orderby=longname&sortby=ASC",searchSymbol,instrumentType];
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.searchResponseArray =[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"SearchResponse:%@",self.searchResponseArray);
                                                        
                                                        if(self.company.count>0)
                                                        {
                                                            [self.company removeAllObjects];
                                                        }
                                                        if([searchType isEqualToString:@"symbol"])
                                                        {
                                                           
                                                        for(int i=0;i<self.searchResponseArray.count;i++)
                                                        {
                                                            
                                                            [self.company addObject:[[self.searchResponseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                            
                                                        }
                                                        }else if([searchType isEqualToString:@"name"])
                                                        {
                                                           
                                                            for(int i=0;i<self.searchResponseArray.count;i++)
                                                            {
                                                                [self.company addObject:[[self.searchResponseArray objectAtIndex:i]objectForKey:@"longname"]];
                                                            }
                                                        }
                                                        
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)dismissKeyboard
{
    [self.symbolTF resignFirstResponder];
}
-(void)onEquityButtonTap
{
    self.etfButton.selected=NO;
    self.equityButton.selected=YES;
    [self.nameButton setTitle:@" Name" forState:UIControlStateNormal];
    instrumentType=@"equity";
}
-(void)onETFButtonTap
{
    self.etfButton.selected=YES;
    self.equityButton.selected=NO;
    [self.nameButton setTitle:@" Fund Family" forState:UIControlStateNormal];
    instrumentType=@"etf";
}
-(void)onSymbolButtonTap
{
    self.nameButton.selected=NO;
    self.symbolButton.selected=YES;
    searchType=@"symbol";
}
-(void)onNameButtonTap
{
    self.nameButton.selected=YES;
    self.symbolButton.selected=NO;
    searchType=@"name";
}
-(void)onAddButtonTap
{
    NSLog(@"Add Button Tapped!");
    NSLog(@"Selected Array:%@",self.selectedCompanyArray);
   
//    addSymbol.equityTickername = [[NSMutableArray alloc]initWithObjects:@"AAPL",@"FB",@"ADBE",@"GOOGL",@"AMZN",@"BABA",@"NFLX",@"MA", nil];
//     addSymbol.equityCompanyname = [[NSMutableArray alloc]initWithObjects:@"Apple Inc",@"Facebook Inc",@"Adobe",@"Alphabet Inc",@"Amazon Inc",@"AliBaba",@"Netflix Inc",@"MasterCard", nil];
  
    [self sendToServer];
    
    
   
    
}

-(void)sendToServer
{
    NSString * ticker = [NSString stringWithFormat:@"%@",[[self.selectedCompanyArray objectAtIndex:0] objectForKey:@"symbol"]];
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"b5e52920-1676-36f3-1f6e-6bab047d74de" };
    NSDictionary *parameters = @{ @"clientid": @"RP3997",
                                  @"action": @"add",
                                  @"symbols": @[ticker] };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://52.66.159.178:8013/api/usstockinfo/marketwatch"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.addResponseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                       
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            if([[self.addResponseDict objectForKey:@"message"]isEqualToString:@"success"])
                                                            {
                                                                [self.navigationController popViewControllerAnimated:YES];
                                                            }else
                                                            {
                                                                NSLog(@"Error while adding symbol");
                                                            }
                                                           
                                                        });
                                                        
                                                       
                                                    }
                                                }];
    [dataTask resume];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
