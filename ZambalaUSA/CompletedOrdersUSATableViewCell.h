//
//  CompletedOrdersUSATableViewCell.h
//  
//
//  Created by Zenwise Technologies on 16/10/17.
//
//

#import <UIKit/UIKit.h>

@interface CompletedOrdersUSATableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeAndDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *tickerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *fillQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *pendingQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *dummyLabel;

@end
