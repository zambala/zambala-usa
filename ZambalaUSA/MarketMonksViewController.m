//
//  MarketMonksViewController.m
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "MarketMonksViewController.h"
#import "HMSegmentedControl.h"
#import "MonksTableViewCell.h"
#import "Top25MonksTableViewCell.h"
#import "FollowingTableViewCell.h"
#import "AppDelegate.h"
#import "HCSStarRatingView.h"
#import "RateView.h"
#import "ProfileViewController.h"
 @interface MarketMonksViewController ()
{
    HMSegmentedControl * segmentedControl;
    AppDelegate * delegate;
    NSURL * url;
    NSMutableString * tickerStoreString;
}

@end

@implementation MarketMonksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate=(AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.title = @"Market Monks";
    self.navigationItem.leftBarButtonItem.title=@" ";
       segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"TOP 25 MONKS",@"MONKS"]];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 54);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //    segmentedControl.verticalDividerEnabled = YES;
    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    segmentedControl.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    [self.view addSubview:segmentedControl];
    self.allAnalystsDict = [[NSMutableDictionary alloc]init];
    self.allAnalystArray = [[NSMutableArray alloc]init];
    self.allAnalystsNames = [[NSMutableArray alloc]init];
    [self.navigationItem.leftBarButtonItem setBackgroundVerticalPositionAdjustment:5 forBarMetrics:UIBarMetricsDefault];
    [self topAnalystsServer];
    
    // Do any additional setup after loading the view.
}


-(void)allAnalystsServer
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    self.monksTableView.hidden=YES;
    NSDictionary *headers = @{ @"x-apikey": @"TR_Zenwise",
                               @"x-apitoken": @"8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",
                               @"cache-control": @"no-cache"
                               };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.tipranks.com/api/LiveFeed?num=200"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        NSMutableArray* allAnalystArrayResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                    for (int i =0; i<allAnalystArrayResponse.count;i++)
                                                        {
                                                            NSMutableDictionary* E1 = [allAnalystArrayResponse objectAtIndex:i];
                                                            BOOL hasDuplicate = [[self.allAnalystArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"analystName == %@", [E1 objectForKey:@"analystName"]]] count] > 0;
                                                            
                                                            if (!hasDuplicate&&![[[[allAnalystArrayResponse objectAtIndex:i] objectForKey:@"numOfStars"] stringValue]isEqualToString:@"5"])
                                                            {
                                                                [self.allAnalystArray addObject:E1];
                                                            }
                                                        }
                                                        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"numOfStars" ascending:NO];
                                                        [self.allAnalystArray sortUsingDescriptors:[NSArray arrayWithObject:descriptor]];


                                                        NSLog(@"%@",self.allAnalystArray);
                                                        
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.monksTableView.delegate=self;
                                                        self.monksTableView.dataSource=self;
                                                        [self.monksTableView reloadData];
                                                        self.activityIndicator.hidden=YES;
                                                        [self.activityIndicator stopAnimating];
                                                        self.monksTableView.hidden=NO;
                                                        
                                                    });
                                                    

                                                }];
    [dataTask resume];

}

-(void)topAnalystsServer
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    self.monksTableView.hidden=YES;
    NSDictionary *headers = @{ @"x-apikey": @"TR_Zenwise",
                               @"x-apitoken": @"8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"b31077c9-6c12-ed74-cb4d-875e6b8f64fd" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.tipranks.com/api/Analysts/BestPerformingExperts/"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        self.topAnalysts = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"Top analysts:%@",self.topAnalysts);
                                                        
                                                        NSLog(@"Top analysts count:%ld",(unsigned long)self.topAnalysts.count);
                                                        
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.monksTableView.delegate=self;
                                                        self.monksTableView.dataSource=self;
                                                        [self.monksTableView reloadData];
                                                        self.activityIndicator.hidden=YES;
                                                        [self.activityIndicator stopAnimating];
                                                        self.monksTableView.hidden=NO;
                                                        
                                                    });
                                                }];
    [dataTask resume];
}





-(void)segmentedControlChangedValue
{
    if (segmentedControl.selectedSegmentIndex==0) {
        self.topAnalysts = [[NSMutableArray alloc]init];
        [self topAnalystsServer];
        //[self.monksTableView reloadData];
    }
    if (segmentedControl.selectedSegmentIndex==1) {
        

        [self allAnalystsServer];
        //[self.monksTableView reloadData];
    }
    if (segmentedControl.selectedSegmentIndex==2) {
        [self.monksTableView reloadData];
    }


}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(segmentedControl.selectedSegmentIndex==1)
    {
        return self.allAnalystArray.count;
    }
    else if(segmentedControl.selectedSegmentIndex==0)
    {
        return self.topAnalysts.count;
    }
    
    return 10;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (segmentedControl.selectedSegmentIndex==1) {
        
        MonksTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"monks" forIndexPath:indexPath];
        cell.detailsButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        cell.detailsButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
        cell.detailsButton.layer.shadowOpacity = 1.0f;
        cell.detailsButton.layer.shadowRadius = 2.0f;
        cell.detailsButton.layer.cornerRadius=1.0f;
        cell.detailsButton.layer.masksToBounds = NO;
        cell.layer.shadowRadius  = 1.5f;
        cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
        cell.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
        cell.layer.shadowOpacity = 0.9f;
        cell.layer.masksToBounds = NO;
        UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
        UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(cell.bounds, shadowInsets)];
        cell.layer.shadowPath    = shadowPath.CGPath;
        
        
        cell.analystNameLabel.text = [[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"analystName"];
        cell.companyNameLabel.text = [[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"firmName"];
        NSString * sector = [NSString stringWithFormat:@" %@",[[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"sector"]];
        NSString * sector1 = @"Sector:";
        NSString * sectorFinal = [sector1 stringByAppendingString:sector];
        cell.sectorLabel.text=sectorFinal;
        
       // NSString *ImageURL = [[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"expertPictureUrl"];
    
        //cell.profilePictureImageView.image = [UIImage imageNamed:ImageURL];
        
        
        //NSString *ImageURL = [[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"expertPictureUrl"];
        
        if([[[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"expertPictureUrl"] isEqualToString:@"_tsqr.jpg"])
        {
            NSString *ImageURL = @"https://www.tipranks.com/images/analyst-fallback_tsqr.png";
            
            @try {
                url = [NSURL URLWithString:ImageURL];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
//                            EquitiesCell1 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
//                            if (updateCell)
                                cell.profilePictureImageView.image = image;
                        });
                    }
                }
            }];
            [task resume];

            
            
            
//            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
//            cell.profilePictureImageView.image = [UIImage imageWithData:imageData];
        }else
        {
            NSString *ImageURL = [NSString stringWithFormat:@"https://az712682.vo.msecnd.net/expert-pictures/%@",[[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"expertPictureUrl"]];
            
            @try {
                url = [NSURL URLWithString:ImageURL];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
            
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            EquitiesCell1 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                            //                            if (updateCell)
                            cell.profilePictureImageView.image = image;
                        });
                    }
                }
            }];
            [task resume];
//            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
//            cell.profilePictureImageView.image = [UIImage imageWithData:imageData];
        }
        
        
        NSString * numOfStars = [[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"numOfStars"];
        float floatStar= [numOfStars floatValue];
        NSLog(@"Float Star:%f",floatStar);
        
        cell.starRatingView.minimumValue=0;
        cell.starRatingView.maximumValue=5;
        cell.starRatingView.value=floatStar;
        cell.starRatingView.allowsHalfStars=YES;
        cell.starRatingView.allowsHalfStars=YES;
        cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
        cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
        cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
        [cell.starRatingView setUserInteractionEnabled:NO];
        
        [cell.detailsButton addTarget:self action:@selector(onAllAnalystsDetailTap:) forControlEvents:UIControlEventTouchUpInside];

        return cell;

    }
    if (segmentedControl.selectedSegmentIndex==0) {
       Top25MonksTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"top" forIndexPath:indexPath];
       
        cell.detailsButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
        cell.detailsButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
        cell.detailsButton.layer.shadowOpacity = 1.0f;
        cell.detailsButton.layer.shadowRadius = 2.0f;
        cell.detailsButton.layer.cornerRadius=1.0f;
        cell.detailsButton.layer.masksToBounds = NO;
        cell.layer.shadowRadius  = 1.5f;
        cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
        cell.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
        cell.layer.shadowOpacity = 0.9f;
        cell.layer.masksToBounds = NO;
        UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
        UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(cell.bounds, shadowInsets)];
        cell.layer.shadowPath    = shadowPath.CGPath;
        
        cell.analystNameLabel.text=[[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"analystName"];
        cell.companyNameLabel.text=[[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"firmName"];
        
        
        NSString * sector = [NSString stringWithFormat:@"%@",[[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"sector"]];
        NSString * sector1 = @"Sector:";
        NSString * sectorFinal = [sector1 stringByAppendingString:sector];
        cell.sectorLabel.text=sectorFinal;
        
        NSString *nulll=[[self.topAnalysts objectAtIndex:indexPath.row]objectForKey:@"expertPictureUrl"];
        
        
        if(![nulll isEqual:[NSNull null]])
        {
            NSString *ImageURL = [NSString stringWithFormat:@"https://az712682.vo.msecnd.net/expert-pictures/%@",[[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"expertPictureUrl"]];
            
            @try {
                url = [NSURL URLWithString:ImageURL];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
            
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            EquitiesCell1 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                            //                            if (updateCell)
                            cell.PIV.image = image;
                        });
                    }
                }
            }];
            [task resume];
            

//            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
//            cell.PIV.image = [UIImage imageWithData:imageData];
        }else
        {
            NSString *ImageURL = @"https://www.tipranks.com/images/analyst-fallback_tsqr.png";
            
            @try {
                url = [NSURL URLWithString:ImageURL];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
            
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            EquitiesCell1 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                            //                            if (updateCell)
                            cell.PIV.image = image;
                        });
                    }
                }
            }];
            [task resume];
            

//            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
//            cell.PIV.image = [UIImage imageWithData:imageData];
        }
        NSString * numOfStars = [[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"numOfStars"];
        float floatStar= [numOfStars floatValue];
        NSLog(@"Float Star:%f",floatStar);
        
        cell.starRatingView.minimumValue=0;
        cell.starRatingView.maximumValue=5;
        cell.starRatingView.value=floatStar;
        cell.starRatingView.allowsHalfStars=YES;
        cell.starRatingView.allowsHalfStars=YES;
        cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
        cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
        cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
        [cell.starRatingView setUserInteractionEnabled:NO];
        
        
        
//        NSString *ImageURL = [[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"expertPictureUrl"];
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
//        cell.profileImageView.image = [UIImage imageWithData:imageData];
        
       // cell.sectorLabel.text=[[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"sector"];
        //cell.profileImageView.image=[UIImage imageNamed:[[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"expertPictureUrl"]];
        
        [cell.detailsButton addTarget:self action:@selector(onTopAnalystsDetailTap:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    if (segmentedControl.selectedSegmentIndex==2) {
        FollowingTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"following" forIndexPath:indexPath];
        
        NSString * numOfStars = [[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"numOfStars"];
        float floatStar= [numOfStars floatValue];
        NSLog(@"Float Star:%f",floatStar);
        
        cell.starRatingView.minimumValue=0;
        cell.starRatingView.maximumValue=5;
        cell.starRatingView.value=4;
        cell.starRatingView.allowsHalfStars=YES;
        cell.starRatingView.allowsHalfStars=YES;
        cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
        cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
        cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
        [cell.starRatingView setUserInteractionEnabled:NO];
        return cell;
        
    }
}
-(void)onTopAnalystsDetailTap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.monksTableView];
    NSIndexPath *indexPath = [self.monksTableView indexPathForRowAtPoint:buttonPosition];
    delegate.expertIDArray = [[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"expertUID"];
    ProfileViewController * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    [self.navigationController pushViewController:profile animated:YES];
    
}
-(void)onAllAnalystsDetailTap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.monksTableView];
    NSIndexPath *indexPath = [self.monksTableView indexPathForRowAtPoint:buttonPosition];
    delegate.expertIDArray = [[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"expertUID"];
    ProfileViewController * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    [self.navigationController pushViewController:profile animated:YES];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==1)
    {
        delegate.expertIDArray = [[self.allAnalystArray objectAtIndex:indexPath.row] objectForKey:@"expertUID"];
        ProfileViewController * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        [self.navigationController pushViewController:profile animated:YES];
        
        NSLog(@"delegate expertID:%@",delegate.expertIDArray);
    }else if (segmentedControl.selectedSegmentIndex==0)
    {
        delegate.expertIDArray = [[self.topAnalysts objectAtIndex:indexPath.row] objectForKey:@"expertUID"];
        ProfileViewController * profile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        [self.navigationController pushViewController:profile animated:YES];
        NSLog(@"delegate expertID:%@",delegate.expertIDArray);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
