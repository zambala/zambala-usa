//
//  OrderViewController1.m
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "OrderViewController1.h"
#import "OrderTableViewCell.h"

@interface OrderViewController1 ()

@end

@implementation OrderViewController1

- (void)viewDidLoad {
    [super viewDidLoad];
        self.navigationItem.title = @"Order";
    self.orderTableView.delegate=self;
    self.orderTableView.dataSource=self;
    
    self.submitOrderButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.submitOrderButton.layer.shadowOpacity = 0;
    self.submitOrderButton.layer.shadowRadius = 0;
    self.submitOrderButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    
    self.orderTypeView.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.orderTypeView.layer.shadowOpacity = 0;
    self.orderTypeView.layer.shadowRadius = 2.1;
    self.orderTypeView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    OrderTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"order" forIndexPath:indexPath];
    [[cell.orderTextButton layer] setBorderWidth:1.0f];
    [[cell.orderTextButton  layer] setBorderColor:[UIColor colorWithRed:69/255.0 green:133/255.0 blue:157/255.0 alpha:1].CGColor];
    
    return cell;
    
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
