//
//  BrokersWebViewControllerUSA.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 06/10/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrokersWebViewControllerUSA : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *brokersWebView;

@end
