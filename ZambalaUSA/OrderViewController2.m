//
//  OrderViewController2.m
//  ZambalaUSA
//
//  Created by guna on 21/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "OrderViewController2.h"
#import "ACFloatingTextField.h"

@interface OrderViewController2 ()
{
    ACFloatingTextField * textField3;
    ACFloatingTextField * textField4;
}
@end

@implementation OrderViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Order";

    
    self.submitButtonOrder.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.submitButtonOrder.layer.shadowOpacity = 0;
    self.submitButtonOrder.layer.shadowRadius = 0;
    self.submitButtonOrder.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    
    self.orderTypeView.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.orderTypeView.layer.shadowOpacity = 0;
    self.orderTypeView.layer.shadowRadius = 2.1;
    self.orderTypeView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);

    
    self.marketQTYLabel.hidden=NO;
    self.marketQTYHeight.constant=27;
    self.marketStepperView.hidden=NO;
    self.marketStepperHeight.constant=16;
    self.marketPrizeLabel.hidden=NO;
    self.marketPrizeHeight.constant=27;
    self.marketPrizeButton.hidden=NO;
    self.marketPrizeButtonHeight.constant=16;
    self.LimitQTYLabel.hidden=YES;
    self.limitQTYHeight.constant=0;
    self.limitStepperView.hidden=YES;
    self.limitStepperHeight.constant=0;
    
    [[self.marketPrizeButton layer] setBorderWidth:1.0f];
    [[self.marketPrizeButton  layer] setBorderColor:[UIColor colorWithRed:69/255.0 green:133/255.0 blue:157/255.0 alpha:1].CGColor];


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)marketButtonAction:(id)sender {
    [self.marketButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.15f]];
    [self.limitButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    [self.stopButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    
    [textField3 removeFromSuperview];
    [textField4 removeFromSuperview];
    
    
    self.marketQTYLabel.hidden=NO;
    self.marketQTYHeight.constant=27;
    self.marketStepperView.hidden=NO;
    self.marketStepperHeight.constant=16;
    self.marketPrizeLabel.hidden=NO;
    self.marketPrizeHeight.constant=27;
    self.marketPrizeButton.hidden=NO;
    self.marketPrizeButtonHeight.constant=16;
    self.LimitQTYLabel.hidden=YES;
    self.limitQTYHeight.constant=0;
    self.limitStepperView.hidden=YES;
    self.limitStepperHeight.constant=0;

    
}
- (IBAction)limitButtonAction:(id)sender {
    [self.marketButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    [self.limitButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.15f]];
    [self.stopButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    
    [textField3 removeFromSuperview];
    [textField4 removeFromSuperview];
    
    
    self.marketQTYLabel.hidden=YES;
    self.marketQTYHeight.constant=0;
    self.marketStepperView.hidden=YES;
    self.marketStepperHeight.constant=0;
    self.marketPrizeLabel.hidden=YES;
    self.marketPrizeHeight.constant=0;
    self.marketPrizeButton.hidden=YES;
    self.marketPrizeButtonHeight.constant=0;
    self.LimitQTYLabel.hidden=NO;
    self.limitQTYHeight.constant=27;
    self.limitStepperView.hidden=NO;
    self.limitStepperHeight.constant=16;
    
    
    

}
- (IBAction)stopButtonAction:(id)sender {
    [self.marketButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    [self.limitButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.0f]];
    [self.stopButtonOutlet setBackgroundColor:[UIColor colorWithRed:0/255.0 green:42/255.0 blue:58/255.0 alpha:0.15f]];
    
    
    self.marketQTYLabel.hidden=YES;
    self.marketQTYHeight.constant=0;
    self.marketStepperView.hidden=YES;
    self.marketStepperHeight.constant=0;
    self.marketPrizeLabel.hidden=YES;
    self.marketPrizeHeight.constant=0;
    self.marketPrizeButton.hidden=YES;
    self.marketPrizeButtonHeight.constant=0;
    self.LimitQTYLabel.hidden=NO;
    self.limitQTYHeight.constant=97.5;
    self.limitStepperView.hidden=NO;
    self.limitStepperHeight.constant=85;
    
    textField3=[ACFloatingTextField new];
    textField3.frame=CGRectMake(13,420,100,45);
    [textField3 setTranslatesAutoresizingMaskIntoConstraints:YES];
    textField3.placeholder=@"Trigger price";
    textField3.text=@"10";
    [textField3 setFont:[UIFont fontWithName:@"Ubuntu-Medium" size:13.5]];
    //    [textField1 setValue:[UIColor darkGrayColor] forKey:@"_placeholderLabel.textColor"];
    textField3.clearButtonMode=UITextFieldViewModeNever;
    [self.orderMainView addSubview:textField3];
    CALayer * border3=[CALayer layer];
    CGFloat borderWidth3=1;
    border3.borderColor=[UIColor whiteColor].CGColor;
    border3.frame=CGRectMake(0, textField3.frame.size.height-borderWidth3, textField3.frame.size.width, textField3.frame.size.height);
    border3.borderWidth=borderWidth3;
    [textField3.layer addSublayer:border3];
    textField3.layer.masksToBounds=YES;
    
    textField3.selectedLineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];
    textField3.placeHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField3.textColor=[UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField3.selectedPlaceHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField3.lineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];
    
    
    textField4=[ACFloatingTextField new];
    textField4.frame=CGRectMake(self.view.frame.size.width-191,420,100,45);
    [textField4 setTranslatesAutoresizingMaskIntoConstraints:YES];
    textField4.placeholder=@"Stop limit price";
    textField4.text=@"10";
    [textField4 setFont:[UIFont fontWithName:@"Ubuntu-Medium" size:13.5]];
    //    [textField1 setValue:[UIColor darkGrayColor] forKey:@"_placeholderLabel.textColor"];
    textField4.clearButtonMode=UITextFieldViewModeNever;
    [self.orderMainView addSubview:textField4];
    CALayer * border4=[CALayer layer];
    CGFloat borderWidth4=1;
    border4.borderColor=[UIColor whiteColor].CGColor;
    border4.frame=CGRectMake(0, textField4.frame.size.height-borderWidth4, textField4.frame.size.width, textField4.frame.size.height);
    border4.borderWidth=borderWidth4;
    [textField4.layer addSublayer:border4];
    textField4.layer.masksToBounds=YES;
    
    textField4.selectedLineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];
    textField4.placeHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField4.textColor=[UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField4.selectedPlaceHolderColor = [UIColor colorWithRed:56/255.0 green:52/255.0 blue:51/255.0 alpha:1.0f];
    textField4.lineColor = [UIColor colorWithRed:140/255.0 green:140/255.0 blue:140/255.0 alpha:1.0f];

    

}
@end
