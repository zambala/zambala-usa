//
//  NewOrderViewController.h
//  
//
//  Created by Zenwise Technologies on 03/10/17.
//
//

#import <UIKit/UIKit.h>

@interface NewOrderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UIView *adviceView;
@property (weak, nonatomic) IBOutlet UILabel *leaderNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateAndTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *adviceLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *bidQuantityLabel;

@property (weak, nonatomic) IBOutlet UILabel *bidPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *askQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *askPriceLabel;
@property (weak, nonatomic) IBOutlet UITextField *quantityTextField;
@property (weak, nonatomic) IBOutlet UIButton *marketButton;
@property (weak, nonatomic) IBOutlet UIButton *limitButton;
@property (weak, nonatomic) IBOutlet UIButton *stopLossButton;
@property (weak, nonatomic) IBOutlet UIView *stopLossView;
@property (weak, nonatomic) IBOutlet UIButton *submitOrderButton;
@property (weak, nonatomic) IBOutlet UIView *LimitView;
@property (weak, nonatomic) IBOutlet UIView *orderTypeView;
@property (weak, nonatomic) IBOutlet UITextField *limitTextField;
@property (weak, nonatomic) IBOutlet UITextField *stopLossTextField;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *sellButton;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewLayoutConstraint;

@property (weak, nonatomic) IBOutlet UITextField *triggerPriceText;
@property (weak, nonatomic) IBOutlet UIButton *depthButton;
@property (weak, nonatomic) IBOutlet UIView *depthView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *depthViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *openLabel;
@property (weak, nonatomic) IBOutlet UILabel *highLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowLabel;
@property (weak, nonatomic) IBOutlet UILabel *previousCloseLabel;
@property (weak, nonatomic) IBOutlet UILabel *weekHighLabel;
@property (weak, nonatomic) IBOutlet UILabel *weekLowLabel;
@property (weak, nonatomic) IBOutlet UILabel *volumeLabel;
@property (weak, nonatomic) IBOutlet UILabel *marketCapLabel;
@property (weak, nonatomic) IBOutlet UILabel *LTTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property NSMutableDictionary *ordersResponseDictionary;
@property NSString * recomondation;
@property NSString * tickerName;
@property NSString * companyName;
@property NSString * buySellCheck;
@property NSString * dateString;
@property NSString * mainOrderID,*checkOrderID,*editOrderCheck,*editQuantity,*editLimitPrice,*editOrderID;
@property NSString * etfOrderCheck;
@end
