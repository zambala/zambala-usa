//
//  SelectLoginViewController.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 30/10/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "SelectLoginViewController.h"
#import "AppDelegate.h"
#import "TabMenuView.h"
#import "UIViewController+MJPopupViewController.h"

@interface SelectLoginViewController ()

@end

@implementation SelectLoginViewController
{
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self.dummyAccountButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    [self.dummyAccountButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
     [self.realAccountButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
     [self.realAccountButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    self.dummyAccountButton.selected=YES;
    
    [self.submitButton addTarget:self action:@selector(onSubmitButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.dummyAccountButton addTarget:self action:@selector(onDummyAccountTap) forControlEvents:UIControlEventTouchUpInside];
    [self.realAccountButton addTarget:self action:@selector(onRealAccountTap) forControlEvents:UIControlEventTouchUpInside];
    if(self.dummyAccountButton.selected==YES)
    {
        self.realAccountView.hidden=YES;
        self.dummyAccountView.hidden=NO;
        self.realAccountButton.selected=NO;
        self.dummyAccountButton.selected=YES;
    }else if (self.realAccountButton.selected==YES)
    {
        self.realAccountView.hidden=NO;
        self.dummyAccountView.hidden=YES;
        self.realAccountButton.selected=YES;
        self.dummyAccountButton.selected=NO;
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view from its nib.
}

-(void)dismissKeyboard
{
    [self.userIDTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
    [self.emailIDTF resignFirstResponder];
}
-(void)loginCheckServer
{
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"%@\",\"password\":\"%@\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}",self.userIDTF.text,self.passwordTF.text];
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://api-test.choicetrade.com/"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    //    NSDictionary *cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
    //                                      @"domain.com", NSHTTPCookieDomain,
    //                                      @"\\", NSHTTPCookiePath,
    //                                      @"myCookie", NSHTTPCookieName,
    //                                      @"1234", NSHTTPCookieValue,
    //                                      nil];
    //    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    //    NSArray* cookieArray = [NSArray arrayWithObject:cookie];
    //    NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:cookieArray];
    //    [request setAllHTTPHeaderFields:headers];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        int successCheck = [[json objectForKey:@"success"] intValue];
                                                        
                                                        if(successCheck==1)
                                                        {
                                                            delegate.userID = self.userIDTF.text;
                                                            delegate.sessionID = [json objectForKey:@"session_id"];
                                                            NSLog(@"Session ID:%@",delegate.sessionID);
                                                            [self getAccountNumberServer];
                                                        }else if ([[json objectForKey:@"message"]isEqualToString:@"Error: Firm is Invalid."])
                                                        {
                                                            [self loginCheckServer];
                                                        }
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getAccountNumberServer
{
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetAccountList\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"byCust\":\"0\",\"userID\":\"%@\",\"accountMode\":\"A\"}",delegate.sessionID,delegate.userID];
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://api-test.choicetrade.com/"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        int successCheck = [[json objectForKey:@"success"] intValue];
                                                        
                                                        if(successCheck==1)
                                                        {
                                                            delegate.accountNumber = [[json objectForKey:@"accounts"] objectForKey:@"accountNo"];
                                                           
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                TabMenuView * view = [self.storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
                                                                [self presentViewController:view animated:YES completion:nil];
                                                            });
                                                        }
                                                    }
                                                }];
    [dataTask resume];
}



-(void)onSubmitButtonTap
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [self loginCheckServer];
}
-(void)onDummyAccountTap
{
    self.realAccountView.hidden=YES;
    self.dummyAccountView.hidden=NO;
    self.realAccountButton.selected=NO;
    self.dummyAccountButton.selected=YES;
}
-(void)onRealAccountTap
{
    self.realAccountView.hidden=NO;
    self.dummyAccountView.hidden=YES;
    self.realAccountButton.selected=YES;
    self.dummyAccountButton.selected=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
