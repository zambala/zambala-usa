//
//  stockDetailTableViewCell.m
//  
//
//  Created by Zenwise Technologies on 18/09/17.
//
//

#import "stockDetailTableViewCell.h"

@implementation stockDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    self.buyButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.buyButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.buyButton.layer.shadowOpacity = 1.0f;
    self.buyButton.layer.shadowRadius = 1.0f;
    self.buyButton.layer.cornerRadius=1.0f;
    self.buyButton.layer.masksToBounds = NO;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
