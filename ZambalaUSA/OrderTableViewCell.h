//
//  OrderTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface OrderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *orderTextButton;

@end
