//
//  ProfileViewController.h
//  ZambalaUSA
//
//  Created by guna on 18/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface ProfileViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *recentAdviceTableView;
@property (weak, nonatomic) IBOutlet UIButton *successRateButton;
@property (weak, nonatomic) IBOutlet UIButton *averageReturnButton;
@property (weak, nonatomic) IBOutlet UIView *performanceView;
@property (weak, nonatomic) IBOutlet UIView *stockRatingView;
@property (weak, nonatomic) IBOutlet UIView *bestStockRatingView;
@property (strong, nonatomic) IBOutlet UILabel *profileName;
@property (strong, nonatomic) IBOutlet UILabel *firmName;
@property (strong, nonatomic) IBOutlet UILabel *sectorLabel;
@property (strong, nonatomic) IBOutlet UILabel *successRateLabel;
@property (strong, nonatomic) IBOutlet UILabel *averageReturnLabel;
@property NSMutableDictionary *responseDict;
@property NSMutableArray * ltpResponseArray;
@property NSMutableArray * tickerStoreArray;

@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;

@property (strong, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UILabel *successRateDepthLabel;


@end
