//
//  MarketWatchViewController.m
//  ZambalaUSA
//
//  Created by guna on 25/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "MarketWatchViewController.h"
#import "MarketWatchTableViewCell.h"
#import "HMSegmentedControl.h"
#import "ETFCell.h"
#import "BrandsTableViewCell.h"
#import "ETFDetailViewController.h"
#import "NewOrderViewController.h"
#import "AppDelegate.h"
#import "NewStockViewController.h"

@interface MarketWatchViewController () <UIActionSheetDelegate>
{
    HMSegmentedControl * segmentedControl;
    NSMutableArray * equityCompanyArray;
   // NSMutableArray * etfCompanyArray;
   // NSMutableArray * etfCompanyDescription,*etfImageArray;
   // NSString * etfDescription1,*etfDescription2,*etfDescription3,*etfDescription4,*etfDescription5;
    AppDelegate *delegate;
   
    NSMutableString * tickerStoreString;
}

@end

@implementation MarketWatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.title = @"Market Watch";
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.addEquityName = [[NSMutableArray alloc]init];
    self.addCompanyName = [[NSMutableArray alloc]init];
    self.symbolsListArray = [[NSMutableArray alloc]init];
    self.removeSymbolsArray = [[NSMutableArray alloc]init];
    self.navigationItem.rightBarButtonItem.enabled = YES;
   // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.plusButton.enabled=YES;
    
//    etfDescription1=@"The iShares Dow Jones U.S. Aerospace & Defense ETF tracks a market-cap-weighted index of US manufacturers, assemblers and distributors of airplane and defense equipment.";
//    etfDescription2=@"The PowerShares QQQ tracks a modified-market-cap-weighted index of 100 NASDAQ-listed stocks";
//    etfDescription3=@"The Global X Social Media ETF tracks a modified market-cap-weighted index of social media companies selected by the Structured Solutions Committee";
//    etfDescription4=@"The SPDR S&P China ETF tracks a broad, cap-weighted index of investable Chinese shares. The fund's holdings stretch across all cap sizes.";
//    etfDescription5=@"The Global X Lithium & Battery Tech ETF tracks a market- cap weighted index of global lithium miners and battery producers.";
//    etfImageArray = [[NSMutableArray alloc]initWithObjects:@"ita.jpg",@"qqq.jpg",@"socl.jpg",@"gxc.jpeg",@"litnew1.jpg", nil];
//    etfCompanyDescription = [[NSMutableArray alloc]initWithObjects:etfDescription1,etfDescription2,etfDescription3,etfDescription4,etfDescription5, nil];
//    etfCompanyArray = [[NSMutableArray alloc]initWithObjects:@"ITA",@"QQQ",@"SOCL",@"GXC",@"LIT",nil];
//    self.equityTickername = [[NSMutableArray alloc]initWithObjects:@"AAPL",@"FB",@"ADBE",@"GOOGL",@"AMZN",@"BABA",@"NFLX",@"MA", nil];
//    
//    self.equityCompanyname = [[NSMutableArray alloc]initWithObjects:@"Apple Inc",@"Facebook Inc",@"Adobe",@"Alphabet Inc",@"Amazon Inc",@"AliBaba",@"Netflix Inc",@"MasterCard", nil];
    
//    if(delegate.equityTickerNameArray.count>0)
//    {
//        [delegate.equityTickerNameArray removeAllObjects];
//    }
//    if(delegate.equityCompanyNameArray.count>0)
//    {
//        [delegate.equityCompanyNameArray removeAllObjects];
//    }
//    [delegate.equityTickerNameArray addObjectsFromArray:self.equityTickername];
//    [delegate.equityCompanyNameArray addObjectsFromArray:self.equityCompanyname];
   
    self.marketWatchTableView.allowsMultipleSelectionDuringEditing = YES;
    //[self LTPServer];
    [self getAllSymbols];
    
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"EQUITY",@"ETF",@"BRANDS"]];
    segmentedControl.frame = CGRectMake(0, 30, self.view.frame.size.width, 54);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //    segmentedControl.verticalDividerEnabled = YES;
    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
//    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    segmentedControl.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    [self.sampleView addSubview:segmentedControl];
    
    [segmentedControl addTarget:self action:@selector(onSegmentedControlTap) forControlEvents:UIControlEventValueChanged];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated
{
   // tickerStoreString = [[delegate.equityTickerNameArray componentsJoinedByString:@","] mutableCopy];
    //[self LTPServer];
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.activityIndicator.hidden=YES;
        [self.activityIndicator stopAnimating];
        [self getAllSymbols];
    }
}

-(void)etflistserver
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    self.marketWatchTableView.hidden=YES;
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"e35a25b6-fe3e-6af0-ddf4-121cd4b25d62" };
    
      NSString*url = [NSString stringWithFormat:@"%@usstockinfo/etflist",delegate.zambalaEndPoint];
  
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.etfListResponseArray =[NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"ETFResponse:%@",self.etfListResponseArray);
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            self.marketWatchTableView.delegate=self;
                                                            self.marketWatchTableView.dataSource=self;
                                                            [self.marketWatchTableView reloadData];
                                                            self.activityIndicator.hidden=YES;
                                                            self.marketWatchTableView.hidden=NO;
                                                            [self.activityIndicator stopAnimating];
                                                        });
                                                        
                                                    }
                                                   
                                                }];
    [dataTask resume];
}

-(void)getAllSymbols
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
    self.marketWatchTableView.hidden=YES;
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"f173a53d-aabf-77bf-24b8-dc344273d038" };
    NSDictionary *parameters = @{ @"clientid": @"RP3997",
                                  @"action": @"list" };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://52.66.159.178:8013/api/usstockinfo/marketwatch"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.symbolsListDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"SymbolsList:%@",self.symbolsListDictionary);
                                                        if(self.symbolsListArray.count>0)
                                                        {
                                                            [self.symbolsListArray removeAllObjects];
                                                        }
                                                        if(tickerStoreString.length>0)
                                                        {
                                                            tickerStoreString=[[NSString stringWithFormat:@""] mutableCopy];
                                                        }
                                                        NSMutableArray * tickerArray = [[NSMutableArray alloc]init];
                                                        for(int i=0;i<[[self.symbolsListDictionary objectForKey:@"data"] count];i++)
                                                        {
                                                        [self.symbolsListArray addObject:[[self.symbolsListDictionary objectForKey:@"data"]objectAtIndex:i]];
                                                            
                                                            [tickerArray addObject:[[[self.symbolsListDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"symbol"]];
                                                         
                                                        }
                                                        tickerStoreString =[[tickerArray componentsJoinedByString:@","] mutableCopy];
                                                        NSLog(@"symbols list array:%@",self.symbolsListArray);
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            [self LTPServer];
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
}


-(void)onSegmentedControlTap
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        self.navigationItem.rightBarButtonItem.enabled = YES;
        self.plusButton.enabled=YES;
        self.equityView.hidden=NO;
        self.equityViewLayoutConstraint.constant=34;
        [self getAllSymbols];
        [self.marketWatchTableView reloadData];
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        [self etflistserver];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        self.plusButton.enabled=NO;
        self.equityView.hidden=YES;
        self.equityViewLayoutConstraint.constant=0;
        [self.marketWatchTableView reloadData];
    }else if (segmentedControl.selectedSegmentIndex==2)
    {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        self.plusButton.enabled=NO;
        self.equityView.hidden=YES;
        self.equityViewLayoutConstraint.constant=0;
        [self.marketWatchTableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(segmentedControl.selectedSegmentIndex==2)
    {
        return 1;
    }else if(segmentedControl.selectedSegmentIndex==0)
    {
    //return delegate.equityTickerNameArray.count;
        return self.symbolsListArray.count;
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        return self.etfListResponseArray.count;
    }
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(segmentedControl.selectedSegmentIndex==0)
    {
        
     //   static NSString *kCellID = @"market";
   //     UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
    MarketWatchTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"market" forIndexPath:indexPath];
        
        cell.tradeButton.layer.shadowOffset = CGSizeMake(1,1.7);
        cell.tradeButton.layer.shadowOpacity = 0.1;
        cell.tradeButton.layer.shadowRadius = 2.1;
        cell.tradeButton.clipsToBounds = NO;
//    cell.tickerNameLabel.text = [delegate.equityTickerNameArray objectAtIndex:indexPath.row];
//    cell.companyNameLabel.text = [delegate.equityCompanyNameArray objectAtIndex:indexPath.row]; a
        tableView.scrollEnabled=YES;
        cell.tickerNameLabel.text = [[self.symbolsListArray objectAtIndex:indexPath.row] objectForKey:@"symbol"];
        cell.companyNameLabel.text = [[self.symbolsListArray objectAtIndex:indexPath.row] objectForKey:@"longname"];
        
        NSString * ltp = [NSString stringWithFormat:@"%@",[[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"pricedata"] objectForKey:@"last"] stringValue]];
        cell.ltpLabel.text = [NSString stringWithFormat:@"%.2f",[ltp floatValue]];
        NSString * percent = @"%";
        NSString * changePercent = [NSString stringWithFormat:@"%@",[[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"pricedata"] objectForKey:@"changepercent"] stringValue]];
        NSString * change = [NSString stringWithFormat:@"%@",[[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"pricedata"] objectForKey:@"change"] stringValue]];
        
        if([change containsString:@"-"])
        {
            cell.changeLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
            cell.changeLabel.text =[NSString stringWithFormat:@"%.2f",[change floatValue]];

        }else
        {
            cell.changeLabel.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
            cell.changeLabel.text =[NSString stringWithFormat:@"%.2f",[change floatValue]];
        }
        if([changePercent containsString:@"-"])
        {
            cell.changePercentLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
            cell.changePercentLabel.text = [NSString stringWithFormat:@"%.2f%@",[changePercent floatValue],percent];
        }else
        {
            cell.changePercentLabel.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
            cell.changePercentLabel.text = [NSString stringWithFormat:@"%.2f%@",[changePercent floatValue],percent];
        }
        
        [cell.tradeButton addTarget:self action:@selector(onEquityTradeButtonTap:) forControlEvents:UIControlEventTouchUpInside];
   
    return cell;
    }
    
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        ETFCell * etf = [tableView dequeueReusableCellWithIdentifier:@"ETF" forIndexPath:indexPath];
        etf.tradeButton.layer.shadowOffset = CGSizeMake(1,1.7);
        etf.tradeButton.layer.shadowOpacity = 0.1;
        etf.tradeButton.layer.shadowRadius = 2.1;
        etf.tradeButton.clipsToBounds = NO;
        etf.etfImageView.layer.shadowOffset = CGSizeMake(1,1.7);
        etf.etfImageView.layer.shadowOpacity = 0.1;
        etf.etfImageView.layer.shadowRadius = 2.1;
        etf.etfImageView.layer.cornerRadius=etf.etfImageView.frame.size.width/2;
        etf.etfImageView.clipsToBounds = YES;
        tableView.scrollEnabled=YES;
        NSString * etfImage = [NSString stringWithFormat:@"%@",[[self.etfListResponseArray objectAtIndex:indexPath.row]objectForKey:@"logourl"]];
        
        
        NSURL *url = [NSURL URLWithString:etfImage];
        
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if (etf)
                            etf.etfImageView.image = image;
                    });
                }
            }
        }];
        [task resume];
        
        
        
        
//        UIImage * image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:etfImage]]];
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            etf.etfImageView.image = image;
//                            //[etf.etfImageView setImage:[UIImage imageWithData:data]];
//                        });

       
//         UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:etfImage]]];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//
//
//
//
//        //NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:etfImage]];
//            //set your image on main thread.
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                etf.etfImageView.image=img;
//                //[etf.etfImageView setImage:[UIImage imageWithData:data]];
//            });
//        });
        
        
        NSString * description = [NSString stringWithFormat:@"%@",[[self.etfListResponseArray objectAtIndex:indexPath.row]objectForKey:@"description"]];
        etf.etfCompanyDescription.text=description;
        NSString * etfName = [NSString stringWithFormat:@"%@",[[self.etfListResponseArray objectAtIndex:indexPath.row]objectForKey:@"etfname"]];
        etf.etfCompanyName.text = etfName;
        
        [etf.tradeButton addTarget:self action:@selector(onTradeButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        
        return etf;
    }
    
    else if (segmentedControl.selectedSegmentIndex==2)
    {
        BrandsTableViewCell * brands = [tableView dequeueReusableCellWithIdentifier:@"brandstable" forIndexPath:indexPath];
        tableView.scrollEnabled=NO;
        return brands;
    }
    
    
    return 0;
}

-(void)LTPServer
{
    
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                   @"postman-token": @"80da8702-247a-1c3e-da9b-84037d4e1429" };
        
        NSString * url = [NSString stringWithFormat:@"http://13.126.147.95/data/getQuotes.json?symbols=%@&webmasterId=89748",tickerStoreString];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            self.marketWatchResponseDict= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSLog(@"LTP details:%@",self.marketWatchResponseDict);
                                                            unsigned long a=[[[self.marketWatchResponseDict objectForKey:@"results"] objectForKey:@"quote"] count];
                                                            self.ltpResponseArray = [[NSMutableArray alloc]init];
                                                            for(int i=0;i<a;i++)
                                                            {
                                                            [self.ltpResponseArray addObject:[[[self.marketWatchResponseDict objectForKey:@"results"] objectForKey:@"quote"]objectAtIndex:i]];
                                                            }
                                                             NSLog(@"LtpArray:%@",self.ltpResponseArray);
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                self.marketWatchTableView.delegate=self;
                                                                self.marketWatchTableView.dataSource=self;
                                                                [self.marketWatchTableView reloadData];
                                                                self.activityIndicator.hidden=YES;
                                                                self.marketWatchTableView.hidden=NO;
                                                                [self.activityIndicator stopAnimating];
                                                            });
                                                        }
                                                    }];
        [dataTask resume];
    
}
-(void)onEquityTradeButtonTap:(UIButton*)sender
{
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.marketWatchTableView];
    NSIndexPath *indexPath = [self.marketWatchTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    NewOrderViewController * orders = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
//    orders.tickerName = [delegate.equityTickerNameArray objectAtIndex:indexPath.row];
//    orders.companyName = [delegate.equityCompanyNameArray objectAtIndex:indexPath.row];
//    delegate.stockTickerString=[delegate.equityTickerNameArray objectAtIndex:indexPath.row];
    
    orders.tickerName = [[[self.symbolsListDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
    orders.companyName = [[[self.symbolsListDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"longname"];
    delegate.stockTickerString = [[[self.symbolsListDictionary objectForKey:@"data"] objectAtIndex:indexPath.row] objectForKey:@"symbol"];
    [self.navigationController pushViewController:orders animated:YES];
}

-(void)onTradeButtonTap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.marketWatchTableView];
    NSIndexPath *indexPath = [self.marketWatchTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
NewOrderViewController * orders = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
//orders.recomondation =  [[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"recommendation"];

orders.tickerName = [NSString stringWithFormat:@"%@",[[self.etfListResponseArray objectAtIndex:indexPath.row]objectForKey:@"etfname"]];
orders.companyName = [NSString stringWithFormat:@"%@",[[self.etfListResponseArray objectAtIndex:indexPath.row]objectForKey:@"etfname"]];
    delegate.stockTickerString=[NSString stringWithFormat:@"%@",[[self.etfListResponseArray objectAtIndex:indexPath.row]objectForKey:@"etfname"]];



[self.navigationController pushViewController:orders animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(segmentedControl.selectedSegmentIndex==0)
    {
        return 64;
    }else if (segmentedControl.selectedSegmentIndex==1)
    {
        return 100;
    }else if (segmentedControl.selectedSegmentIndex==2)
    {
        return self.marketWatchTableView.frame.size.height;
    }
    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        if(self.marketWatchTableView.editing)
        {
            [self updateButtonsToMatchTableState];
        }else
        {
            if([[[self.symbolsListArray objectAtIndex:indexPath.row] objectForKey:@"instrumenttype"] isEqualToString:@"etf"])
        {
            NSLog(@"ETF Tapped");
            ETFDetailViewController * etfDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"ETFDetailViewController"];
            etfDetail.etfCompanyNameString = [[self.symbolsListArray objectAtIndex:indexPath.row] objectForKey:@"symbol"];
            [self.navigationController pushViewController:etfDetail animated:YES];
        }else if([[[self.symbolsListArray objectAtIndex:indexPath.row] objectForKey:@"instrumenttype"] isEqualToString:@"equity"])
        {
        NewStockViewController * stockDetailTrending = [self.storyboard instantiateViewControllerWithIdentifier:@"NewStockViewController"];
        stockDetailTrending.tickerNameString = [[self.symbolsListArray objectAtIndex:indexPath.row] objectForKey:@"symbol"];
            stockDetailTrending.priceTargetCheck=@"watch";
        [self.navigationController pushViewController:stockDetailTrending animated:YES];
        }else
        {
            NewStockViewController * stockDetailTrending = [self.storyboard instantiateViewControllerWithIdentifier:@"NewStockViewController"];
            stockDetailTrending.tickerNameString = [[self.symbolsListArray objectAtIndex:indexPath.row] objectForKey:@"symbol"];
            stockDetailTrending.priceTargetCheck=@"watch";
            [self.navigationController pushViewController:stockDetailTrending animated:YES];
        }
        }
    }
    else if(segmentedControl.selectedSegmentIndex==1)
    {
        ETFDetailViewController * etfDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"ETFDetailViewController"];
        etfDetail.etfCompanyNameString = [NSString stringWithFormat:@"%@",[[self.etfListResponseArray objectAtIndex:indexPath.row]objectForKey:@"etfname"]];
        [self.navigationController pushViewController:etfDetail animated:YES];
        NSLog(@"ETF Detail Tapped");
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.marketWatchTableView.editing)
    {
    // Update the delete button's title based on how many items are selected.
    [self updateDeleteButtonTitle];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onEditButtonTap:(id)sender {
    NSLog(@"Edit Button Clicked");
    [self.marketWatchTableView setEditing:YES animated:YES];
    [self updateButtonsToMatchTableState];
}
- (IBAction)onDeleteButtonTap:(id)sender {
    NSLog(@"Delete button Tapped");
    
        // Open a dialog with just an OK button.
        NSString *actionTitle;
        if (([[self.marketWatchTableView indexPathsForSelectedRows] count] == 1)) {
            actionTitle = NSLocalizedString(@"Are you sure you want to remove this item?", @"");
        }
        else
        {
            actionTitle = NSLocalizedString(@"Are you sure you want to remove these items?", @"");
        }
        
        NSString *cancelTitle = NSLocalizedString(@"Cancel", @"Cancel title for item removal action");
        NSString *okTitle = NSLocalizedString(@"OK", @"OK title for item removal action");
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:actionTitle
                                                                 delegate:self
                                                        cancelButtonTitle:cancelTitle
                                                   destructiveButtonTitle:okTitle
                                                        otherButtonTitles:nil];
        
        actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
        
        // Show from our table view (pops up in the middle of the table).
        [actionSheet showInView:self.view];
    
}
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(self.marketWatchTableView.editing)
//    {
//        return UITableViewCellEditingStyleDelete;
//    }
//    return UITableViewCellEditingStyleNone;
//}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // The user tapped one of the OK/Cancel buttons.
    if (buttonIndex == 0)
    {
        // Delete what the user selected.
        NSArray *selectedRows = [self.marketWatchTableView indexPathsForSelectedRows];
        BOOL deleteSpecificRows = selectedRows.count > 0;
        if (deleteSpecificRows)
        {
            // Build an NSIndexSet of all the objects to delete, so they can all be removed at once.
            NSMutableIndexSet *indicesOfItemsToDelete = [NSMutableIndexSet new];
            for (NSIndexPath *selectionIndex in selectedRows)
            {
                [indicesOfItemsToDelete addIndex:selectionIndex.row];
                
            }
            
            NSMutableArray * removeArray = [[NSMutableArray alloc]init];
            
            [self.removeSymbolsArray addObjectsFromArray:[self.symbolsListArray objectsAtIndexes:indicesOfItemsToDelete]];
            
            for (int i=0; i<self.removeSymbolsArray.count; i++) {
                [removeArray addObject:[[self.removeSymbolsArray objectAtIndex:i]objectForKey:@"symbol"]];
            }
            
           
            //Delete server
            
            
            
            NSDictionary *headers = @{ @"content-type": @"application/json",
                                       @"cache-control": @"no-cache",
                                       @"postman-token": @"6d42a302-7853-7808-185e-d08f3fdbfbf0" };
            NSDictionary *parameters = @{ @"clientid": @"RP3997",
                                          @"action": @"delete",
                                          @"symbols": removeArray };
            
            NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://52.66.159.178:8013/api/usstockinfo/marketwatch"]
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                               timeoutInterval:10.0];
            [request setHTTPMethod:@"POST"];
            [request setAllHTTPHeaderFields:headers];
            [request setHTTPBody:postData];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            if (error) {
                                                                NSLog(@"%@", error);
                                                            } else {
                                                                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                                NSLog(@"%@", httpResponse);
                                                               
                                                            }
                                                        }];
            [dataTask resume];
            
            // Delete the objects from our data model.
            [self.symbolsListArray removeObjectsAtIndexes:indicesOfItemsToDelete];
            [self.ltpResponseArray removeObjectsAtIndexes:indicesOfItemsToDelete];
            //[[[[self.marketWatchResponseDict objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:indexPath.row]
            
            // Tell the tableView that we deleted the objects
            [self.marketWatchTableView deleteRowsAtIndexPaths:selectedRows withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        else
        {
            // Delete everything, delete the objects from our data model.
            [[self.symbolsListDictionary objectForKey:@"data"] removeAllObjects];
            [self.ltpResponseArray removeAllObjects];
            
            // Tell the tableView that we deleted the objects.
            // Because we are deleting all the rows, just reload the current table section
            [self.marketWatchTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
        // Exit editing mode after the deletion.
        [self.marketWatchTableView setEditing:NO animated:YES];
        [self updateButtonsToMatchTableState];
    }
}
- (IBAction)onCancelButtonTap:(id)sender {
    NSLog(@"Cancel button Tapped");
    [self.marketWatchTableView setEditing:NO animated:YES];
    [self updateButtonsToMatchTableState];
}
- (void)updateButtonsToMatchTableState
{
    if (self.marketWatchTableView.editing)
    {
        // Show the option to cancel the edit.
        self.navigationItem.rightBarButtonItem = self.cancelButton;
        
        [self updateDeleteButtonTitle];
        
        // Show the delete button.
        self.navigationItem.leftBarButtonItem = self.deleteButton;
    }
    else
    {
        // Not in editing mode.
        //self.navigationItem.leftBarButtonItem=self.cancelButton;
        self.navigationItem.leftBarButtonItem=self.navigationItem.backBarButtonItem;
        
        // Show the edit button, but disable the edit button if there's nothing to edit.
        if ([[self.symbolsListDictionary objectForKey:@"data"]count] > 0)
        {
            self.editButton.enabled = YES;
        }
        else
        {
            self.editButton.enabled = NO;
        }
        self.navigationItem.rightBarButtonItem = self.editButton;
    }
}
- (void)updateDeleteButtonTitle
{
    // Update the delete button's title, based on how many items are selected
    NSArray *selectedRows = [self.marketWatchTableView indexPathsForSelectedRows];
    
    BOOL allItemsAreSelected = selectedRows.count == [[self.symbolsListDictionary objectForKey:@"data"]count];
    BOOL noItemsAreSelected = selectedRows.count == 0;
    
    if (allItemsAreSelected || noItemsAreSelected)
    {
        self.deleteButton.title = NSLocalizedString(@"Delete All", @"");
    }
    else
    {
        NSString *titleFormatString =
        NSLocalizedString(@"Delete (%d)", @"Title for delete button with placeholder for number");
        self.deleteButton.title = [NSString stringWithFormat:titleFormatString, selectedRows.count];
    }
}



@end
