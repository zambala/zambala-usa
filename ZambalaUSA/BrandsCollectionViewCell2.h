//
//  BrandsCollectionViewCell2.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 14/09/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandsCollectionViewCell2 : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *brandsBackView;
@property (weak, nonatomic) IBOutlet UIImageView *brands2ImageView;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *symbolNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ltpLabel;
@property (weak, nonatomic) IBOutlet UIButton *navigateButton;

@end
