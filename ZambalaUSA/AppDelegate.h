//
//  AppDelegate.h
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property NSMutableArray *expertIDArray;
@property NSString * sessionID;
@property NSString * stockTickerString;
@property NSString * orderRecomdationType;
@property NSMutableArray * mainOrdersArray;
@property NSString * userID;
@property NSString * accountNumber;
@property NSString * dummyAccountURL;
@property NSString * realAccountURL;
@property NSString * accountCheck;
@property NSMutableArray * equityTickerNameArray;
@property NSMutableArray * equityCompanyNameArray;
@property NSString *etfCheck;
@property NSString * zambalaEndPoint;
@end

