//
//  TradesViewController.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 04/12/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TradesViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITableView *tradesTableView;
@property NSString * dateString;
@property NSMutableArray * tradesResponseArray;
- (IBAction)onBackButtonTap:(id)sender;


@end
