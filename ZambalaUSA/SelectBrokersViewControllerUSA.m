//
//  SelectBrokersViewControllerUSA.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 06/10/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "SelectBrokersViewControllerUSA.h"
#import "SelectBrokersTableViewCell.h"
#import "BrokersWebViewControllerUSA.h"

@interface SelectBrokersViewControllerUSA ()

@end

@implementation SelectBrokersViewControllerUSA

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title=@"Select Broker";
    // Do any additional setup after loading the view.
    self.listofBrokersTableView.delegate=self;
    self.listofBrokersTableView.dataSource=self;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SelectBrokersTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"SelectBrokersTableViewCell" forIndexPath:indexPath];
    cell.brokerNameLabel.text = @"ChoiceTrade";
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BrokersWebViewControllerUSA * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"BrokersWebViewControllerUSA"];
    [self.navigationController pushViewController:webView animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
