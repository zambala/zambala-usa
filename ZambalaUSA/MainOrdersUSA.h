//
//  MainOrdersUSA.h
//  
//
//  Created by Zenwise Technologies on 16/10/17.
//
//

#import <UIKit/UIKit.h>

@interface MainOrdersUSA : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property NSString * orderStatusString,*dateString;
@property (weak, nonatomic) IBOutlet UIView *pendingView;
@property (weak, nonatomic) IBOutlet UIView *completedView;
@property (weak, nonatomic) IBOutlet UIView *cancelView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UITableView *mainOrdersTableView;
@property NSMutableDictionary * ordersResponseDictionary;
@property NSString *OrderID;


@end
