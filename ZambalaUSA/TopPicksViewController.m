//
//  TopPicksViewController.m
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "TopPicksViewController.h"
#import "TopPicksTableViewCell.h"
#import "NewOrderViewController.h"
#import "NewStockViewController.h"
#import <QuartzCore/QuartzCore.h>


@interface TopPicksViewController ()

@end

@implementation TopPicksViewController
{
    NSMutableString *tickerStoreString;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Top Picks";
    self.tickerStoreArray= [[NSMutableArray alloc]init];
    [[UIBarButtonItem appearance] setTintColor:[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1]];

    [self serverHit];
    
    
    // Do any additional setup after loading the view.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [[self.topPicksResponseDictionary objectForKey:@"stocks"] count];
}
-(void)serverHit
{
    self.activityIndicator.hidden=NO;
    [self.activityIndicator startAnimating];
  //  self.topPicksTableView.hidden=YES;
    
    NSDictionary *headers = @{ @"x-apikey": @"TR_Zenwise",
                               @"x-apitoken": @"8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",
                              };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.tipranks.com/api/Stocks/TopRated"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.topPicksResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"Response Dict:%@",self.topPicksResponseDictionary);
                                                        if(self.tickerStoreArray.count>0)
                                                        {
                                                            [self.tickerStoreArray removeAllObjects];
                                                        }
                                                        if(tickerStoreString.length>0)
                                                        {
                                                            tickerStoreString=[[NSString stringWithFormat:@""] mutableCopy];
                                                        }
                                                        
                                                        for (int i=0; i<[[self.topPicksResponseDictionary objectForKey:@"stocks"]count]; i++) {
                                                            [self.tickerStoreArray addObject:[[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:i] objectForKey:@"ticker"]];
                                                        }
                                                        tickerStoreString = [[self.tickerStoreArray componentsJoinedByString:@","] mutableCopy];
                                                        
                                                        NSLog(@"Ticker store array:%@",self.tickerStoreArray);
                                                        
                                                        NSLog(@"Ticker store string:%@",tickerStoreString);
                                                        

                                                        
                                                    
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
//                                                        self.activityIndicator.hidden=YES;
//                                                        [self.activityIndicator stopAnimating];
//                                                        self.topPicksTableView.hidden=NO;
//                                                        
//                                                        [self.topPicksTableView reloadData];
                                                        [self LTPServer];
                                                        
                                                        
                                                        
                                                    });
                                                }];
    [dataTask resume];
}

-(void)LTPServer
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"64a51fa5-83b5-e61d-be01-275a44103c72" };
    
    NSString * url1 = [NSString stringWithFormat:@"http://13.126.147.95/data/getQuotes.json?symbols=%@&webmasterId=89748",tickerStoreString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        
                                                        self.ltpResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"LTP:%@",self.ltpResponseDictionary);
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            self.topPicksTableView.delegate=self;
                                                            self.topPicksTableView.dataSource=self;
                                                            [self.topPicksTableView reloadData];
                                                            self.activityIndicator.hidden=YES;
                                                            [self.activityIndicator stopAnimating];
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
            TopPicksTableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:@"topPicks" forIndexPath:indexPath];
//            cell.layer.shadowOffset = CGSizeMake(0,2);
//            cell.layer.shadowOpacity = 0.1;
//            cell.layer.shadowRadius = 3.1;
//            cell.clipsToBounds = NO;
    
    cell.consesusButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    cell.consesusButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    cell.consesusButton.layer.shadowOpacity = 1.0f;
    cell.consesusButton.layer.shadowRadius = 2.0f;
    cell.consesusButton.layer.cornerRadius=1.0f;
    cell.consesusButton.layer.masksToBounds = NO;
    cell.layer.shadowRadius  = 1.5f;
    cell.layer.shadowColor   = [UIColor colorWithRed:176.f/255.f green:199.f/255.f blue:226.f/255.f alpha:1.f].CGColor;
    cell.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    cell.layer.shadowOpacity = 0.9f;
    cell.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(cell.bounds, shadowInsets)];
    cell.layer.shadowPath    = shadowPath.CGPath;
    
    cell.tickerName.text=[[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    cell.companyNameLabel.text=[[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"companyName"];
    cell.dateLabel.text = [[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"lastRatingDate"];
    NSMutableString * titleString = [[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"consensus"];
    if([titleString containsString:@"StrongBuy"])
    {
        NSString * Buy = [titleString substringFromIndex:6];
        NSLog(@"buy:%@",Buy);
        NSString * strong = [titleString substringToIndex:6];
        NSLog(@"strong:%@",strong);
        NSString * title2 = [[strong stringByAppendingString:@" "]stringByAppendingString:Buy];
        NSLog(@"Title2:%@",title2);
        [cell.consesusButton setTitle:title2 forState:UIControlStateNormal];
        
    }else
    {
        [cell.consesusButton setTitle:titleString forState:UIControlStateNormal];
    }
    
    for (int i=0; i<[[[self.ltpResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] count]; i++) {
        NSString * testString = [[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ticker"];
        NSString * testString1 = [[[[[self.ltpResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:indexPath.row] objectForKey:@"equityinfo"] objectForKey:@"shortname"];
        if([testString isEqualToString:testString1])
        {
            NSLog(@"Teststring:%@",testString);
            NSLog(@"Teststring1:%@",testString1);
            float priceTargetFloat;
            NSString * percent=@"%";
            NSString * ltp = [NSString stringWithFormat:@"%@",[[[[[self.ltpResponseDictionary objectForKey:@"results"]objectForKey:@"quote"]objectAtIndex:indexPath.row]objectForKey:@"pricedata"]objectForKey:@"last"]];
            float ltpInt = [ltp floatValue];
            cell.ltpLabel.text = [NSString stringWithFormat:@"%.2f",ltpInt];
            
            
            NSString * priceTarget = [NSString stringWithFormat:@"%@",[[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];
            float priceFloat=[priceTarget floatValue];
            
            NSString * newAvgPrice=[NSString stringWithFormat:@"%.2f",priceFloat];
            NSString * dollar = @"$";
            NSString * avgPriceFinal = [dollar stringByAppendingString:newAvgPrice];
            
            // NSLog(@"Average Target:%@",avgPriceFinal);
            cell.priceTargetLabel.text=avgPriceFinal;
            
            NSString * changePercent = [NSString stringWithFormat:@"%@",[[[[[self.ltpResponseDictionary objectForKey:@"results"]objectForKey:@"quote"]objectAtIndex:indexPath.row]objectForKey:@"pricedata"]objectForKey:@"changepercent"]];
            
            if([changePercent containsString:@"-"])
            {
                cell.changePercentLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
                 cell.changePercentLabel.text = [NSString stringWithFormat:@"%.2f%@",[changePercent floatValue],percent];
            }else
            {
                cell.changePercentLabel.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
                cell.changePercentLabel.text = [NSString stringWithFormat:@"%.2f%@",[changePercent floatValue],percent];
            }
            
           // cell.changePercentLabel.text = [NSString stringWithFormat:@"%.2f%@",[[[self.ltpResponseArray objectAtIndex:indexPath.row] objectForKey:@"percentChange"] floatValue],percent];
            
            
            //NSString * priceTarget = [NSString stringWithFormat:@"%@",[[self.analystAdviceArray objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];
            if([priceTarget isEqual:[NSNull null]]||[priceTarget isEqualToString:@"<null>"])
            {
                priceTargetFloat = 0;
            }else
            {
                priceTargetFloat = [[NSString stringWithFormat:@"%@",[[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]] floatValue];
            }
            float upsideCalculation = ((priceTargetFloat-ltpInt)/(ltpInt))*100;
            NSString * upsideString = [NSString stringWithFormat:@"%.2f",upsideCalculation];
            
            NSLog(@"Upside calculation:%f",upsideCalculation);
            NSString * percent1 = @"%";
            
            if([upsideString containsString:@"-"])
            {
                if(priceTargetFloat==0)
                {
                    cell.upsideLabel.text = @"0";
                }
                else
                {
                    cell.upsideLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
                    cell.upsideLabel.text =[NSString stringWithFormat:@"%.2f%@",upsideCalculation,percent1];
                }
            }else
            {
                if(priceTargetFloat==0)
                {
                    cell.upsideLabel.text= @"0";
                }else
                {
                    cell.upsideLabel.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
                    cell.upsideLabel.text =[NSString stringWithFormat:@"%.2f%@",upsideCalculation,percent1];
                }
            }
            
            
        }
    }
   

    [cell.consesusButton addTarget:self action:@selector(onBuyButtontap:) forControlEvents:UIControlEventTouchUpInside];
    
            return cell;
}


-(void)onBuyButtontap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.topPicksTableView];
    NSIndexPath *indexPath = [self.topPicksTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    NewOrderViewController * orders = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    orders.recomondation =  [[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"consensus"];
    orders.tickerName = [[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    orders.companyName = [[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"companyName"];
    [self.navigationController pushViewController:orders animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewStockViewController * stockDetailTrending = [self.storyboard instantiateViewControllerWithIdentifier:@"NewStockViewController"];
    stockDetailTrending.tickerNameString = [[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    NSString * priceTarget = [NSString stringWithFormat:@"%@",[[[self.topPicksResponseDictionary objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];
    if([priceTarget isEqualToString:@"<null>"]||[priceTarget isEqual:[NSNull null]])
    {
        stockDetailTrending.priceTargetString=@"0";
    }else
    {
        stockDetailTrending.priceTargetString=priceTarget;
    }
    [self.navigationController pushViewController:stockDetailTrending animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return 89;
    }
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10; // you can have your own choice, of course
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
