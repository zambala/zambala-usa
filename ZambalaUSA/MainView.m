//
//  MainView.m
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "MainView.h"
#import <QuartzCore/QuartzCore.h>
#import "Mixpanel/Mixpanel.h"
#import "AppDelegate.h"

@interface MainView ()

@end

@implementation MainView
{
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
   // mixpanel//
    
    
   
    
    NSDictionary *barButtonAppearanceDict = @{NSFontAttributeName : [UIFont fontWithName:@"Ubuntu" size:14.6], NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255.0 green:205.0/255.0 blue:3.0/255.0 alpha:1.0f]};
    [self.USMarketBarButton setTitleTextAttributes:barButtonAppearanceDict forState:UIControlStateNormal];
    NSDictionary *barButtonAppearanceDict1 = @{NSFontAttributeName : [UIFont fontWithName:@"Ubuntu-Medium" size:20.8], NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255.0 green:205.0/255.0 blue:3.0/255.0 alpha:1.0f]};
    [self.zambalaBarButton setTitleTextAttributes:barButtonAppearanceDict1 forState:UIControlStateNormal];
    
    
//    self.marketMonksButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
//    self.marketMonksButton.layer.shadowOpacity = 3;
//    self.marketMonksButton.layer.shadowRadius = 1;
//    self.marketMonksButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
//    
//    
//    self.wisdomGardenButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
//    self.wisdomGardenButton.layer.shadowOpacity = 3;
//    self.wisdomGardenButton.layer.shadowRadius = 1;
//    self.wisdomGardenButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
//    
//    self.marketWatchButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
//    self.marketWatchButton.layer.shadowOpacity = 3;
//    self.marketWatchButton.layer.shadowRadius = 1;
//    self.marketWatchButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
//    
//    //to be uncommented
////    self.openAccountButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
////    self.openAccountButton.layer.shadowOpacity = 3;
////    self.openAccountButton.layer.shadowRadius = 1;
////    self.openAccountButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
//    
//    
//    self.premiumServiceButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
//    self.premiumServiceButton.layer.shadowOpacity = 3;
//    self.premiumServiceButton.layer.shadowRadius = 1;
//    self.premiumServiceButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
//    
//    self.indianButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
//    self.indianButton.layer.shadowOpacity = 3;
//    self.indianButton.layer.shadowRadius = 1;
//    self.indianButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);

    ///////////////
    
    
    
    
    self.tickerImageView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.tickerImageView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.tickerImageView.layer.shadowOpacity = 1.0f;
    self.tickerImageView.layer.shadowRadius = 1.0f;
    self.tickerImageView.layer.cornerRadius=1.0f;
    self.tickerImageView.layer.masksToBounds = NO;
    
    self.marketMonksButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.marketMonksButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.marketMonksButton.layer.shadowOpacity = 1.0f;
    self.marketMonksButton.layer.shadowRadius = 1.0f;
    self.marketMonksButton.layer.cornerRadius=1.0f;
    self.marketMonksButton.layer.masksToBounds = NO;
    
    
    
    self.wisdomGardenButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.wisdomGardenButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.wisdomGardenButton.layer.shadowOpacity = 1.0f;
    self.wisdomGardenButton.layer.shadowRadius = 1.0f;
    self.wisdomGardenButton.layer.cornerRadius=1.0f;
    self.wisdomGardenButton.layer.masksToBounds = NO;
    
    self.marketWatchButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.marketWatchButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.marketWatchButton.layer.shadowOpacity = 1.0f;
    self.marketWatchButton.layer.shadowRadius = 1.0f;
    self.marketWatchButton.layer.cornerRadius=1.0f;
    self.marketWatchButton.layer.masksToBounds = NO;
    
    self.premiumServiceButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.premiumServiceButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.premiumServiceButton.layer.shadowOpacity = 1.0f;
    self.premiumServiceButton.layer.shadowRadius = 1.0f;
    self.premiumServiceButton.layer.cornerRadius=1.0f;
    self.premiumServiceButton.layer.masksToBounds = NO;
    
    self.openAccountButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.openAccountButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.openAccountButton.layer.shadowOpacity = 1.0f;
    self.openAccountButton.layer.shadowRadius = 1.0f;
    self.openAccountButton.layer.cornerRadius=1.0f;
    self.openAccountButton.layer.masksToBounds = NO;
    
    self.indianButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.indianButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.indianButton.layer.shadowOpacity = 1.0f;
    self.indianButton.layer.shadowRadius = 1.0f;
    self.indianButton.layer.cornerRadius=1.0f;
    self.indianButton.layer.masksToBounds = NO;
    [self nasdaqServer];


    // Do any additional setup after loading the view.
}

-(void)nasdaqServer
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"80da8702-247a-1c3e-da9b-84037d4e1429" };
    
    NSString * url = [NSString stringWithFormat:@"http://13.126.147.95/data/getQuotes.json?symbols=%@&webmasterId=89748",@"^NDX,^NASD"];
    url = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        self.nasdaqResponseDictionary= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"NASDAQ:%@",self.nasdaqResponseDictionary);
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            self.nasdaqLTPLabel.text =[NSString stringWithFormat:@"%.2f",[[[[[[self.nasdaqResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"last"] floatValue]];
                                                            NSString * percent =@"%";
                                                            NSString * changePercent = [NSString stringWithFormat:@"%@",[[[[[[self.nasdaqResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"changepercent"] stringValue]];
                                                            if([changePercent containsString:@"-"])
                                                            {
                                                                self.nasdaqChangePercentLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
                                                                self.nasdaqChangePercentLabel.text =[NSString stringWithFormat:@"%.2f%@",[changePercent floatValue],percent];
                                                                
                                                            }else
                                                            {
                                                                self.nasdaqChangePercentLabel.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
                                                                self.nasdaqChangePercentLabel.text =[NSString stringWithFormat:@"%.2f%@",[changePercent floatValue],percent];
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                            self.nasdaqCompositeLTPLabel.text =[NSString stringWithFormat:@"%.2f",[[[[[[self.nasdaqResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:1] objectForKey:@"pricedata"] objectForKey:@"last"] floatValue]];
                                                            
                                                            NSString * compositeChangePercent = [NSString stringWithFormat:@"%@",[[[[[[self.nasdaqResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:1] objectForKey:@"pricedata"] objectForKey:@"changepercent"] stringValue]];
                                                            if([compositeChangePercent containsString:@"-"])
                                                            {
                                                                self.nasdaqCompositeChangePercentLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
                                                                self.nasdaqCompositeChangePercentLabel.text =[NSString stringWithFormat:@"%.2f%@",[compositeChangePercent floatValue],percent];
                                                                
                                                            }else
                                                            {
                                                                self.nasdaqCompositeChangePercentLabel.textColor = [UIColor colorWithRed:(27.0)/255 green:(160.0)/255 blue:(33.0)/255 alpha:1];
                                                                self.nasdaqCompositeChangePercentLabel.text =[NSString stringWithFormat:@"%.2f%@",[compositeChangePercent floatValue],percent];
                                                            }
                                                            
                                                            
//                                                            self.nasdaqChangePercentLabel.text=[NSString stringWithFormat:@"%.2f",[[[[[[self.nasdaqResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedate"] objectForKey:@"changepercent"] floatValue]];
                                                            
                                                
                                                            
                                                        });
                                                    }
                                                }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
