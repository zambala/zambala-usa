//
//  BrandsTableViewCell.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 14/09/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "BrandsTableViewCell.h"
#import "BrandsCollectionViewCell1.h"
#import "BrandsCollectionViewCell2.h"
#import "NewStockViewController.h"
#import "AppDelegate.h"
#import "NewOrderViewController.h"

@implementation BrandsTableViewCell
{
    NSMutableArray * companyNamesArray;
    NSMutableArray * symbolsArray;
    NSMutableArray * imagesArray;
    NSMutableString * symbolsString;
    NSMutableArray *Json;
    AppDelegate * delegate;
    NSString * catergoryNameString;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    companyNamesArray = [[NSMutableArray alloc]initWithObjects:@"Apple",@"Adobe",@"Alphabet",@"Amazon",@"Blackberry",@"Twitter", nil];
    symbolsArray = [[NSMutableArray alloc]initWithObjects:@"AAPL",@"ADBE",@"GOOGL",@"AMZN",@"FB",@"TWTR", nil];
    symbolsString = [[NSMutableString alloc]init];
    
    imagesArray = [[NSMutableArray alloc]initWithObjects:@"aapl.png",@"adbe.png",@"alpha.png",@"amzn.png",@"bbry.png",@"twtr.png", nil];
    
    NSIndexPath *indexPathForFirstRow = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.brandsCollectionView selectItemAtIndexPath:indexPathForFirstRow animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    //[self collectionView:self.brandsCollectionView didSelectItemAtIndexPath:indexPathForFirstRow];
    [self.brandsCollectionView selectItemAtIndexPath:indexPathForFirstRow animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    [self categoriesServer];
    
   
    
    

}

-(void)ltpServerHit
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"f058642e-d034-ecd2-1d5d-946928aa5c25" };
    
    NSString * url = [NSString stringWithFormat:@"http://trexternalapi-preprod2.azurewebsites.net/api/stocks/prices/%@?X-APIKey=TR_Zenwise&X-APIToken=8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",symbolsString];
    
    NSLog(@"url:%@",url);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        Json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"Json:%@",Json);
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                          //  self.brandsCollectionView.delegate=self;
                                                          //  self.brandsCollectionView.dataSource=self;
                                                            self.brandsCollectionView2.delegate=self;
                                                            self.brandsCollectionView2.dataSource=self;
                                                            //[self.brandsCollectionView reloadData];
                                                            [self.brandsCollectionView2 reloadData];

                                                        });

                                                        
                                                        
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)categoriesServer
{
        NSDictionary *headers = @{ @"cache-control": @"no-cache",
                                   };
        
        NSString * url = [NSString stringWithFormat:@"%@usstockinfo/categories",delegate.zambalaEndPoint];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        [request setAllHTTPHeaderFields:headers];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            self.categoriesResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                            NSLog(@"Categories:%@",self.categoriesResponseArray);
                                                            catergoryNameString = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:0]objectForKey:@"category"]];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                 self.brandsCollectionView.delegate=self;
                                                                self.brandsCollectionView.dataSource=self;
                                                                [self.brandsCollectionView reloadData];
                                                                
                                                            });
                                                            [self categoriesListServer];
                                                        }
                                                       
                                                    }];
        [dataTask resume];
   
}
-(void)categoriesListServer
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               };
    
    NSString * url = [NSString stringWithFormat:@"%@usstockinfo/brands?category=%@&limit=1000",delegate.zambalaEndPoint,catergoryNameString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                        self.categoriesListResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"Categories list:%@",self.categoriesListResponseArray);
                                                        
                                                        NSMutableArray * symbolArray = [[NSMutableArray alloc] init];
                                                        for (int i=0; i<self.categoriesListResponseArray.count; i++) {
                                                            [symbolArray addObject:[[self.categoriesListResponseArray objectAtIndex:i]objectForKey:@"symbol"]];
                                                        }
                                                        symbolsString = [[symbolArray componentsJoinedByString:@","] mutableCopy];
                                                        NSLog(@"SymbolsString:%@",symbolsString);
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            //                                                        self.brandsCollectionView.delegate=self;
                                                            //                                                        self.brandsCollectionView2.dataSource=self;
                                                            [self ltpServerHit];
                                                            
                                                        });
                                                       
                                                    }
                                                   
                                                }];
    [dataTask resume];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == self.brandsCollectionView)
    {
    return self.categoriesResponseArray.count;
    }else if (collectionView == self.brandsCollectionView2)
    {
        return self.categoriesListResponseArray.count;
    }
    
    return 0;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == self.brandsCollectionView)
    {
        BrandsCollectionViewCell1 *brands1 = [collectionView dequeueReusableCellWithReuseIdentifier:@"brands1" forIndexPath:indexPath];
        brands1.contentView.layer.cornerRadius = 2.0f;
        brands1.contentView.layer.borderWidth = 1.0f;
        brands1.contentView.layer.borderColor = [UIColor clearColor].CGColor;
        brands1.contentView.layer.masksToBounds = YES;
        
        brands1.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        brands1.layer.shadowOffset = CGSizeMake(0, 2.0f);
        brands1.layer.shadowRadius = 3.2f;
        brands1.layer.shadowOpacity = 1.0f;
        brands1.layer.masksToBounds = NO;
        brands1.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:brands1.bounds cornerRadius:brands1.contentView.layer.cornerRadius].CGPath;
        NSString * capital;
        NSString * category = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:indexPath.row]objectForKey:@"category"]];
        capital = [category stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                                  withString:[[category substringToIndex:1] capitalizedString]];
        
        
        brands1.titleLabel.text = capital;
        
        NSString * etfImage = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:indexPath.row]objectForKey:@"categoryurl"]];
        NSURL *url = [NSURL URLWithString:etfImage];
        
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if (brands1)
                            brands1.titleImageView.image = image;
                    });
                }
            }
        }];
        [task resume];
        return brands1;
    }else if (collectionView == self.brandsCollectionView2)
    {
        BrandsCollectionViewCell2 * brands2 = [collectionView dequeueReusableCellWithReuseIdentifier:@"brands2" forIndexPath:indexPath];
        brands2.contentView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.18] CGColor];

        brands2.brandsBackView.layer.shadowOffset = CGSizeMake(0, 2.0f);
        
        brands2.brandsBackView.layer.shadowOpacity = 2.0f;
        
        brands2.brandsBackView.layer.shadowRadius = 3.2f;
        
        brands2.brandsBackView.layer.cornerRadius=2.1f;
        
        brands2.brandsBackView.layer.masksToBounds = YES;
        brands2.brandsBackView.layer.borderColor =  [[UIColor colorWithRed:247/255 green:247/255 blue:247/255 alpha:0.18] CGColor];

        brands2.brandsBackView.layer.borderWidth = 1.0f;
         brands2.brandsBackView.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:brands2.bounds cornerRadius:brands2.contentView.layer.cornerRadius].CGPath;
        
        NSString * categoryImage = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"logourl"]];
        
    
        NSURL *url = [NSURL URLWithString:categoryImage];
        
        NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (data) {
                UIImage *image = [UIImage imageWithData:data];
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if (brands2)
                            brands2.brands2ImageView.image = image;
                    });
                }
            }
        }];
        [task resume];
        
       // brands2.brands2ImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[imagesArray objectAtIndex:indexPath.row]]];
        brands2.symbolNameLabel.text = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
        brands2.companyNameLabel.text = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"name"]];
        brands2.ltpLabel.text = [NSString stringWithFormat:@"%@",[[Json objectAtIndex:indexPath.row] objectForKey:@"price"]];
        [brands2.navigateButton addTarget:self action:@selector(onNavigateButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        
//        brands2.contentView.layer.cornerRadius = 2.0f;
//        brands2.contentView.layer.borderWidth = 1.0f;
//        brands2.contentView.layer.borderColor = [UIColor clearColor].CGColor;
//        brands2.contentView.layer.masksToBounds = YES;
//        //brands2.contentView.layer.borderColor =  [[UIColor colorWithRed:247/255 green:247/255 blue:247/255 alpha:0.18] CGColor];
//        
//       // brands2.layer.shadowColor = [UIColor lightGrayColor].CGColor;
//        brands2.layer.shadowOffset = CGSizeMake(0, 2.0f);
//        brands2.layer.shadowRadius = 3.2f;
//        brands2.layer.shadowOpacity = 1.0f;
//        brands2.layer.masksToBounds = NO;
//        brands2.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:brands2.bounds cornerRadius:brands2.contentView.layer.cornerRadius].CGPath;
        
        return brands2;
    }
        
    return 0;
}
-(void)onNavigateButtonTap:(UIButton*)sender
{
   
    UIViewController *viewController = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    if ( viewController.presentedViewController && !viewController.presentedViewController.isBeingDismissed ) {
        viewController = viewController.presentedViewController;
    }
    
//   // NSLayoutConstraint *constraint = [NSLayoutConstraint
//                                      constraintWithItem:alert.view
//                                      attribute:NSLayoutAttributeHeight
//                                      relatedBy:NSLayoutRelationLessThanOrEqual
//                                      toItem:nil
//                                      attribute:NSLayoutAttributeNotAnAttribute
//                                      multiplier:1
//                                      constant:viewController.view.frame.size.height*2.0f];
//
   // [alert.view addConstraint:constraint];
   
    
    
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.brandsCollectionView2];
    NSIndexPath *indexPath = [self.brandsCollectionView2 indexPathForItemAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    NewOrderViewController * trendingOrders = [storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    delegate.stockTickerString = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
   // delegate.orderRecomdationType = [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"rating"];
  //  trendingOrders.recomondation =   [[[self.responseDict objectForKey:@"stocks"] objectAtIndex:indexPath.row] objectForKey:@"rating"];
    
    trendingOrders.tickerName = [NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"symbol"]];
    trendingOrders.companyName =[NSString stringWithFormat:@"%@",[[self.categoriesListResponseArray objectAtIndex:indexPath.row]objectForKey:@"name"]];
    trendingOrders.etfOrderCheck=@"etf";
    
    //[viewController.navigationController pushViewController:trendingOrders animated:YES];
     [viewController presentViewController:trendingOrders animated:YES completion:^{}];
    
//    [self presentViewController:trendingOrders
//                       animated:YES
//                     completion:nil];
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == self.brandsCollectionView)
    {
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        catergoryNameString = [NSString stringWithFormat:@"%@",[[self.categoriesResponseArray objectAtIndex:indexPath.row] objectForKey:@"category"]];
        cell.backgroundColor = [UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
        [self categoriesListServer];
        [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    }else if (collectionView == self.brandsCollectionView2)
    {
       
    }
}


- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView==self.brandsCollectionView)
    {
        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
    }
}
//- (void)collectionView:(UICollectionView *)collectionView didhighlightItemAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    if(collectionView==self.brandsCollectionView)
//    {
//        UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//       cell.contentView.backgroundColor = [UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
//        [collectionView deselectItemAtIndexPath:indexPath animated:NO];
//
//
//    }
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(collectionView == self.brandsCollectionView)
//    {
//    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    cell.backgroundColor = [UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
//
//    }
//}
//

//- (CGSize)collectionView:(UICollectionView *)collectionView
//                  layout:(UICollectionViewLayout*)collectionViewLayout
//  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake(self.brandsCollectionView.frame.size.width, 80);
//    //return CGSizeMake(50, 50);
//}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:YES];

    // Configure the view for the selected state
}

@end
