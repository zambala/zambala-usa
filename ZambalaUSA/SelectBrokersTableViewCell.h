//
//  SelectBrokersTableViewCell.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 06/10/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectBrokersTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *brokerImageView;
@property (weak, nonatomic) IBOutlet UILabel *brokerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *brokerDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *moreDetailsButton;

@end
