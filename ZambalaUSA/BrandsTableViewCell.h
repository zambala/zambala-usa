//
//  BrandsTableViewCell.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 14/09/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandsTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *brandsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *brandsCollectionView2;
@property NSMutableArray * categoriesResponseArray;
@property NSMutableArray * categoriesListResponseArray;

@end
