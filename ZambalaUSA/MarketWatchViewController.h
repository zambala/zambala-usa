//
//  MarketWatchViewController.h
//  ZambalaUSA
//
//  Created by guna on 25/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketWatchViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *marketWatchTableView;

@property (weak, nonatomic) IBOutlet UIView *equityView;

@property (weak, nonatomic) IBOutlet UIView *sampleView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *equityViewLayoutConstraint;
@property NSMutableDictionary *marketWatchResponseDict;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *plusButton;
//@property NSMutableArray * equityTickername;
//@property NSMutableArray * equityCompanyname;
@property NSMutableArray * addEquityName;
@property NSMutableArray * addCompanyName;
@property NSString * etfCheck;
@property NSMutableArray * etfListResponseArray;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
- (IBAction)onEditButtonTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *deleteButton;
- (IBAction)onDeleteButtonTap:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelButton;
- (IBAction)onCancelButtonTap:(id)sender;
@property NSMutableDictionary * symbolsListDictionary;
@property NSMutableArray * ltpResponseArray;
@property NSMutableArray * symbolsListArray;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property NSMutableArray * removeSymbolsArray;

@end
