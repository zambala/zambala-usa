//
//  NewSelectLoginViewController.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 30/10/17.
//  Copyright © 2017 Sandeep Maganti. All rights reserved.
//

#import <UIKit/UIKit.h>
/*!
 @author Sandeep Maganti
 @class NewSelectLoginViewController
 @file NewSelectLoginViewController
 @brief The Class is used to select the type of login as per the user preference.
 
@discussion This class has two options @b Virtual Account @@b and @b Real Account @@b. @b Virtual Account @@b is a free account user just need to enter email id. @b Real Accunt @@b is the actual account of the user linked with the respective broker.
 @superclass SuperClass: UIViewController\n
 @classdesign    Designed using UIView, UITextfield.
 @coclass    AppDelegate
 @helps  Helps MainView.m.
 @helper    No helper exists for this class.
 */


@interface NewSelectLoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *popUPView;
@property (weak, nonatomic) IBOutlet UIView *realAccountView;
@property (weak, nonatomic) IBOutlet UIButton *dummyAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *realAccountButton;
@property (weak, nonatomic) IBOutlet UITextField *userIDTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIView *dummyAccountView;
@property (weak, nonatomic) IBOutlet UITextField *emailIDTF;
- (void)showInView:(UIView *)aView animated:(BOOL)animated;
@property NSString * dummyUserID;
@property NSString *dummyPassword;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property NSString * fromWhere;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@end
