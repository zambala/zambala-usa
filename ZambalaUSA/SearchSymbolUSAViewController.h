//
//  SearchSymbolUSAViewController.h
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 09/11/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchSymbolUSAViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *segmentView;
@property (weak, nonatomic) IBOutlet UIView *typeOfSearchView;
@property (weak, nonatomic) IBOutlet UIView *textFieldView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *equityButton;
@property (weak, nonatomic) IBOutlet UIButton *etfButton;
@property (weak, nonatomic) IBOutlet UIButton *symbolButton;
@property (weak, nonatomic) IBOutlet UIButton *nameButton;
@property (weak, nonatomic) IBOutlet UITextField *symbolTF;
@property NSMutableArray * symbols;
@property NSMutableArray * company;
@property NSMutableArray * searchResponseArray;
@property NSMutableArray * selectedCompanyArray;
@property NSMutableString * tickerNamesString;
@property BOOL successCheck;
@property NSMutableDictionary * addResponseDict;

@end
