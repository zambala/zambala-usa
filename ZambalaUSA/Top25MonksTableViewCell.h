//
//  Top25MonksTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface Top25MonksTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *analystNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *sectorLabel;
@property (strong, nonatomic) IBOutlet UIImageView *PIV;
@property (strong, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UIButton *detailsButton;

@end
