//
//  OrderViewController.h
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *orderTypeView;
@property (weak, nonatomic) IBOutlet UIView *orderValueView;
@property (weak, nonatomic) IBOutlet UIButton *submitOrderButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sumbitOrderButtonHeight;
- (IBAction)marketButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *marketButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *limitButtonOutlet;
- (IBAction)limitButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *adviceView;

- (IBAction)stopLossButtonAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *stopLossButtonOutlet;
@property (weak, nonatomic) IBOutlet UIView *orderMainView;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *buySellSegment;

@property (weak, nonatomic) IBOutlet UILabel *tickerNameLabel;
@property NSString * recomondation;
@property NSString * tickerName;
@property NSString * companyName;
 @property NSString * buySellCheck;

@end
