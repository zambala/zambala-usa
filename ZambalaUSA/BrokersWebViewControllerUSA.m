//
//  BrokersWebViewControllerUSA.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 06/10/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "BrokersWebViewControllerUSA.h"

@interface BrokersWebViewControllerUSA ()

@end

@implementation BrokersWebViewControllerUSA

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *link = @"https://directtrack.choicetrade.com/redirect/2/ZMBA";
    NSURL *url=[NSURL URLWithString:link];
    
    NSURLRequest *urlReq=[NSURLRequest requestWithURL: url];
    NSLog(@"URl:%@",urlReq);
    [self.brokersWebView loadRequest:urlReq];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
