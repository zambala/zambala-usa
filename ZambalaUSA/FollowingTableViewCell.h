//
//  FollowingTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface FollowingTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet HCSStarRatingView *starRatingView;

@end
