//
//  OpenAccountViewController.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 13/11/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "OpenAccountViewController.h"
#import "BrokersWebViewControllerUSA.h"

@interface OpenAccountViewController ()

@end

@implementation OpenAccountViewController
{
    NSString * typeCheck;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nextButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.nextButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.nextButton.layer.shadowOpacity = 1.0f;
    self.nextButton.layer.shadowRadius = 1.0f;
    self.nextButton.layer.cornerRadius=1.0f;
    self.nextButton.layer.masksToBounds = NO;
    
    
    self.enterEmailLabel.hidden=YES;
    self.enterEmailHeight.constant=0;
    self.emailTF.hidden=YES;
    self.emailTFHeight.constant=0;
    [self.virtualAccountButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    [self.virtualAccountButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];

    
    [self.openAccountButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    [self.openAccountButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    typeCheck=@"Real";
    
    self.virtualAccountButton.selected=NO;
    self.openAccountButton.selected=NO;
    
    [self.virtualAccountButton addTarget:self action:@selector(onVirtualButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.openAccountButton addTarget:self action:@selector(onRealButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.nextButton addTarget:self action:@selector(onNextButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.alreadyHaveButton addTarget:self action:@selector(onAlreadyHaveButton) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}
-(void)dismissKeyboard
{
    [self.emailTF resignFirstResponder];
}
-(void)onVirtualButtonTap
{
    [self.nextButton setTitle:@"Create" forState:UIControlStateNormal];
    self.enterEmailLabel.hidden=NO;
    self.enterEmailHeight.constant=22;
    self.virtualAccountButton.selected=YES;
    self.openAccountButton.selected=NO;
    self.emailTF.hidden=NO;
    self.emailTFHeight.constant=30;
    typeCheck=@"Virtual";
}
-(void)onRealButtonTap
{
    [self.nextButton setTitle:@"Next" forState:UIControlStateNormal];
    self.enterEmailLabel.hidden=YES;
    self.enterEmailHeight.constant=0;
    self.virtualAccountButton.selected=NO;
    self.openAccountButton.selected=YES;
    self.emailTF.hidden=YES;
    self.emailTFHeight.constant=0;
    typeCheck=@"Real";
}
-(void)onNextButtonTap
{
    if([typeCheck isEqualToString:@"Real"])
    {
    BrokersWebViewControllerUSA * webView = [self.storyboard instantiateViewControllerWithIdentifier:@"BrokersWebViewControllerUSA"];
    [self.navigationController pushViewController:webView animated:YES];
    }else if ([typeCheck isEqualToString:@"Virtual"])
    {
        
    }
}
-(void)onAlreadyHaveButton
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
