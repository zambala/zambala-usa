//
//  NewOrderViewController.m
//  
//
//  Created by Zenwise Technologies on 03/10/17.
//
//

#import "NewOrderViewController.h"
#import "AppDelegate.h"
#import "MainOrdersUSA.h"
#import "NewSelectLoginViewController.h"
#import "MarketWatchViewController.h"

@interface NewOrderViewController ()
{
    AppDelegate * delegate;
    NSString * orderTypeCheck;
    NSString * str;
    BOOL depthCheck;
    NSMutableDictionary * depthResponseDictionary;
    NSString * bidPrice,*bidQuantity,*askPrice,*askQuantity,*openString,*lowString,*highString,*previousCloseString,*weekHighString,*weekLowString,*volumeString,*marketCapString,*LTTmeString;
    NSString *finalDate;

}
@end

@implementation NewOrderViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationItem.title = @"Order";
    self.topView.hidden=YES;
    self.topViewLayoutConstraint.constant=0;
    orderTypeCheck=@"1";
    self.submitOrderButton.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.submitOrderButton.layer.shadowOpacity = 0;
    self.submitOrderButton.layer.shadowRadius = 0;
    self.submitOrderButton.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    
    self.orderTypeView.layer.shadowColor = [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.12f] CGColor];
    self.orderTypeView.layer.shadowOpacity = 0;
    self.orderTypeView.layer.shadowRadius = 2.1;
    self.orderTypeView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    
    self.LimitView.hidden=YES;
    self.stopLossView.hidden=YES;
    [self.sellButton.layer setBorderWidth:1.0f];
    [self.sellButton.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7]CGColor]];
    
    [self.sellButton setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1] forState:UIControlStateNormal];
    
    
    [self.buyButton.layer setBorderWidth:1.0f];
    [self.buyButton.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
    
    [self.buyButton setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
    
    
    self.depthView.hidden=YES;
    self.depthViewHeightConstraint.constant=0;
    
    depthCheck=true;
    
    if([self.editOrderCheck isEqualToString:@"edit"])
    {
        [self onLimitButtonTap];
        self.quantityTextField.text=self.editQuantity;
        self.limitTextField.text=self.editLimitPrice;
        self.topView.hidden=NO;
        self.topViewLayoutConstraint.constant=57;
    }else if ([self.etfOrderCheck isEqualToString:@"etf"])
    {
        self.topView.hidden=NO;
        self.topViewLayoutConstraint.constant=57;
    }
    
    [self.depthButton addTarget:self action:@selector(onDepthButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.backButton addTarget:self action:@selector(onBackButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
   // self.marketButton.selected = YES;
    
    [self depthServer];
    [self UIUpdate];
    
    if([self.recomondation isEqualToString:@"Buy"])
    {
        self.buySellCheck=@"1";
       // self.buyButton.selected=YES;
       // self.buySellSegment.tintColor=[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1];
        
        
        
        
        [self.sellButton.layer setBorderWidth:1.0f];
        
        [self.sellButton.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7] CGColor]];
        
        [self.sellButton setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]  forState:UIControlStateNormal];
        
        
        [self.buyButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
         [self.buyButton setTitleColor:[UIColor colorWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:1]  forState:UIControlStateNormal];

        
    }else if([self.recomondation isEqualToString:@"Sell"])
    {
        self.buySellCheck=@"2";
       // self.sellButton.selected=YES;
        [self.sellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
        [self.sellButton setTitleColor:[UIColor colorWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:1]  forState:UIControlStateNormal];
        
        [self.buyButton.layer setBorderWidth:1.0f];
        [self.buyButton.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
        
        [self.buyButton setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
    }else if ([self.recomondation isEqualToString:@"StrongBuy"]||[self.recomondation isEqualToString:@"Strong Buy"])
    {
        self.buySellCheck=@"1";
        //self.buyButton.selected=YES;
        
        [self.sellButton.layer setBorderWidth:1.0f];
        
        [self.sellButton.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7] CGColor]];
        
        [self.sellButton setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]  forState:UIControlStateNormal];
        
        
        [self.buyButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
        [self.buyButton setTitleColor:[UIColor colorWithRed:(255/255.0) green:(255/255.0) blue:(255/255.0) alpha:1]  forState:UIControlStateNormal];
    }

    
    [self.submitOrderButton addTarget:self action:@selector(onSubmitButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.marketButton addTarget:self action:@selector(onMarketButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.limitButton addTarget:self action:@selector(onLimitButtonTap) forControlEvents:UIControlEventTouchUpInside];
    
    [self.stopLossButton addTarget:self action:@selector(onStopLossButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.buyButton addTarget:self action:@selector(onBuyButtontap) forControlEvents:UIControlEventTouchUpInside];
    [self.sellButton addTarget:self action:@selector(onSellButtontap) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

-(void)dismissKeyboard
{
    [self.quantityTextField resignFirstResponder];
    [self.limitTextField resignFirstResponder];
    [self.stopLossTextField resignFirstResponder];
    [self.triggerPriceText resignFirstResponder];
}
-(void)onBackButtonTap
{
    if([self.etfOrderCheck isEqualToString:@"etf"])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else
    {
    
  //  [self.navigationController popViewControllerAnimated:YES];
    MainOrdersUSA * mainOrders = [self.storyboard instantiateViewControllerWithIdentifier:@"MainOrdersUSA"];
    [self presentViewController:mainOrders animated:YES completion:nil];
    }
}

-(void)UIUpdate
{
    //self.tickerNameLabel.text=self.tickerName;
    self.companyNameLabel.text=self.companyName;
}
-(void)onDepthButtonTap
{
    if(depthCheck==true)
    {
        self.depthView.hidden=NO;
        self.depthViewHeightConstraint.constant=135;
        depthCheck=false;
    }else if (depthCheck==false)
    {
        self.depthView.hidden=YES;
        self.depthViewHeightConstraint.constant=0;
        depthCheck=true;
    }
}

-(void)onBuyButtontap
{
    
    self.buySellCheck=@"1";
    [self.sellButton.layer setBorderWidth:1.0f];
    
    [self.sellButton.layer setBorderColor:[[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:0.7] CGColor]];
    
    [self.sellButton setTitleColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]  forState:UIControlStateNormal];
    
    [self.sellButton setBackgroundColor:[UIColor clearColor]];
    
    
    
    [self.buyButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]];
    [self.buyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

}
-(void)onSellButtontap
{
    self.buySellCheck=@"2";
    [self.sellButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1]];
    
    [self.sellButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.buyButton setBackgroundColor:[UIColor clearColor]];
    
    [self.buyButton.layer setBorderWidth:1.0f];
    [self.buyButton.layer setBorderColor:[[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:0.7] CGColor]];
    
    [self.buyButton setTitleColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1]  forState:UIControlStateNormal];
}
-(void)onMarketButtonTap
{
   orderTypeCheck=@"1";
    self.LimitView.hidden=YES;
    self.stopLossView.hidden=YES;
    self.marketButton.selected=YES;
    self.limitButton.selected=NO;
    self.stopLossButton.selected=NO;
    self.marketButton.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.limitButton.backgroundColor=[UIColor clearColor];
    self.stopLossButton.backgroundColor=[UIColor clearColor];
}
-(void)onLimitButtonTap
{
   orderTypeCheck=@"2";
   // self.LimitView.hidden=NO;
    //self.stopLossView.hidden=YES;
    
    self.LimitView.hidden=NO;
    self.stopLossView.hidden=YES;
    self.marketButton.selected=NO;
    self.limitButton.selected=YES;
    self.stopLossButton.selected=NO;
   // self.limitLbl.hidden=NO;
   // self.limitTxtView.hidden=NO;
    self.limitButton.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.marketButton.backgroundColor=[UIColor clearColor];
    self.stopLossButton.backgroundColor=[UIColor clearColor];

}
-(void)onStopLossButtonTap
{
   orderTypeCheck=@"3";
    self.stopLossView.hidden=NO;
    self.LimitView.hidden=YES;
    self.stopLossView.hidden=NO;
    self.marketButton.selected=NO;
    self.limitButton.selected=NO;
    self.stopLossButton.selected=YES;
   // self.limitLbl.hidden=YES;
    
    
   // self.limitTxtView.hidden=YES;
    self.stopLossButton.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(212/255.0) blue:(215/255.0) alpha:1];
    self.limitButton.backgroundColor=[UIColor clearColor];
    self.marketButton.backgroundColor=[UIColor clearColor];
}

-(void)orderServer
{
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    int len=6;
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    for (int i=0; i<len; i++) {
        
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
        
    }
    
    NSLog(@"Random:%@",randomString);
    
    //trailtype
    //uid
    //legs
    
    NSString * legs = [NSString stringWithFormat:@"\"Leg1\":{\"tx\":\"%@\",\"qty\":\"%@\",\"symbol\":\"%@\"}",self.buySellCheck,self.quantityTextField.text,delegate.stockTickerString];
    
    //        ,\"tx\":\"1\"
    
    if([orderTypeCheck isEqualToString:@"1"])
    {
        str = [NSString stringWithFormat:@"api={\"name\":\"JSON_PlaceOrder\",\"sourceID\":\"T7qqV91jR2\",\"sessionID\":\"%@\",\"firmCode\":\"5ntJOPXqQb\",\"requestType\":\"real\",\"account\":\"%@\",\"duration\":\"1\",\"ordertype\":\"%@\",\"limprice\":\"0\",\"stopprice\":\"0\",\"legs\": {%@},\"requesteddestination\":\"\",\"aon\":\"0\",\"trailPrice\":\"\",\"trailType\":0,\"ActivateTime0\":\"\",\"ExpireTime0\":\"\",\"HiddenOrder0\":\"\",\"Conditions0\":\"\",\"ActivateTime1\":\"\",\"ExpireTime1\":\"\",\"HiddenOrder1\":\"\",\"Conditions1\":\"\",\"QueueOrderID\":\"\",\"forMargin\":\"C\",\"contingency\":\"XXXXXXXXXXXXXXXXXX 1 1 Single Order\",\"manualCommission\":\"\",\"forCollateral\":\"\",\"journalEntry\":\"\",\"channel\":\"\",\"limittype\":\"\",\"watchprice\":\"\",\"isMFrequest\":\"N\",\"qtyType\":\"\",\"uid\":\"%@\",\"timeInForce\":\"1\",\"IPaddress\":\"1.1.1.1\"}",delegate.sessionID,delegate.accountNumber,orderTypeCheck,legs,randomString];
    }else if ([orderTypeCheck isEqualToString:@"2"])
    {
    
str = [NSString stringWithFormat:@"api={\"name\":\"JSON_PlaceOrder\",\"sourceID\":\"T7qqV91jR2\",\"sessionID\":\"%@\",\"firmCode\":\"5ntJOPXqQb\",\"requestType\":\"real\",\"account\":\"%@\",\"duration\":\"1\",\"ordertype\":\"%@\",\"limprice\":\"%@\",\"stopprice\":\"0\",\"legs\": {%@},\"requesteddestination\":\"\",\"aon\":\"0\",\"trailPrice\":\"\",\"trailType\":0,\"ActivateTime0\":\"\",\"ExpireTime0\":\"\",\"HiddenOrder0\":\"\",\"Conditions0\":\"\",\"ActivateTime1\":\"\",\"ExpireTime1\":\"\",\"HiddenOrder1\":\"\",\"Conditions1\":\"\",\"QueueOrderID\":\"\",\"forMargin\":\"C\",\"contingency\":\"XXXXXXXXXXXXXXXXXX 1 1 Single Order\",\"manualCommission\":\"\",\"forCollateral\":\"\",\"journalEntry\":\"\",\"channel\":\"\",\"limittype\":\"\",\"watchprice\":\"\",\"isMFrequest\":\"N\",\"qtyType\":\"\",\"uid\":\"%@\",\"timeInForce\":\"1\",\"IPaddress\":\"1.1.1.1\"}",delegate.sessionID,delegate.accountNumber,orderTypeCheck,self.limitTextField.text,legs,randomString];
        
        
    }else if ([orderTypeCheck isEqualToString:@"3"])
    {
        str = [NSString stringWithFormat:@"api={\"name\":\"JSON_PlaceOrder\",\"sourceID\":\"T7qqV91jR2\",\"sessionID\":\"%@\",\"firmCode\":\"5ntJOPXqQb\",\"requestType\":\"real\",\"account\":\"%@\",\"duration\":\"1\",\"ordertype\":\"%@\",\"limprice\":\"%@\",\"stopprice\":\"%@\",\"legs\": {%@},\"requesteddestination\":\"\",\"aon\":\"0\",\"trailPrice\":\"\",\"trailType\":0,\"ActivateTime0\":\"\",\"ExpireTime0\":\"\",\"HiddenOrder0\":\"\",\"Conditions0\":\"\",\"ActivateTime1\":\"\",\"ExpireTime1\":\"\",\"HiddenOrder1\":\"\",\"Conditions1\":\"\",\"QueueOrderID\":\"\",\"forMargin\":\"C\",\"contingency\":\"XXXXXXXXXXXXXXXXXX 1 1 Single Order\",\"manualCommission\":\"\",\"forCollateral\":\"\",\"journalEntry\":\"\",\"channel\":\"\",\"limittype\":\"\",\"watchprice\":\"\",\"isMFrequest\":\"N\",\"qtyType\":\"\",\"uid\":\"%@\",\"timeInForce\":\"1\",\"IPaddress\":\"1.1.1.1\"}",delegate.sessionID,delegate.accountNumber,orderTypeCheck,self.triggerPriceText.text,self.stopLossTextField.text,legs,randomString];
    }
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    
    
    NSLog(@"order string:%@",str);
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];

    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        NSString * successCheck = [json objectForKey:@"message"];
                                                        self.mainOrderID = [json objectForKey:@"orderid"];
                                                        if([successCheck containsString:@"Success"])
                                                        {
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                UIAlertController * alert = [UIAlertController
                                                                                             alertControllerWithTitle:@"Order Status"
                                                                                             message:@"Order Placed Successfully!"
                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                //Add Buttons
                                                                
                                                                UIAlertAction* okButton = [UIAlertAction
                                                                                           actionWithTitle:@"Ok"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               //Handle your yes please button action here
                                                                           self.limitTextField.text=@"";
                                                                        self.stopLossTextField.text=@"";
                                                                      self.triggerPriceText.text=@"";
                                                                        self.quantityTextField.text=@"";
                                                                                               [self ordersListServer];
                                                                                           }];
                                                                //Add your buttons to alert controller
                                                                
                                                                [alert addAction:okButton];
                                                                
                                                                
                                                                [self presentViewController:alert animated:YES completion:nil];
                                                                
                                                            });
                                                        }else
                                                        {
                                                            
                                                            NSString * orderFailString = [NSString stringWithFormat:@"Order Failed! Reason:%@",[json objectForKey:@"message"]];
                                                            UIAlertController * alert = [UIAlertController
                                                                                         alertControllerWithTitle:@"Order Status"
                                                                                         message:orderFailString
                                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            //Add Buttons
                                                            
                                                            UIAlertAction* okButton = [UIAlertAction
                                                                                       actionWithTitle:@"Ok"
                                                                                       style:UIAlertActionStyleDefault
                                                                                       handler:^(UIAlertAction * action) {
                                                                                           //Handle your yes please button action here
                                                                                           self.limitTextField.text=@"";
                                                                                           self.stopLossTextField.text=@"";
                                                                                           self.triggerPriceText.text=@"";
                                                                                           self.quantityTextField.text=@"";
                                                                                           
                                                                                           
                                                                                       }];
                                                            //Add your buttons to alert controller
                                                            
                                                            [alert addAction:okButton];
                                                            
                                                            
                                                            [self presentViewController:alert animated:YES completion:nil];
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
    
    
}



-(void)onSubmitButtonTap
{
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//
//    // getting an NSString
//    NSString *savedUsername = [prefs stringForKey:@"dummyUserName"];
//    NSString *savedPassword = [prefs stringForKey:@"dummyPassword"];
//    NSString *savedEmail = [prefs stringForKey:@"dummyEmail"];
//    if(savedEmail.length==0)
//    {
//        NewSelectLoginViewController * login = [self.storyboard instantiateViewControllerWithIdentifier:@"NewSelectLoginViewController"];
//        login.fromWhere=@"orderticket";
//        [self.navigationController presentViewController:login animated:YES completion:nil];
//    }else
//    {
    
    
    
    
    
    
    if(self.marketButton.selected==YES)
    {
    if(self.quantityTextField.text.length==0||self.buySellCheck.length==0)
    {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Warning"
                                     message:@"Missing fields.Verify!"
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        //Add Buttons
        
        UIAlertAction* okButton = [UIAlertAction
                                   actionWithTitle:@"Ok"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle your yes please button action here

                                       
                                       
                                   }];
        //Add your buttons to alert controller
        
        [alert addAction:okButton];
        
        
        [self presentViewController:alert animated:YES completion:nil];
    }else
    {
        if([self.editOrderCheck isEqualToString:@"edit"])
        {
            [self modifyOrder];
        }else
        {
        [self orderServer];
        }
    }
    }else if(self.limitButton.selected==YES)
    {
       if(self.quantityTextField.text.length==0||self.buySellCheck.length==0||self.limitTextField.text.length==0)
       {
           UIAlertController * alert = [UIAlertController
                                        alertControllerWithTitle:@"Warning"
                                        message:@"Missing fields.Verify!"
                                        preferredStyle:UIAlertControllerStyleAlert];
           
           //Add Buttons
           
           UIAlertAction* okButton = [UIAlertAction
                                      actionWithTitle:@"Ok"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          //Handle your yes please button action here
                                          
                                          
                                          
                                      }];
           //Add your buttons to alert controller
           
           [alert addAction:okButton];
           
           
           [self presentViewController:alert animated:YES completion:nil];
       }else
       {
           if([self.editOrderCheck isEqualToString:@"edit"])
           {
               [self modifyOrder];
           }else
           {
               [self orderServer];
           }
       }
    }else if(self.stopLossButton.selected==YES)
    {
        if(self.quantityTextField.text.length==0||self.buySellCheck.length==0||self.triggerPriceText.text.length==0||self.stopLossTextField.text.length==0)
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Warning"
                                         message:@"Missing fields.Verify!"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                           
                                           
                                           
                                       }];
            //Add your buttons to alert controller
            
            [alert addAction:okButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
        }else
        {
            if([self.editOrderCheck isEqualToString:@"edit"])
            {
                [self modifyOrder];
            }else
            {
                [self orderServer];
            }
        }
    }
  //  }
}

-(void)ordersListServer
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    self.dateString = [dateFormat stringFromDate:today];
    NSLog(@"date: %@", self.dateString);
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetOrderListPaginated\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"account\":\"%@\",\"sortBy\":\"2\",\"page\":\"1\",\"startDate\":\"%@\",\"endDate\":\"%@\"}",delegate.sessionID,delegate.accountNumber,self.dateString,self.dateString];
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];

    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        
//                                                        if(self.ordersResponseDictionary.count>0)
//                                                        {
//                                                            [self.ordersResponseDictionary removeAllObjects];
//                                                        }
//                                                        [self.ordersResponseDictionary setObject:json forKey:@"orders"];
                                                        NSMutableArray * checkArray = [[NSMutableArray alloc]init];
//                                                        NSLog(@"orders:%@",self.ordersResponseDictionary);
                                                        for(int i=0;i<[[json objectForKey:@"records"] count];i++)
                                                        {
                                                            if([self.mainOrderID isEqualToString:[[[json objectForKey:@"records"]objectAtIndex:i]objectForKey:@"orderID"]])
                                                            {
                                                                [checkArray addObject:[[json objectForKey:@"records"]objectAtIndex:i]];
                                                            }
                                                        }
                                                        NSLog(@"checkarray:%@",checkArray);
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            MainOrdersUSA * mainOrders = [self.storyboard instantiateViewControllerWithIdentifier:@"MainOrdersUSA"];
                                                            mainOrders.orderStatusString= [[checkArray objectAtIndex:0] objectForKey:@"status"];
                                                            [self presentViewController:mainOrders animated:YES completion:nil];

                                                        });
                                                    }
                                                }];
    [dataTask resume];
    
}
-(void)modifyOrder
{
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_CancelReplace\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"order_id\":\"%@\",\"quantity\":\"%@\",\"newPriceType\":\"%@\",\"newLimitPrice\":\"%@\",\"newStopPrice\":\"%@\",\"newDuration\":\"1\",\"newQualifier\":\"0\",\"newTrailPrice\":\"\",\"newTrailType\":\"\",\"newTimeInForce\":\"1\"}",delegate.sessionID,self.editOrderID,self.quantityTextField.text,orderTypeCheck,self.limitTextField.text,self.stopLossTextField.text];
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        
                                                        NSString * successCheck = [json objectForKey:@"message"];
                                                        self.mainOrderID = self.editOrderID;
                                                        if([successCheck containsString:@"Success"])
                                                        {
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                UIAlertController * alert = [UIAlertController
                                                                                             alertControllerWithTitle:@"Order Status"
                                                                                             message:@"Order Modified Successfully!"
                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                                                                
                                                                //Add Buttons
                                                                
                                                                UIAlertAction* okButton = [UIAlertAction
                                                                                           actionWithTitle:@"Ok"
                                                                                           style:UIAlertActionStyleDefault
                                                                                           handler:^(UIAlertAction * action) {
                                                                                               //Handle your yes please button action here
                                                                                               self.limitTextField.text=@"";
                                                                                               self.stopLossTextField.text=@"";
                                                                                               self.triggerPriceText.text=@"";
                                                                                               self.quantityTextField.text=@"";
                                                                                               [self ordersListServer];
                                                                                           }];
                                                                //Add your buttons to alert controller
                                                                
                                                                [alert addAction:okButton];
                                                                
                                                                
                                                                [self presentViewController:alert animated:YES completion:nil];
                                                                
                                                            });
                                                        }else
                                                        {
                                                            
                                                            NSString * orderFailString = [NSString stringWithFormat:@"Order Failed! Reason:%@",[json objectForKey:@"message"]];
                                                            UIAlertController * alert = [UIAlertController
                                                                                         alertControllerWithTitle:@"Order Status"
                                                                                         message:orderFailString
                                                                                         preferredStyle:UIAlertControllerStyleAlert];
                                                            
                                                            //Add Buttons
                                                            
                                                            UIAlertAction* okButton = [UIAlertAction
                                                                                       actionWithTitle:@"Ok"
                                                                                       style:UIAlertActionStyleDefault
                                                                                       handler:^(UIAlertAction * action) {
                                                                                           //Handle your yes please button action here
                                                                                           self.limitTextField.text=@"";
                                                                                           self.stopLossTextField.text=@"";
                                                                                           self.triggerPriceText.text=@"";
                                                                                           self.quantityTextField.text=@"";
                                                                                           [self orderServer];
                                                                                           
                                                                                       }];
                                                            //Add your buttons to alert controller
                                                            
                                                            [alert addAction:okButton];
                                                            
                                                            
                                                            [self presentViewController:alert animated:YES completion:nil];
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
    
}

-(void)depthServer
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"80da8702-247a-1c3e-da9b-84037d4e1429" };
    
    NSString * url = [NSString stringWithFormat:@"http://13.126.147.95/data/getQuotes.json?symbols=%@&webmasterId=89748",self.tickerName];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        depthResponseDictionary= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"Depth details:%@",depthResponseDictionary);
                                                        
                                                        [self depthServerValues];
                                                       
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if([bidPrice isEqual:[NSNull null]])
                                                            {
                                                                self.bidPriceLabel.text=@"N/A";
                                                            }else
                                                            {
                                                                self.bidPriceLabel.text=bidPrice;
                                                            }
                                                            
                                                            
                                                            self.bidQuantityLabel.text=bidQuantity;
                                                            
                                                            if([askPrice isEqual:[NSNull null]])
                                                            {
                                                                self.askPriceLabel.text=@"N/A";
                                                            }else
                                                            {
                                                            self.askPriceLabel.text=askPrice;
                                                            }
                                                            self.askQuantityLabel.text=askQuantity;
                                                            self.openLabel.text = openString;
                                                            self.highLabel.text = highString;
                                                            self.lowLabel.text = lowString;
                                                            self.previousCloseLabel.text = previousCloseString;
                                                            self.weekHighLabel.text = weekHighString;
                                                            self.weekLowLabel.text = weekLowString;
                                                            self.volumeLabel.text = volumeString;
                                                            self.marketCapLabel.text = marketCapString;
                                                            self.LTTimeLabel.text = finalDate;

                                                        });
                                                    }
                                                }];
    [dataTask resume];
}

-(void)depthServerValues
{
    NSString * bidPriceString = [NSString stringWithFormat:@"%.2f",[[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"bid"] floatValue]];
    bidPrice = bidPriceString;
     bidQuantity = [[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"bidsize"] stringValue];
    NSString * askPriceString = [NSString stringWithFormat:@"%.2f",[[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"ask"] floatValue]];
    
    askPrice = askPriceString;
     askQuantity = [[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"asksize"] stringValue];
    openString = [[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"open"] stringValue];
    highString = [[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"high"] stringValue];
    lowString = [[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"low"] stringValue];
    previousCloseString = [[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"prevclose"] stringValue];
    weekHighString = [[[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"fundamental"] objectForKey:@"week52high"] objectForKey:@"content"] stringValue];
    weekLowString = [[[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"fundamental"] objectForKey:@"week52low"] objectForKey:@"content"] stringValue];
    volumeString = [[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"sharevolume"] stringValue];
    marketCapString = [[[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"fundamental"] objectForKey:@"marketcap"] stringValue];
    LTTmeString = [[[[[depthResponseDictionary objectForKey:@"results"] objectForKey:@"quote"] objectAtIndex:0] objectForKey:@"pricedata"] objectForKey:@"lasttradedatetime"];
    finalDate=@"";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss-HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *date = [dateFormatter dateFromString:LTTmeString]; // create date from string
    
    // change to a readable time format and change to local time zone
    [dateFormatter setDateFormat:@"dd MMM yyyy HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    finalDate = [dateFormatter stringFromDate:date];
    NSLog(@"finalDate:%@",finalDate);
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
