//
//  NewStockViewController.h
//  
//
//  Created by Zenwise Technologies on 18/09/17.
//
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"

@interface NewStockViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,XYPieChartDelegate, XYPieChartDataSource>
@property (weak, nonatomic) IBOutlet UIView *ratingMainView;
@property (weak, nonatomic) IBOutlet UILabel *ratingBuyLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingHoldLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingSellLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingHighLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingAverageLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLowLabel;
@property (weak, nonatomic) IBOutlet UIButton *BuyButton;
@property (weak, nonatomic) IBOutlet UIView *tableUpperView;
@property (weak, nonatomic) IBOutlet UITableView *stockDetailTableView;
@property NSString * tickerNameString;
@property (weak, nonatomic) IBOutlet XYPieChart *pieChartOutlet;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@property NSMutableArray *sliceColors;
@property NSMutableArray *pieChartPercentageArray;
@property NSMutableDictionary *pieChartPercentageDict;
@property (weak, nonatomic) IBOutlet UILabel *tickerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *symbolNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingsLabel;
@property NSMutableArray * ltpResponseArray;
@property (weak, nonatomic) IBOutlet UILabel *ltpLabel;
@property (weak, nonatomic) IBOutlet UILabel *upSidePercentLabel;
@property NSMutableDictionary * ltpResponseDictionary;
@property NSString * priceTargetString;
@property NSString * priceTargetCheck;

@end
