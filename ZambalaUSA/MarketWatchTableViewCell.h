//
//  MarketWatchTableViewCell.h
//  ZambalaUSA
//
//  Created by guna on 25/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketWatchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *tradeButton;
@property (weak, nonatomic) IBOutlet UILabel *tickerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ltpLabel;
@property (weak, nonatomic) IBOutlet UILabel *changeLabel;
@property (weak, nonatomic) IBOutlet UILabel *changePercentLabel;

@end
