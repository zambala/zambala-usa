//
//  AppDelegate.m
//  ZambalaUSA
//
//  Created by guna on 17/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "AppDelegate.h"
#import "Mixpanel/Mixpanel.h"
#import <TwitterKit/TwitterKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
     [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    
   
    UIImage *NavigationPortraitBackground= [UIImage imageNamed:@"topUS-1"];
    [[UINavigationBar appearance] setBackgroundImage:NavigationPortraitBackground forBarMetrics:UIBarMetricsDefault];

        
    [[UINavigationBar appearance] setTranslucent:NO];
    
    
    //MarketWatch
    
    self.equityTickerNameArray = [[NSMutableArray alloc]init];
    self.equityCompanyNameArray = [[NSMutableArray alloc]init];
    
    
    NSMutableDictionary *twitterKeys;
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Twitter" ofType:@"plist"];
    
    twitterKeys = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    
    [[Twitter sharedInstance] startWithConsumerKey:[twitterKeys objectForKey:@"consumer_key"] consumerSecret:[twitterKeys objectForKey:@"consumer_secret"]];
    
    
    
    [[Twitter sharedInstance] startWithConsumerKey:@"E5b4foHqkKv9AT8I4eDTKCqt2" consumerSecret:@"205ANedDQktKbaNUGFK5Kj2q8TB1ow0PJZeS7PTa8nojrAOJPG"];

    [Mixpanel sharedInstanceWithToken:@"38124e20212adfc92d9b44a25f3b8981"];
    self.expertIDArray = [[NSMutableArray alloc]init];
    self.mainOrdersArray = [[NSMutableArray alloc]init];
    self.dummyAccountURL = @"http://api-test.choicetrade.com/";
    self.realAccountURL=@"http://api-trade.choicetrade.com/";
   // self.zambalaEndPoint =@"http://zambaladev.ap-southeast-1.elasticbeanstalk.com/api/";
    self.zambalaEndPoint=@"http://52.66.159.178:8013/api/";
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    return YES;
    
    
    
    
    //[self.window addSubview:viewController.view];
//    [self.window makeKeyAndVisible];
//
//    NSLog(@"Registering for push notifications...");
//    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//     (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
//
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

//- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
//{
//    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
//    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSLog(@"content---%@", token);
//}


- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *str = [NSString stringWithFormat:@"Device Token=%@",deviceToken];
    NSLog(@"%@", str);
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"%@",str);
}





- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
