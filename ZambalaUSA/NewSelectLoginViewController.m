//
//  NewSelectLoginViewController.m
//  ZambalaUSA
//
//  Created by Zenwise Technologies on 30/10/17.
//  Copyright © 2017 Sandeep Maganti. All rights reserved.
//

#import "NewSelectLoginViewController.h"
#import "AppDelegate.h"
#import "TabMenuView.h"
#import <QuartzCore/QuartzCore.h>
#import "Mixpanel/Mixpanel.h"
#import "NewOrderViewController.h"


@interface NewSelectLoginViewController ()

@end

@implementation NewSelectLoginViewController
{
    AppDelegate * delegate;
    int successCheck;
    NSDictionary* json;
    int successCheck1;
    int successCheck2;
    NSDictionary* json1;
    NSDictionary* json2;
    NSString * dummyEmail;
}

- (void)viewDidLoad {
    self.emailIDTF.text=@"sandeep@zenwise.net";
    self.userIDTF.text=@"CTA19872";
    self.passwordTF.text=@"Chinky123$";
    self.passwordTF.secureTextEntry=YES;
    self.view.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:.6];
    self.popUPView.layer.cornerRadius = 5;
    self.popUPView.layer.shadowOpacity = 0.8;
    self.popUPView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    [super viewDidLoad];
    [self showAnimate];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self.dummyAccountButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    [self.dummyAccountButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.realAccountButton setImage:[UIImage imageNamed:@"radioSelect"] forState:UIControlStateSelected];
    [self.realAccountButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    self.dummyAccountButton.selected=YES;
    delegate.accountCheck=@"dummy";
    
    
    [self.submitButton addTarget:self action:@selector(onSubmitButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.dummyAccountButton addTarget:self action:@selector(onDummyAccountTap) forControlEvents:UIControlEventTouchUpInside];
    [self.realAccountButton addTarget:self action:@selector(onRealAccountTap) forControlEvents:UIControlEventTouchUpInside];
    if(self.dummyAccountButton.selected==YES)
    {
        self.realAccountView.hidden=YES;
        self.dummyAccountView.hidden=NO;
        self.realAccountButton.selected=NO;
        self.dummyAccountButton.selected=YES;
    }else if (self.realAccountButton.selected==YES)
    {
        self.realAccountView.hidden=NO;
        self.dummyAccountView.hidden=YES;
        self.realAccountButton.selected=YES;
        self.dummyAccountButton.selected=NO;
    }
//    UIVisualEffect *blurEffect;
//    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//
//    UIVisualEffectView *visualEffectView;
//    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//
//    visualEffectView.frame = self.mainView.bounds;
//    [self.mainView addSubview:visualEffectView];
    
   // [self applyBlurToView:self.mainView withEffectStyle:UIBlurEffectStyleLight andConstraints:YES];
    
    self.submitButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.submitButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.submitButton.layer.shadowOpacity = 1.0f;
    self.submitButton.layer.shadowRadius = 1.0f;
    self.submitButton.layer.cornerRadius=1.0f;
    self.submitButton.layer.masksToBounds = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.mainView addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}
- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0.5;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
       
    }];
}


- (UIView *)applyBlurToView:(UIView *)view withEffectStyle:(UIBlurEffectStyle)style andConstraints:(BOOL)addConstraints
{
    //only apply the blur if the user hasn't disabled transparency effects
    if(!UIAccessibilityIsReduceTransparencyEnabled())
    {
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:style];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = view.bounds;
        
        [view addSubview:blurEffectView];
        
        if(addConstraints)
        {
            //add auto layout constraints so that the blur fills the screen upon rotating device
            [blurEffectView setTranslatesAutoresizingMaskIntoConstraints:NO];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:blurEffectView
                                                             attribute:NSLayoutAttributeTop
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeTop
                                                            multiplier:1
                                                              constant:0]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:blurEffectView
                                                             attribute:NSLayoutAttributeBottom
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeBottom
                                                            multiplier:1
                                                              constant:0]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:blurEffectView
                                                             attribute:NSLayoutAttributeLeading
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeLeading
                                                            multiplier:1
                                                              constant:0]];
            
            [view addConstraint:[NSLayoutConstraint constraintWithItem:blurEffectView
                                                             attribute:NSLayoutAttributeTrailing
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:view
                                                             attribute:NSLayoutAttributeTrailing
                                                            multiplier:1
                                                              constant:0]];
        }
    }
    else
    {
        view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    }
    
    return view;
}

- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.5;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
           
            
        }
    }];
}
- (void)showInView:(UIView *)aView animated:(BOOL)animated
{
    [aView addSubview:self.view];
    if (animated) {
        [self showAnimate];
    }
}

-(void)dismissKeyboard
{
    [self.userIDTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
    [self.emailIDTF resignFirstResponder];
}
-(void)loginCheckServer
{
    NSString * str;
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        str = [NSString stringWithFormat:@"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"%@\",\"password\":\"%@\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}",self.dummyUserID,self.dummyPassword];
    }else if([delegate.accountCheck isEqualToString:@"real"])
    {
        str = [NSString stringWithFormat:@"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"%@\",\"password\":\"%@\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}",self.userIDTF.text,self.passwordTF.text];
    }
    
    
    
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
    
    //    NSDictionary *cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
    //                                      @"domain.com", NSHTTPCookieDomain,
    //                                      @"\\", NSHTTPCookiePath,
    //                                      @"myCookie", NSHTTPCookieName,
    //                                      @"1234", NSHTTPCookieValue,
    //                                      nil];
    //    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    //    NSArray* cookieArray = [NSArray arrayWithObject:cookie];
    //    NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:cookieArray];
    //    [request setAllHTTPHeaderFields:headers];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                   
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                         successCheck = [[json objectForKey:@"success"] intValue];
                                                        
                                                    }
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            if(successCheck==1)
                                                            {
                                                                if([delegate.accountCheck isEqualToString:@"dummy"])
                                                                {
                                                                    delegate.userID = self.dummyUserID;
                                                                }else if([delegate.accountCheck isEqualToString:@"real"])
                                                                {
                                                                delegate.userID = self.userIDTF.text;
                                                                }
                                                                delegate.sessionID = [json objectForKey:@"session_id"];
                                                                NSLog(@"Session ID:%@",delegate.sessionID);
                                                                if(delegate.accountNumber.length>0)
                                                                {
                                                                    [self removeAnimate];
                                                                    TabMenuView * view = [self.storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
                                                                    [self presentViewController:view animated:YES completion:nil];
                                                                }else
                                                                {
                                                                    [self getAccountNumberServer];
                                                                }
                                                                
                                                                
                                                                
                                                                
                                                            }else if(successCheck==0)
                                                            {
                                                                 NSString * message = [NSString stringWithFormat:@"%@",[json objectForKey:@"message"]];
                                                                if([[json objectForKey:@"message"]isEqualToString:@"Error: Firm is Invalid."])
                                                                {
                                                                [self loginCheckServer];
                                                                }else if(message.length>0&&![message isEqualToString:@"Error: Firm is Invalid."])
                                                                {
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        
                                                                        UIAlertController * alert = [UIAlertController
                                                                                                     alertControllerWithTitle:@"Warning"
                                                                                                     message:message
                                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                                                        
                                                                        //Add Buttons
                                                                        
                                                                        UIAlertAction* okButton = [UIAlertAction
                                                                                                   actionWithTitle:@"Ok"
                                                                                                   style:UIAlertActionStyleDefault
                                                                                                   handler:^(UIAlertAction * action) {
                                                                                                       //Handle your yes please button action here
                                                                                                       
                                                                                                   }];
                                                                        //Add your buttons to alert controller
                                                                        
                                                                        [alert addAction:okButton];
                                                                        
                                                                        
                                                                        [self presentViewController:alert animated:YES completion:nil];
                                                                        
                                                                    });
                                                                }
                                                                
                                                            }
                                                            
                                                        });
                                                        
                                                    
                                                }];
    [dataTask resume];
}

-(void)getAccountNumberServer
{
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetAccountList\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"byCust\":\"0\",\"userID\":\"%@\",\"accountMode\":\"A\"}",delegate.sessionID,delegate.userID];
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
       request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:10.0];
    }
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        json1 = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json1);
                                                     successCheck1 = [[json1 objectForKey:@"success"] intValue];
                                                        
                                                        
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if(successCheck1==1)
                                                        {
                                                            delegate.accountNumber = [[json1 objectForKey:@"accounts"] objectForKey:@"accountNo"];
                                                            Mixpanel *mixpanel = [Mixpanel sharedInstance];
                                                            
                                                            [mixpanel identify:delegate.userID];
                                                            if([delegate.accountCheck isEqualToString:@"dummy"])
                                                            {
                                                                [mixpanel.people set:@{@"first_name": delegate.accountNumber,@"email":self.emailIDTF.text}];
                                                            }else if ([delegate.accountCheck isEqualToString:@"real"])
                                                            {
                                                                NSString * name = [NSString stringWithFormat:@"%@",[[json1 objectForKey:@"accounts"]objectForKey:@"name"]];
                                                                [mixpanel.people set:@{@"first_name":name}];
                                                            }
                                                            [mixpanel.people set:@{@"brokername":@"ChoiceTrade"}];
                                                            
                                                            [self removeAnimate];
                                                            
                                                            TabMenuView * view = [self.storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
                                                            [self presentViewController:view animated:YES completion:nil];
                                                            
                                                            
                                                        }
                                                        
                                                    });
                                                }];
    [dataTask resume];
}

-(void)createNewVirtualAccount
{
    
    NSString * email = self.emailIDTF.text;
    
     NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_NewVirtualAccount\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"unused1\":\"\",\"unused2\":\"\",\"LastName\":\"Test\",\"FirstName\":\"TestOne\",\"MiddleInitial\":\"test\",\"Email\":\"%@\"}",email];
    
    
    
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        json2 = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json2);
                                                       
                                                        successCheck2 = [[json2 objectForKey:@"success"] intValue];
                                                        if([[json2 objectForKey:@"message"]isEqualToString:@"Error: Firm is Invalid."])
                                                        {
                                                            [self createNewVirtualAccount];
                                                        }
                                                        
                                                    }
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if(successCheck2==1)
                                                        {
                                                            self.dummyUserID = [json2 objectForKey:@"userID"];
                                                            self.dummyPassword = [json2 objectForKey:@"password"];
                                                            delegate.accountNumber = [json2 objectForKey:@"AccountNo"];
                                                            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                            
                                                            // saving an NSString
                                                            [prefs setObject:self.dummyUserID forKey:@"dummyUserName"];
                                                            [prefs setObject:self.dummyPassword forKey:@"dummyPassword"];
                                                            [prefs setObject:self.emailIDTF.text forKey:@"dummyEmail"];
                                                            
                                                            [prefs synchronize];
                                                            
//                                                            self.userIDTF.text=[json objectForKey:@"userID"];
//                                                            self.passwordTF.text=[json objectForKey:@"password"];
                                                            [self loginCheckServer];
                                                        }
                                                        
                                                    });
                                                }];
    [dataTask resume];
}
- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(void)onSubmitButtonTap
{
    if(self.realAccountButton.selected==YES)
    {
    if(self.userIDTF.text.length==0||self.passwordTF.text.length==0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"Warning"
                                         message:@"Missing Fields!"
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* okButton = [UIAlertAction
                                       actionWithTitle:@"Ok"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle your yes please button action here
                                           
                                       }];
            //Add your buttons to alert controller
            
            [alert addAction:okButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
        });
    
    }else
    {
       [self loginCheckServer];
    }
    }else if (self.dummyAccountButton.selected==YES)
    {
        [self validateEmailWithString:self.emailIDTF.text];
        if(self.emailIDTF.text.length==0||[self validateEmailWithString:self.emailIDTF.text]==NO)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert = [UIAlertController
                                             alertControllerWithTitle:@"Warning"
                                             message:@"Invalid Email!"
                                             preferredStyle:UIAlertControllerStyleAlert];
                
                //Add Buttons
                
                UIAlertAction* okButton = [UIAlertAction
                                           actionWithTitle:@"Ok"
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                                               //Handle your yes please button action here
                                               
                                           }];
                //Add your buttons to alert controller
                
                [alert addAction:okButton];
                
                
                [self presentViewController:alert animated:YES completion:nil];
                
            });
            
        }else
        {
//            if([dummyEmail isEqualToString:@"sandeep@zenwise.net"])
//            {
//            self.userIDTF.text=@"VT070971";
//            self.passwordTF.text=@"EQI387";
//            [self loginCheckServer];
//            }else
//            {
//                dispatch_async(dispatch_get_main_queue(), ^{
//
//                    UIAlertController * alert = [UIAlertController
//                                                 alertControllerWithTitle:@"Warning"
//                                                 message:@"Invalid Email!"
//                                                 preferredStyle:UIAlertControllerStyleAlert];
//
//                    //Add Buttons
//
//                    UIAlertAction* okButton = [UIAlertAction
//                                               actionWithTitle:@"Ok"
//                                               style:UIAlertActionStyleDefault
//                                               handler:^(UIAlertAction * action) {
//                                                   //Handle your yes please button action here
//
//                                               }];
//                    //Add your buttons to alert controller
//
//                    [alert addAction:okButton];
//
//
//                    [self presentViewController:alert animated:YES completion:nil];
//
//                });
//            }
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            
            // getting an NSString
            NSString *savedUsername = [prefs stringForKey:@"dummyUserName"];
            NSString *savedPassword = [prefs stringForKey:@"dummyPassword"];
            NSString *savedEmail = [prefs stringForKey:@"dummyEmail"];
            
            if([self.emailIDTF.text isEqualToString:savedEmail])
            {
                self.dummyUserID = savedUsername;
                self.dummyPassword = savedPassword;
                [self loginCheckServer];
            }else
            {
            [self createNewVirtualAccount];
            }
//            self.userIDTF.text=@"VT070971";
//            self.passwordTF.text=@"EQI387";
          //  [self loginCheckServer];
        }
    }
}
-(void)onDummyAccountTap
{
    self.realAccountView.hidden=YES;
    self.dummyAccountView.hidden=NO;
    self.realAccountButton.selected=NO;
    self.dummyAccountButton.selected=YES;
    delegate.accountCheck=@"dummy";
//    dummyEmail=@"sandeep@zenwise.net";
}
-(void)onRealAccountTap
{
    self.realAccountView.hidden=NO;
    self.dummyAccountView.hidden=YES;
    self.realAccountButton.selected=YES;
    self.dummyAccountButton.selected=NO;
    delegate.accountCheck=@"real";
    self.userIDTF.text=@"";
    self.passwordTF.text=@"";
    delegate.accountNumber=@"";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
