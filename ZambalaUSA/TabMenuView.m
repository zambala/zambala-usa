//
//  TabMenuView.m
//  ZambalaUSA
//
//  Created by guna on 20/04/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "TabMenuView.h"
#import "PortFolioViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "MainOrdersUSA.h"
#import "NewSelectLoginViewController.h"
#import "TradesViewController.h"

@interface TabMenuView ()

@end

@implementation TabMenuView
{
    AppDelegate * delegate;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.dummyTable = [[UITableView alloc]init];
//    self.dummyTable.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    [self.view addSubview:self.dummyTable];
    self.tabBar1.delegate=self;
   
    float X_Co = (self.view.frame.size.width - 70)/2;
    self.addButton = [[VCFloatingActionButton alloc]initWithFrame:CGRectMake(X_Co,(self.view.frame.size.height-60), 70, 70) normalImage:[UIImage imageNamed:@"icon10Plus"] andPressedImage:[UIImage imageNamed:@"icon10PlusRotated"] withScrollview:_dummyTable];
    
    //        NSDictionary *optionsDictionary = @{@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu9",@"plusMenu9.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"plusMenu10.png":@"plusMenu10",@"linkedin-icon":@"Linked in",@"linkedin-icon":@"Linked in",@"linkedin-icon":@"Linked in",@"linkedin-icon":@"Linked in"};
    //        self.addButton.menuItemSet = optionsDictionary;
    
    //    delegate1.imageArray=[[NSMutableArray alloc]initWithObjects:@"plusMenu10.png",@"plusMenu9.png",@"plusMenu8.png",@"plusMenu7.png",@"plusMenu6.png",@"plusMenu5.png",@"plusMenu4.png",@"plusMenu3@2x.png",@"plusMenu2.png",@"plusMenu1.png", nil];
    
    self.addButton.imageArray = @[@"plusMenu10",@"plusMenu9",@"plusMenu8",@"plusMenu7",@"plusMenu5",@"plusMenu4",@"plusMenu3",@"plusMenu2"];
    self.addButton.labelArray = @[@"Log Out",@"Contact Support",@"Trades",@"Orders",@"Fund Transfer",@"Wallet",@"Messages",@"Profile Settings"];
    self.tabBarController.tabBar.barTintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
    self.tabBarController.tabBar.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:1];
   // self.tabBarController.tabBar.backgroundImage= [UIImage imageNamed:@"bottomBg.png"];
    self.addButton.hideWhileScrolling = YES;
    self.addButton.delegate = self;
    _dummyTable.delegate = self;
    _dummyTable.dataSource = self;
    
    
    [self.view addSubview:self.addButton];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId forIndexPath:indexPath];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%tu",indexPath.row];
    
    
    
    cell.imageView.image =[UIImage imageNamed:[NSString stringWithFormat:@"%tu",indexPath.row]];

    return cell;
}





-(void) didSelectMenuOptionAtIndex:(NSInteger)row
{
    NSLog(@"Floating action tapped index %tu",row);
    if(row==0)
    {
        [self logOutServer];
    }
    
    if(row==2)
    {
        TradesViewController * trades = [self.storyboard instantiateViewControllerWithIdentifier:@"TradesViewController"];
        [self presentViewController:trades animated:YES completion:nil];
    }
    
    if(row==3)
    {
        MainOrdersUSA * mainOrders = [self.storyboard instantiateViewControllerWithIdentifier:@"MainOrdersUSA"];
        [self presentViewController:mainOrders animated:YES completion:nil];
    }
    
    
//    if(row==6)
//    {
//        PortFolioViewController * portfolioView = [self.storyboard instantiateViewControllerWithIdentifier:@"PortFolioViewController"];
//        [self presentViewController:portfolioView animated:YES completion:nil];
//    }
}


-(void)logOutServer
{
        NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_Logout\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"userID\":\"%@\"}",delegate.sessionID,delegate.userID];
    
        
        NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request=[[NSMutableURLRequest alloc]init];
    if([delegate.accountCheck isEqualToString:@"dummy"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.dummyAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }else if ([delegate.accountCheck isEqualToString:@"real"])
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:delegate.realAccountURL]
                                          cachePolicy:NSURLRequestUseProtocolCachePolicy
                                      timeoutInterval:10.0];
    }
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody:dt];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                    completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            NSDictionary* json = [NSJSONSerialization
                                                                                  JSONObjectWithData:data
                                                                                  options:kNilOptions
                                                                                  error:&error];
                                                            NSLog(@"jjson %@",json);
                                                            
                                                            
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                                    int success = [[json objectForKey:@"success"] intValue];
                                                                    if(success==1)
                                                                    {
                                                                    NewSelectLoginViewController * view=[self.storyboard instantiateViewControllerWithIdentifier:@"NewSelectLoginViewController"];
                                                                        delegate.accountNumber=@"";
                                                                    [self presentViewController:view animated:YES completion:nil];
                                                                    }
                                                                });
                                                            
                                                        }
                                                    }];
        [dataTask resume];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
//{
//    if(tabBarController.selectedIndex==2)
//    {
//        return NO;
//    }
//    
//    return YES;
//}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
