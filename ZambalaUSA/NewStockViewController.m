//
//  NewStockViewController.m
//  
//
//  Created by Zenwise Technologies on 18/09/17.
//
//

#import "NewStockViewController.h"
#import "HMSegmentedControl.h"
#import "stockDetailTableViewCell.h"
#import "XYPieChart.h"
#import "OrderViewController.h"
#import "AppDelegate.h"
#import "NewOrderViewController.h"

@interface NewStockViewController ()

@end

@implementation NewStockViewController
{
    HMSegmentedControl * segmentedControl;
    NSMutableArray * ratingResponseArray;
    NSMutableArray * advicesResponseArray;
    NSURL * url;
    AppDelegate* delegate;
    
    NSString * upsideValue;
    NSString * upsidePercent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"RESEARCH",@"OPTIONS",@"NEWS"]];
//    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 54);
//    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
//    //    segmentedControl.verticalDividerEnabled = YES;
//    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
//    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
//    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
//    segmentedControl.backgroundColor=[UIColor colorWithRed:(2/255.0) green:(30/255.0) blue:(41/255.0) alpha:1];
//    segmentedControl.tintColor=[UIColor colorWithRed:(255/255.0) green:(205/255.0) blue:(3/255.0) alpha:0.5];
//    [self.view addSubview:segmentedControl]
//    ;
    self.navigationItem.title = @"Stock Detail";
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.sliceColors = [[NSMutableArray alloc]init];
    self.pieChartPercentageArray = [[NSMutableArray alloc]init];
    self.pieChartPercentageDict = [[NSMutableDictionary alloc]init];
    self.ltpResponseArray = [[NSMutableArray alloc]init];
    
    [self analystRatingServer];
    [self adviceListServer];
  //  [self LTPServer];
   

    
    // Do any additional setup after loading the view.
}


-(void)analystRatingServer
{
    NSDictionary *headers = @{ @"x-apikey": @"TR_Zenwise",
                               @"x-apitoken": @"8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",
                               };
    
    NSString * url1 = [NSString stringWithFormat:@"https://api.tipranks.com/api/Stocks/Overview/%@",self.tickerNameString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        if(data!=nil)
                                                        {
                                                        
                                                        ratingResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                       
                                                            NSLog(@"Rating Response:%@",ratingResponseArray);
                                                            [self.pieChartPercentageArray addObject:[[ratingResponseArray objectAtIndex:0] objectForKey:@"buy"]];
                                                            [self.pieChartPercentageArray addObject:[[ratingResponseArray objectAtIndex:0] objectForKey:@"sell"]];
                                                            [self.pieChartPercentageArray addObject:[[ratingResponseArray objectAtIndex:0] objectForKey:@"hold"]];
                                                            NSLog(@"piechart:%@",self.pieChartPercentageArray);
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                
                                                                
                                                                [self pieChart];
                                                                [self analystRatingAssignUI];
                                                                
                                                                self.tickerNameLabel.text=[[ratingResponseArray objectAtIndex:0] objectForKey:@"ticker"];
                                                                self.symbolNameLabel.text = [[ratingResponseArray objectAtIndex:0] objectForKey:@"companyName"];
                                                                [self LTPServer];
                                                                
                                                            });

                                                            
                                                            
                                                        }
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)adviceListServer
{
    NSDictionary *headers = @{ @"x-apikey": @"TR_Zenwise",
                               @"x-apitoken": @"8a342d1a-1bha-21a2-d1d9-3a5cd0fac442",
                              };
    NSString * url1 = [NSString stringWithFormat:@"https://api.tipranks.com/api/Analysts/%@?num=30",self.tickerNameString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        
                                                        advicesResponseArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        NSLog(@"Advices Response Array:%@",advicesResponseArray);
                                                       
                                                        
                                                        
                                                        
                                                    }
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        
                                                        self.stockDetailTableView.delegate=self;
                                                        self.stockDetailTableView.dataSource=self;
                                                        
                                                        NSString * ratingsCount = [NSString stringWithFormat:@"%lu",(unsigned long)advicesResponseArray.count];
                                                        
                                                        NSString * messageString = [NSString stringWithFormat:@"(Average price target is based on 12 month price targets recommended by %@ top analysts in the last 6 months)",ratingsCount];
                                                        self.messageLabel.text= messageString;
                                                        [self.stockDetailTableView reloadData];
                                                        
                                                    });
                                                }];
    [dataTask resume];
}

-(void)analystRatingAssignUI
{
    
    self.ratingMainView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.ratingMainView.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.ratingMainView.layer.shadowOpacity = 1.0f;
    self.ratingMainView.layer.shadowRadius = 1.0f;
    self.ratingMainView.layer.cornerRadius=1.0f;
    self.ratingMainView.layer.masksToBounds = NO;
    
    self.BuyButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.12] CGColor];
    self.BuyButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.BuyButton.layer.shadowOpacity = 1.0f;
    self.BuyButton.layer.shadowRadius = 1.0f;
    self.BuyButton.layer.cornerRadius=1.0f;
    self.BuyButton.layer.masksToBounds = NO;

    
    NSString * buy = [NSString stringWithFormat:@"%@ Buy",[[ratingResponseArray objectAtIndex:0] objectForKey:@"buy"]];
    
    if([buy isEqualToString:@"<null>"]||[buy isEqual:[NSNull null]])
    {
        self.ratingBuyLabel.text=@"0";
    }else
        
    {
    
    self.ratingBuyLabel.text = buy;
    }
    
    
    NSString * hold = [NSString stringWithFormat:@"%@ Hold",[[ratingResponseArray objectAtIndex:0] objectForKey:@"hold"]];
    if([hold isEqualToString:@"<null>"]||[hold isEqual:[NSNull null]])
    {
        self.ratingHoldLabel.text=@"0";
    }else
        
    {
        
        self.ratingHoldLabel.text = hold;
    }
    
    NSString * sell = [NSString stringWithFormat:@"%@ Sell",[[ratingResponseArray objectAtIndex:0] objectForKey:@"sell"]];
    if([sell isEqualToString:@"<null>"]||[sell isEqual:[NSNull null]])
    {
        self.ratingSellLabel.text=@"0";
    }else
        
    {
        
        self.ratingSellLabel.text = sell;
    }
    
    
    NSString * highPrice = [NSString stringWithFormat:@"$%@",[[ratingResponseArray objectAtIndex:0] objectForKey:@"highPriceTarget"]];
    if([highPrice isEqual:[NSNull null]]||[highPrice isEqualToString:@"<null>"])
    {
        self.ratingHighLabel.text = @"0";
    }else
    {
    self.ratingHighLabel.text =highPrice;
    }
    NSString * priceTarget = [NSString stringWithFormat:@"$%@",[[ratingResponseArray objectAtIndex:0] objectForKey:@"priceTarget"]];
    if([priceTarget isEqual:[NSNull null]]||[priceTarget isEqualToString:@"<null>"])
    {
        self.ratingAverageLabel.text =@"0";
    }
    else
    {
    self.ratingAverageLabel.text = priceTarget;
    }
    NSString * lowPriceTarget = [NSString stringWithFormat:@"$%@",[[ratingResponseArray objectAtIndex:0] objectForKey:@"lowPriceTarget"]];
    if([lowPriceTarget isEqual:[NSNull null]]||[lowPriceTarget isEqualToString:@"<null>"])
    {
        self.ratingLowLabel.text = @"0";
    }
    else
    {
    self.ratingLowLabel.text = lowPriceTarget;
    }
    
    NSString * buySelltile = [NSString stringWithFormat:@"%@",[[ratingResponseArray objectAtIndex:0] objectForKey:@"consensus"]];
    if([buySelltile containsString:@"Hold"]||[buySelltile containsString:@"Neutral"])
    {
        [self.BuyButton setTitle:@"Trade" forState:UIControlStateNormal];
        [self.BuyButton setBackgroundColor:[UIColor colorWithRed:(85/255.0) green:(90/255.0) blue:(92/255.0) alpha:1.0f]];
    }else if ([buySelltile containsString:@"Sell"])
    {
        [self.BuyButton setBackgroundColor:[UIColor colorWithRed:(242/255.0) green:(30/255.0) blue:(51/255.0) alpha:1.0f]];
    }else if ([buySelltile containsString:@"Buy"])
    {
        [self.BuyButton setBackgroundColor:[UIColor colorWithRed:(24/255.0) green:(131/255.0) blue:(213/255.0) alpha:1.0f]];
    }
    if([buySelltile isEqual:[NSNull null]]||[buySelltile isEqualToString:@"<null>"])
    {
        [self.BuyButton setTitle:@"Trade" forState:UIControlStateNormal];
    }else
    {
    [self.BuyButton setTitle:buySelltile forState:UIControlStateNormal];
    }
    [self.BuyButton addTarget:self action:@selector(onMiddleButtonTap:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)segmentedControlChangedValue
{
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(segmentedControl.selectedSegmentIndex==0)
    {
        return advicesResponseArray.count;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if(tableView==self.stockDetailTableView)
//    {
//        NewStockViewController * stockDetailTrending = [self.storyboard instantiateViewControllerWithIdentifier:@"NewStockViewController"];
//        stockDetailTrending.tickerNameString = [[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
//        [self.navigationController pushViewController:stockDetailTrending animated:YES];
//    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(segmentedControl.selectedSegmentIndex==0)
    {
        stockDetailTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"stockDetailTableViewCell"];
        
        cell.profileImageView.layer.shadowOffset = CGSizeMake(1,1.7);
        cell.profileImageView.layer.shadowOpacity = 0.1;
        cell.profileImageView.layer.shadowRadius = 2.1;
        cell.profileImageView.layer.cornerRadius=cell.profileImageView.frame.size.width/2;
        cell.profileImageView.clipsToBounds = YES;

        
        NSString *nulll=[[advicesResponseArray objectAtIndex:indexPath.row]objectForKey:@"expertPictureUrl"];
        
        
        if(![nulll isEqual:[NSNull null]])
        {
            NSString *ImageURL = [NSString stringWithFormat:@"https://az712682.vo.msecnd.net/expert-pictures/%@",[[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"expertPictureURL"]];
            
            @try {
                url = [NSURL URLWithString:ImageURL];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            EquitiesCell1 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                            //                            if (updateCell)
                            cell.profileImageView.image = image;
                        });
                    }
                }
            }];
            [task resume];
            
            
            //            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
            //            cell.PIV.image = [UIImage imageWithData:imageData];
        }else
        {
            NSString *ImageURL = @"https://www.tipranks.com/images/analyst-fallback_tsqr.png";
            
            @try {
                url = [NSURL URLWithString:ImageURL];
            } @catch (NSException *exception) {
                
                
                
            } @finally {
                
            }
            
            
            
            
            NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                if (data) {
                    UIImage *image = [UIImage imageWithData:data];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //                            EquitiesCell1 *updateCell = (id)[tableView cellForRowAtIndexPath:indexPath];
                            //                            if (updateCell)
                            cell.profileImageView.image = image;
                        });
                    }
                }
            }];
            [task resume];
        }

        //Image
        
        
       // NSString *dateStr =[[advicesResponseArray objectAtIndex:indexPath.row ]objectForKey:@"timestamp"];
        
        
        NSString *dateStr =[[advicesResponseArray objectAtIndex:indexPath.row ]objectForKey:@"recommendationDate"];
        
        NSLog(@"%@",dateStr);
        
        // Convert string to date object
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];  //2017-05-31T13:45:29.623Z
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
        NSDate *date = [dateFormat dateFromString:dateStr];
        
        
        NSString *differnce = [self remaningTime:date endDate:[NSDate date]];
        
        cell.dateLabel.text=[NSString stringWithFormat:@"%@",dateStr];
        
        NSLog(@"%@",differnce);

        
        
        cell.nameLabel1.text = [[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"analystName"];
        cell.nameLabel2.text = [[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"firmName"];
        cell.buySellLabel.text = [[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"recommendation"];
            NSString * priceTarget = [NSString stringWithFormat:@"%@",[[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"priceTarget"]];
            
            if([priceTarget isEqual:[NSNull null]]||[priceTarget isEqualToString:@"<null>"])
            {
                cell.priceTargetLabel.text =@"0";
            }else
            {
        cell.priceTargetLabel.text = priceTarget;
            }
            
            NSString * actionLabel = [NSString stringWithFormat:@"%@",[[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"analystAction"]];
        cell.actionLabel.text = actionLabel;
         NSString * percent = @"%";
        NSString * successRate = [NSString stringWithFormat:@"%@",[[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"successRate"]];
        
        float successRateFloat = [successRate floatValue]*100;
        
        NSString * finalSuccessRate = [NSString stringWithFormat:@"%.2f%@",successRateFloat,percent];
        
        NSString * averageReturn = [NSString stringWithFormat:@"%.2f",[[[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"excessReturn"] floatValue]];
        NSString * finalAverageReturn = [averageReturn stringByAppendingString:percent];
        cell.actedBylabel.text = finalSuccessRate;
        cell.sharesSoldLabel.text = finalAverageReturn;
            
            NSString * numOfStars = [[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"numOfStars"];
            float floatStar= [numOfStars floatValue];
            NSLog(@"Float Star:%f",floatStar);
            
            cell.starRatingView.minimumValue=0;
            cell.starRatingView.maximumValue=5;
            cell.starRatingView.value=floatStar;
            cell.starRatingView.allowsHalfStars=YES;
            cell.starRatingView.allowsHalfStars=YES;
            cell.starRatingView.emptyStarImage = [UIImage imageNamed:@"ratingstarEmpty"];
            cell.starRatingView.halfStarImage = [UIImage imageNamed:@"starsmallHalf"]; // optional
            cell.starRatingView.filledStarImage = [UIImage imageNamed:@"ratingstarFilled"];
            [cell.starRatingView setUserInteractionEnabled:NO];
        [cell.buyButton addTarget:self action:@selector(onTableBuySellButtonTap:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    
}
    return nil;

}


-(void)LTPServer
{
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"64a51fa5-83b5-e61d-be01-275a44103c72" };
    
    NSString * url1 = [NSString stringWithFormat:@"http://13.126.147.95/data/getQuotes.json?symbols=%@&webmasterId=89748",self.tickerNameString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url1]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        self.ltpResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        NSLog(@"LTP:%@",self.ltpResponseDictionary);
                                                        NSString * ltp = [NSString stringWithFormat:@"%@",[[[[[self.ltpResponseDictionary objectForKey:@"results"]objectForKey:@"quote"]objectAtIndex:0]objectForKey:@"pricedata"]objectForKey:@"last"]];
                                                        float ltpInt = [ltp floatValue];
                                                        NSString * priceTarget;
                                                        
                                                        if([self.priceTargetCheck isEqualToString:@"watch"])
                                                        {
                                                            priceTarget = [NSString stringWithFormat:@"%@",[[ratingResponseArray objectAtIndex:0] objectForKey:@"priceTarget"]];
                                                            if([priceTarget isEqual:[NSNull null]]||[priceTarget isEqualToString:@"<null>"])
                                                            {
                                                                priceTarget =@"0";
                                                            }
                                                            
                                                        }else
                                                        {
                                                             priceTarget = self.priceTargetString;
                                                        }
                                                        
                                                        float priceInt = [priceTarget floatValue];
                                                        
                                                        float upsideCalculation = ((priceInt-ltpInt)/(ltpInt))*100;
                                                        NSString * upsideString = [NSString stringWithFormat:@"%f",upsideCalculation];
                                                        
                                                        NSLog(@"Upside calculation:%f",upsideCalculation);
                                                       
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            NSString * percent = @"%";
                                                            
                                                            self.ltpLabel.text = [NSString stringWithFormat:@"%.2f",ltpInt];
                                                            if([self.priceTargetString isEqualToString:@"0"])
                                                            {
                                                                self.upSidePercentLabel.text=@"N/A";
                                                            }else
                                                            {
                                                            
                                                            self.upSidePercentLabel.text = [NSString stringWithFormat:@"(%.2f%@)",upsideCalculation,percent];
                                                            }
                                                            
                                                            if([upsideString containsString:@"-"])
                                                            {
                                                                self.upSidePercentLabel.textColor = [UIColor colorWithRed:(242.0)/255 green:(30.0)/255 blue:(51.0)/255 alpha:1];
                                                            }else
                                                            {
                                                                self.upSidePercentLabel.textColor = [UIColor colorWithRed:(10.0)/255 green:(212.0)/255 blue:(0.0)/255 alpha:1];
                                                            }
                                                            
                                                        });

                                                    }
                                                }];
    [dataTask resume];
}


-(void)onMiddleButtonTap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.stockDetailTableView];
    NSIndexPath *indexPath = [self.stockDetailTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
    NewOrderViewController * orders = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    NSString * buySellCheck = [NSString stringWithFormat:@"%@",[[ratingResponseArray objectAtIndex:indexPath.row] objectForKey:@"consensus"]];
    orders.recomondation = buySellCheck;
    delegate.stockTickerString = [[ratingResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    orders.tickerName = [[ratingResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    orders.companyName = [[ratingResponseArray objectAtIndex:indexPath.row] objectForKey:@"companyName"];
    
    [self.navigationController pushViewController:orders animated:YES];
}

-(void)onTableBuySellButtonTap:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.stockDetailTableView];
    
    
    
    NSIndexPath *indexPath = [self.stockDetailTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"Index path:%ld",(long)indexPath.row);
   // stockDetailTableViewCell *cell = [self.stockDetailTableView cellForRowAtIndexPath:indexPath];
    
    NewOrderViewController * orders = [self.storyboard instantiateViewControllerWithIdentifier:@"NewOrderViewController"];
    NSString * buySellCheck = [NSString stringWithFormat:@"%@",[[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"recommendation"]];
    orders.recomondation = buySellCheck;
    delegate.stockTickerString = [[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    orders.tickerName = [[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    orders.companyName = [[advicesResponseArray objectAtIndex:indexPath.row] objectForKey:@"ticker"];
    
    [self.navigationController pushViewController:orders animated:YES];


}
-(NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate
{
    NSDateComponents *components;
    NSInteger days;
    NSInteger hour;
    NSInteger minutes;
    NSInteger seconds;
    NSString *durationString;
    
    components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: startDate toDate: endDate options: 0];
    
    days = [components day];
    hour = [components hour];
    minutes = [components minute];
    seconds = [components second];
    
    if(days>0)
    {
        if(days>1)
            durationString=[NSString stringWithFormat:@"%ld days ago",(long)days];
        else
            durationString=[NSString stringWithFormat:@"%ld days ago",(long)days];
        return durationString;
    }
    if(hour>0)
    {
        if(hour>1)
            durationString=[NSString stringWithFormat:@"%ld hrs ago",(long)hour];
        else
            durationString=[NSString stringWithFormat:@"%ld hrs ago",(long)hour];
        return durationString;
    }
    if(minutes>0)
    {
        if(minutes>1)
            durationString = [NSString stringWithFormat:@"%ld min ago",(long)minutes];
        else
            durationString = [NSString stringWithFormat:@"%ld min ago",(long)minutes];
        
        return durationString;
    }
    if(seconds>0)
    {
        if(seconds>1)
            durationString = [NSString stringWithFormat:@"%ld sec ago",(long)minutes];
        else
            durationString = [NSString stringWithFormat:@"%ld sec ago",(long)minutes];
        
        return durationString;
    }
    return @"";
}


-(void)pieChart
{
    [self.pieChartOutlet setDelegate:self];
    
    [self.pieChartOutlet setDataSource:self];
    [self.pieChartOutlet setStartPieAngle:M_PI_2];
    [self.pieChartOutlet setAnimationSpeed:1.0];
    [self.pieChartOutlet setLabelFont:[UIFont fontWithName:@"DBLCDTempBlack" size:18]];
    [self.pieChartOutlet setLabelRadius:160];
    [self.pieChartOutlet setShowPercentage:YES];
    [self.pieChartOutlet setPieRadius:self.pieChartOutlet.frame.size.height-30];
    [self.pieChartOutlet setUserInteractionEnabled:YES];
    [self.pieChartOutlet setLabelShadowColor:[UIColor blackColor]];
    [self.pieChartOutlet setShowLabel:NO];
    
    @try {
        self.sliceColors =[NSMutableArray arrayWithObjects:
                           [UIColor colorWithRed:(139.0)/255 green:(212.0)/255 blue:(119.0)/255 alpha:1],
                           [UIColor colorWithRed:(238.0)/(255.0) green:(151.0)/(255.0) blue:(120.0)/(255.0) alpha:1],
                           [UIColor colorWithRed:(238.0)/(255.0) green:(214.0)/(255.0) blue:(120.0)/(255.0) alpha:1]
                           ,nil];
        ;
    } @catch (NSException *exception) {
        
        // [UIColor colorWithRed:(247.0)/(255.0) green:(186.0)/(255.0) blue:(65.0)/(255.0) alpha:1],
        //  [UIColor colorWithRed:136/255.0 green:115/255.0 blue:162/255.0 alpha:1],
        //  [UIColor colorWithRed:58/255.0 green:149/255.0 blue:179/255.0 alpha:1],nil]
        
    } @finally {
        
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        
        [self.pieChartOutlet reloadData];
        
        
        
        
    });
}
- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
    return self.pieChartPercentageArray.count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
    return [[self.pieChartPercentageArray objectAtIndex:index] doubleValue];
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    //    if(pieChart == self.pieChartOutlet) return nil;
    return [self.sliceColors objectAtIndex:(index % self.sliceColors.count)];
}

#pragma mark - XYPieChart Delegate
- (void)pieChart:(XYPieChart *)pieChart willSelectSliceAtIndex:(NSUInteger)index
{
    NSLog(@"will select slice at index %lu",(unsigned long)index);
}
- (void)pieChart:(XYPieChart *)pieChart willDeselectSliceAtIndex:(NSUInteger)index
{
    NSLog(@"will deselect slice at index %lu",(unsigned long)index);
}
- (void)pieChart:(XYPieChart *)pieChart didDeselectSliceAtIndex:(NSUInteger)index
{
    NSLog(@"did deselect slice at index %lu",(unsigned long)index);
}
- (void)pieChart:(XYPieChart *)pieChart didSelectSliceAtIndex:(NSUInteger)index
{
    //    NSLog(@"did select slice at index %d",index);
    //    self.selectedSliceLabel.text = [NSString stringWithFormat:@"$%@",[self.slices objectAtIndex:index]];
}
- (NSString *)pieChart:(XYPieChart *)pieChart textForSliceAtIndex:(NSUInteger)index//optional
{
    return @"test";
}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
