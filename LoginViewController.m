//
//  LoginViewController.m
//  ZambalaUSA
//
//  Created by zenwise mac 2 on 9/5/17.
//  Copyright © 2017 guna. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "TabMenuView.h"
#import "SelectLoginViewController.h"
#import "UIViewController+MJPopupViewController.h"



@interface LoginViewController ()

@end

@implementation LoginViewController
{
    AppDelegate * delegate;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.clientIDTF.text=@"VT070971";
    self.passwordTF.text=@"EQI387";
    
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Login"
//                                                                                  message: @"Input username and password"
//                                                                           preferredStyle:UIAlertControllerStyleAlert];
//        UIButton * guestLoginButton = [[UIButton alloc]initWithFrame:CGRectMake(20, 10, 50, 30)];
//        guestLoginButton.titleLabel.text = @"Guest Login";
//        
//        UIButton * clientLoginButton = [[UIButton alloc]initWithFrame:CGRectMake(60, 10, 50, 30)];
//        clientLoginButton.titleLabel.text = @"Client Login";
//        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//            textField.placeholder = @"name";
//            textField.textColor = [UIColor blueColor];
//            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//            textField.borderStyle = UITextBorderStyleNone;
//        }];
//        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//            textField.placeholder = @"password";
//            textField.textColor = [UIColor blueColor];
//            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//            textField.borderStyle = UITextBorderStyleNone;
//            textField.secureTextEntry = YES;
//        }];
//        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            NSArray * textfields = alertController.textFields;
//            UITextField * namefield = textfields[0];
//            UITextField * passwordfiled = textfields[1];
//            NSLog(@"%@:%@",namefield.text,passwordfiled.text);
//            
//        }]];
//        [alertController.view addSubview:guestLoginButton];
//        [alertController.view addSubview:clientLoginButton];
//        [self presentViewController:alertController animated:YES completion:nil];
//        
//       
//         
//    });
//
    
//    SelectLoginViewController *selectLogin = [[SelectLoginViewController alloc] initWithNibName:@"SelectLoginViewController" bundle:nil];
//    [self presentPopupViewController:selectLogin animationType: MJPopupViewAnimationFade];
//
//
//
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
//

    
    // Do any additional setup after loading the view.
    
}

-(void)dismissKeyboard
{
    [self.clientIDTF resignFirstResponder];
    [self.passwordTF resignFirstResponder];
}

-(void)loginCheckServer
{
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"%@\",\"password\":\"%@\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}",self.clientIDTF.text,self.passwordTF.text];
   // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://api-test.choicetrade.com/"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
//    NSDictionary *cookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
//                                      @"domain.com", NSHTTPCookieDomain,
//                                      @"\\", NSHTTPCookiePath,
//                                      @"myCookie", NSHTTPCookieName,
//                                      @"1234", NSHTTPCookieValue,
//                                      nil];
//    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
//    NSArray* cookieArray = [NSArray arrayWithObject:cookie];
//    NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:cookieArray];
//    [request setAllHTTPHeaderFields:headers];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        int successCheck = [[json objectForKey:@"success"] intValue];
                                                        
                                                        if(successCheck==1)
                                                        {
                                                            delegate.userID = self.clientIDTF.text;
                                                        delegate.sessionID = [json objectForKey:@"session_id"];
                                                            NSLog(@"Session ID:%@",delegate.sessionID);
                                                            [self getAccountNumberServer];
                                                        }else if ([[json objectForKey:@"message"]isEqualToString:@"Error: Firm is Invalid."])
                                                        {
                                                            [self loginCheckServer];
                                                        }
                                                    }
                                                }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getAccountNumberServer
{
    NSString * str = [NSString stringWithFormat:@"api={\"name\":\"JSON_GetAccountList\",\"client_id\":\"T7qqV91jR2\",\"session_id\":\"%@\",\"firm_code\":\"5ntJOPXqQb\",\"byCust\":\"0\",\"userID\":\"%@\",\"accountMode\":\"A\"}",delegate.sessionID,delegate.userID];
    // NSString* str = @"api={\"name\":\"JSON_Login\",\"client_id\":\"T7qqV91jR2\",\"firm_code\":\"5ntJOPXqQb\",\"type\":\"C\",\"userID\":\"VT070971\",\"password\":\"EQI387\",\"fromIP\":\"192.168.12.26\",\"browser\":\"mobilebrowser\"}";
    
    NSData *dt = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://api-test.choicetrade.com/"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody:dt];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary* json = [NSJSONSerialization
                                                                              JSONObjectWithData:data
                                                                              options:kNilOptions
                                                                              error:&error];
                                                        NSLog(@"jjson %@",json);
                                                        int successCheck = [[json objectForKey:@"success"] intValue];
                                                        
                                                        if(successCheck==1)
                                                        {
                                                            delegate.accountNumber = [[json objectForKey:@"accounts"] objectForKey:@"accountNo"];
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                TabMenuView * view = [self.storyboard instantiateViewControllerWithIdentifier:@"TabMenuView"];
                                                                [self presentViewController:view animated:YES completion:nil];
                                                            });
                                                        }
                                                    }
                                                }];
    [dataTask resume];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onContinueButtonTap:(id)sender {
    
    
    [self loginCheckServer];
}
@end
